@startuml
title Modèle Conceptuel de Données (UML)
abstract class Contributeur{
+nom : string
+prenom : string
+date_naissance : date
+nationalite : string
}
class Acteur{
}
class Realisateur{
}
class Auteur {
}
class Compositeur {
}
class Interprete {
}
class Pret{
+date_pret : date
+date_maximale : date
+date_retour : date
+nb_jours_retard() : int
+duree_pret() : time
}
abstract class Ressource{
+code : string {primary key}
+titre : string
+date_appartition : date
+editeur : string
+genre : string
+code_classification : string
}
class Exemplaire {
+etat : enum{neuf, bon, abîmé, perdu}
+est_disponible()
}
class Livre{
+ISBN : string
+resume : string
+langue : string
}
class Film{
+synopsis : string
+langue : string
+longueur : time
}
class Enregistrement_musical{
+longueur : time
}
abstract class Utilisateur{
+login : string
+mot_de_passe : string
+nom : string
+prenom : string
+mail : string
}
class Membre_personnel{
}
class Adherent{
+ncarte : int {primary key}
+date_naissance : date
+telephone : string
+actuel : bool
+adresse : string
+bool est_blacklist
}
abstract class Sanction{
}
class Retard{
+nb_jours : int
}
class Dégradation{
+prix_a_payer : float
+nouvel_etat {bon, abîmé,perdu}
}
Exemplaire "*"--"*" Adherent
Pret . (Exemplaire,Adherent)
Contributeur <|-- Acteur
Contributeur <|-- Auteur
Contributeur <|-- Interprete
Contributeur <|-- Compositeur
Contributeur <|-- Realisateur
Ressource "1"--"1.." Exemplaire : est_exemplaire_de
Ressource <|-- Livre
Ressource <|-- Film
Ressource <|-- Enregistrement_musical
Livre "*"--"1..*" Auteur : est_auteur
Film "*"--"1..*" Acteur : est_acteur
Film "*"--"1..*" Realisateur : est_realisateur
Enregistrement_musical "*"--"1..*" Compositeur : est_compositeur
Enregistrement_musical "*"--"1..*" Interprete : est_interprete
Utilisateur <|-- Adherent
Utilisateur <|-- Membre_personnel
Pret "*"--"1..1" Membre_personnel
Pret "1..1"--"0..1" Sanction
Sanction <|-- Retard
Sanction <|-- Dégradation
@enduml
