# Note de clarification


### Liste des classes objet

- _Ressource_ (code : string {clé primaire}, titre : string, date\_apparition : date, editeur : string, genre : string, code\_classification : string)
- _Exemplaire_ (etat : enum{neuf, bon, abîmé, perdu})
- _Livre_ (ISBN {clé primaire} : string, resume : string, langue : string)
- _Film_ (synopsis : string, langue : string, longueur : time)
- _Enregistrement_musical_ (longueur : time)
- _Contributeur_ (nom : string, prenom : string, date_naissance : date, nationalite : string)
- _Utilisateur_ (login : string, mot\_de\_passe : string ,nom : string, prenom : string, mail : string, adresse : string)
- _Membre_personnel_ ()
- _Adherent_ (ncarte : int {clé primaire}, date\_naissance : date, telephone : string, actuel : bool, nb_pret_simultane() : int, date\_emprunt\_possible() : date, nb\_retards() : int, reste_a_rembourser() : int, nb\_remboursements() : int, blackliste() : bool))
- _Pret_ (date_pret : date, duree : time, date_retour : date, sanction : enum{aucune, retard, dégradation})


### Liste des classes association

- *Livre*, *Film* et *Enregistrement_musical* héritent de la classe *Ressource*.
- *Ressource* est une classe abstraite.
- Il existe plusieurs objets de la classe *Exemplaire* pour un même objet de la classe *Ressource*.
- *Acteur*, *Auteur*, *Realisateur*, *Interprete*, *Compositeur* héritent de la classe *Contribueur*.
- Un *Contributeur* peut être à la fois *Compositeur*, *Interprete* ou *Realisateur* et *Auteur* donc l'héritage est non exclusif. 
- *Membre_personnel* et *Adherent* héritent de la classe *Utilisateur*.
- Un *Utilisateur* peut être à la fois *Membre_personnel* et *Adherent* donc l'héritage est non exclusif.
- *Adherent* et *Exemplaire* sont mis en relation par la classe association *Pret*, un prêt possède un emprunteur - l'adhérent - et un document - l'exemplaire d'une ressource.


### Liste des propriétés liées aux classes objet et association

- Plusieurs exemplaires d'une ressource peuvent existés. Pour cela, chaque exemplaire aura son code unique et son état, tout le reste sera similaire pour tous les exemplaires (dans un même objet de la classe *Ressource*). 
- Un adhérent peut être blacklisté par un membre du personnel, pour savoir le nombre de santions, il faut se fier aux méthodes *nb_retards* et *nb_remboursements* qui sont calculés à partir de l'association *Pret* pour un utilisateur donné.
- Un adhérent ayant rendu en retard une ressource se verra appliqué une suspension du droit de prêt d'une durée égale au retard. L'attribut *date_emprunt_possible* correspond à la date à laquelle l'adhérent peut emprunter. Si un retard a lieu, la date d'emprunt sera mise à jour avec la prochaine date à laquelle il pourra emprunter
- Un adhérent peut devoir rembourser un document, la méthode *reste-a-rembourser* est calcul le montant que l'adhérent doit a la bibliothèque. Alors il ne pourra plus emprunter jusqu'à ce que *reste-a-rembourser* soit à 0.
- Un adhérent peut emprunter plusieurs exemplaires simultanément (nb_pret_simultane) dans un nombre limité m (nb_pret_simultane <= m), c'est une realtion n..n entre *Exemplaire* et *Adhérent*.
- Un contributeur peut avoir plusieurs rôles, dans plusieurs ressources, et une ressource peut avoir plusieurs contributeurs, c'est une relations n..n entre *Contributeur* et *Livre*, n..n entre *Contributeur* et *Film*, n..n entre *Contributeur* et *Enregistrement_musical*.
- Pour les personnes blacklistées, le compte sera bloqué par l'application.


### Liste des contraintes

- Les ressources empruntées doivent être dans l'état *neuf* ou *bon*. 
- Un adhérent blacklistée ne pourra plus empruntée. 
- Tant que *reste-a-rembourser* n'est pas égal à 0, un *Adhérent* n'est pas autorisé à emprunter de nouvel *exemplaire*.
- Une ressource peut être prêtée tant qu'il reste des *Exemplaire* disponibles.
- **Il faut ajouter des contraites pour qu'un *Auteur* ne peuvent être en relation qu'avec un *Livre* (et non un *Film* ou un *Enregistrement_musical*). De même un *Acteur* et un *Producteur* ne peuvent être en relation qu'avec un *Film* (et non un *Enregistrement_musical* ou un *Livre*). Enfin, un *Compositeur* et un *Interprete* ne peuvent être en relation qu'avec un *Enregistrement_musical* (et non un *Film* ou un *Livre*).**


### Liste des utilisateurs

- Les membres du personnel pourront modifier et consulter les données. Ils auront accès et pourront gérer les ressources, les adhérents, les différents contributeurs, les prêts en cours, etc. 
- Les adhérents pourront accéder aux livres disponibles et choisir d'en emprunter, s'ils le peuvent. 


### Liste des besoins

Pour les membres du personnel : 
- Ajouter, supprimer, modifier des ressources et des exemplaires
- Gérer les prêts, retards et remboursements
- Établir des statistiques sur les documents empruntés

Pour les adhérents : 
- Consulter les ressources disponibles (c'est à dire celles dont il reste des exemplaires)
- Emprunter et rendre un exemplaire
- Rembourser un exemplaire rendu en mauvais état



