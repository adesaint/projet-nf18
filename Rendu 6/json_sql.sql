DROP TABLE IF EXISTS Ressource CASCADE;
DROP TABLE IF EXISTS Sanction;
DROP TABLE IF EXISTS PRET;
DROP TABLE IF EXISTS Exemplaire;
DROP TABLE IF EXISTS Adherent;
DROP TABLE IF EXISTS Membre_personnel;
DROP TABLE IF EXISTS PRET;
DROP TABLE IF EXISTS Contributeur CASCADE;
DROP TABLE IF EXISTS est_acteur;
DROP TABLE IF EXISTS est_auteur;
DROP TABLE IF EXISTS est_realisateur;
DROP TABLE IF EXISTS est_interprete;
DROP TABLE IF EXISTS est_compositeur;
DROP TABLE IF EXISTS est_acteur_check;
DROP TABLE IF EXISTS est_realisateur_check;
DROP TABLE IF EXISTS est_interprete_check;
DROP TABLE IF EXISTS est_compositeur_check;
DROP TABLE IF EXISTS est_auteur_check;


CREATE TABLE Ressource (
  code SERIAL PRIMARY KEY,
  titre VARCHAR(255) NOT NULL,
  date_apparition DATE NOT NULL,
  editeur VARCHAR(255) NOT NULL,
  genre VARCHAR(255) NOT NULL,
  code_classification VARCHAR(255) NOT NULL,
  type VARCHAR(255) NOT NULL CHECK (type IN ('livre', 'film', 'enregistrement_musical')),
  ISBN VARCHAR(255) UNIQUE,
  resume VARCHAR(255),
  langue VARCHAR(255),
  synopsis VARCHAR(255),
  longueur TIME,
  CHECK (
    NOT (ISBN IS NOT NULL AND (type = 'film' OR type = 'enregistrement_musical')) AND
    NOT (resume IS NOT NULL AND (type = 'film' OR type = 'enregistrement_musical')) AND
    NOT (langue IS NOT NULL AND type = 'enregistrement_musical') AND
    NOT (synopsis IS NOT NULL AND (type = 'livre' OR type = 'enregistrement_musical')) AND
    NOT (longueur IS NOT NULL AND type = 'livre') AND
    ((ISBN IS NOT NULL AND resume IS NOT NULL AND langue IS NOT NULL AND type = 'livre') OR
    (synopsis IS NOT NULL AND langue IS NOT NULL AND longueur IS NOT NULL AND type = 'film') OR
    (longueur IS NOT NULL AND type = 'enregistrement_musical'))),
  CHECK (
    (type = 'livre' AND NOT (type = 'film' OR type = 'enregistrement_musical')) OR
    (type = 'film' AND NOT (type = 'livre' OR type = 'enregistrement_musical')) OR
    (type = 'enregistrement_musical' AND NOT (type = 'livre' OR type = 'film'))
  )
);


CREATE TABLE Contributeur (
  id SERIAL PRIMARY KEY,
  nom VARCHAR(255) NOT NULL,
  prenom VARCHAR(255) NOT NULL,
  date_naissance DATE NOT NULL,
  nationalite VARCHAR(255) NOT NULL,
  type VARCHAR(255) NOT NULL CHECK (type IN('auteur', 'acteur', 'realisateur', 'interprete', 'compositeur')),
  CHECK (
    (type = 'auteur' AND type = 'livre') OR
    (type <> 'auteur') OR
    (type = 'acteur' AND type = 'film') OR
    (type = 'realisateur' AND type = 'film') OR
    (type <> 'acteur' AND type <> 'realisateur') OR
    (type = 'compositeur' AND type = 'enregistrement_musical') OR
    (type = 'interprete' AND type = 'enregistrement_musical') OR
    (type <> 'compositeur' AND type <> 'interprete')
  )
);

CREATE TABLE est_auteur (
  livre INT,
  auteur INT,
  PRIMARY KEY(livre, auteur),
  FOREIGN KEY (livre) REFERENCES Ressource (code),
  FOREIGN KEY (auteur) REFERENCES Contributeur (id)
);
CREATE OR REPLACE FUNCTION check_auteur_constraint() RETURNS TRIGGER AS $$
BEGIN
  IF NEW.auteur IS NULL THEN
    RETURN NEW;
  END IF;

  IF NOT EXISTS (
    SELECT 1 
    FROM est_auteur ea
    WHERE ea.livre = NEW.livre AND ea.auteur = NEW.auteur
  ) AND EXISTS (
    SELECT 1 
    FROM Contributeur c
    INNER JOIN Ressource r ON NEW.livre = r.code
    WHERE c.id = NEW.auteur AND c.type = 'auteur' AND r.type = 'livre'
  ) THEN
    RETURN NEW;
  ELSE
    RAISE EXCEPTION 'Le contributeur n''est pas de type Auteur, la ressource n''est pas de type Livre, ou la relation existe déjà.';
  END IF;

  RETURN NULL; 
END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER est_auteur_check
BEFORE INSERT OR UPDATE
ON est_auteur
FOR EACH ROW
EXECUTE FUNCTION check_auteur_constraint();


CREATE TABLE est_acteur (
  film INT,
  acteur INT,
  PRIMARY KEY(film, acteur),
  FOREIGN KEY (film) REFERENCES Ressource (code),
  FOREIGN KEY (acteur) REFERENCES Contributeur (id)
);

CREATE OR REPLACE FUNCTION check_acteur_constraint() RETURNS TRIGGER AS $$
BEGIN
  IF NEW.acteur IS NULL THEN
    RETURN NEW;
  END IF;

  IF NOT EXISTS (
    SELECT 1 
    FROM est_acteur ea
    WHERE ea.film = NEW.film AND ea.acteur = NEW.acteur
  ) AND EXISTS (
    SELECT 1 
    FROM Contributeur c
    INNER JOIN Ressource r ON NEW.film = r.code
    WHERE c.id = NEW.acteur AND c.type = 'acteur' AND r.type = 'film'
  ) THEN
    RETURN NEW;
  ELSE
    RAISE EXCEPTION 'Le contributeur n''est pas de type Acteur, la ressource n''est pas de type Film, ou la relation existe déjà.';
  END IF;

  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER est_acteur_check
BEFORE INSERT OR UPDATE
ON est_acteur
FOR EACH ROW
EXECUTE FUNCTION check_acteur_constraint();


CREATE TABLE est_realisateur (
  film INT,
  realisateur INT,
  PRIMARY KEY(film, realisateur),
  FOREIGN KEY (film) REFERENCES Ressource (code),
  FOREIGN KEY (realisateur) REFERENCES Contributeur (id)
);

CREATE OR REPLACE FUNCTION check_realisateur_constraint() RETURNS TRIGGER AS $$
BEGIN
  IF NEW.realisateur IS NULL THEN
    RETURN NEW;
  END IF;

  IF NOT EXISTS (
    SELECT 1 
    FROM est_realisateur er
    WHERE er.film = NEW.film AND er.realisateur = NEW.realisateur
  ) AND EXISTS (
    SELECT 1 
    FROM Contributeur c
    INNER JOIN Ressource r ON NEW.film = r.code
    WHERE c.id = NEW.realisateur AND c.type = 'realisateur' AND r.type = 'film'
  ) THEN
    RETURN NEW;
  ELSE
    RAISE EXCEPTION 'Le contributeur n''est pas de type Réalisateur, la ressource n''est pas de type Film, ou la relation existe déjà.';
  END IF;

  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER est_realisateur_check
BEFORE INSERT OR UPDATE
ON est_realisateur
FOR EACH ROW
EXECUTE FUNCTION check_realisateur_constraint();

CREATE TABLE est_interprete (
  enregistrement_musical INT,
  interprete INT,
  PRIMARY KEY(enregistrement_musical, interprete),
  FOREIGN KEY (enregistrement_musical) REFERENCES Ressource (code),
  FOREIGN KEY (interprete) REFERENCES Contributeur (id)
);

CREATE OR REPLACE FUNCTION check_interprete_constraint() RETURNS TRIGGER AS $$
BEGIN
  IF NEW.interprete IS NULL THEN
    RETURN NEW;
  END IF;

  IF NOT EXISTS (SELECT 1 FROM Contributeur WHERE id = NEW.interprete AND type = 'interprete') THEN
    RAISE EXCEPTION 'Le contributeur n''est pas de type Interprete.';
  END IF;

  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER est_interprete_check
BEFORE INSERT OR UPDATE
ON est_interprete
FOR EACH ROW
EXECUTE FUNCTION check_interprete_constraint();


CREATE TABLE est_compositeur (
  enregistrement_musical INT,
  compositeur INT,
  PRIMARY KEY(enregistrement_musical, compositeur),
  FOREIGN KEY (enregistrement_musical) REFERENCES Ressource (code),
  FOREIGN KEY (compositeur) REFERENCES Contributeur (id)
);

CREATE OR REPLACE FUNCTION check_compositeur_constraint() RETURNS TRIGGER AS $$
BEGIN
  IF NEW.compositeur IS NULL THEN
    RETURN NEW;
  END IF;

  IF NOT EXISTS (
    SELECT 1 
    FROM est_compositeur ec
    WHERE ec.enregistrement_musical = NEW.enregistrement_musical AND ec.compositeur = NEW.compositeur
  ) AND EXISTS (
    SELECT 1 
    FROM Contributeur c
    INNER JOIN Ressource r ON NEW.enregistrement_musical = r.code
    WHERE c.id = NEW.compositeur AND c.type = 'compositeur' AND r.type = 'enregistrement_musical'
  ) THEN
    RETURN NEW;
  ELSE
    RAISE EXCEPTION 'Le contributeur n''est pas de type Compositeur, la ressource n''est pas de type Enregistrement Musical, ou la relation existe déjà.';
  END IF;

  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER est_compositeur_check
BEFORE INSERT OR UPDATE
ON est_compositeur
FOR EACH ROW
EXECUTE FUNCTION check_compositeur_constraint();



CREATE TABLE Membre_personnel (
  login VARCHAR(255) PRIMARY KEY,
  mot_de_passe VARCHAR(255) NOT NULL,
  nom VARCHAR(255) NOT NULL,
  prenom VARCHAR(255) NOT NULL,
  mail VARCHAR(255) NOT NULL
);

CREATE TABLE Adherent (
  login VARCHAR(255) PRIMARY KEY,
  mot_de_passe VARCHAR(255) NOT NULL,
  nom VARCHAR(255) NOT NULL,
  prenom VARCHAR(255) NOT NULL,
  mail VARCHAR(255) NOT NULL,
  ncarte INT NOT NULL,
  date_naissance DATE NOT NULL,
  telephone VARCHAR(255) NOT NULL,
  actuel BOOLEAN NOT NULL,
  adresse VARCHAR(255) NOT NULL,
  est_blacklist BOOLEAN NOT NULL,
  UNIQUE (ncarte)
);



CREATE TABLE Exemplaire (
  id SERIAL PRIMARY KEY,
  etat VARCHAR(255) CHECK (etat IN ('neuf', 'bon', 'abime', 'perdu')),
  code_ressource INT NOT NULL,
  FOREIGN KEY (code_ressource) REFERENCES Ressource (code)
);


CREATE TABLE PRET(
  date_pret DATE,
  exemplaire INT,
  adherent VARCHAR(255),
  membre_personnel VARCHAR(255) NOT NULL,
  date_retour DATE,
  date_maximale DATE,
  PRIMARY KEY (date_pret, exemplaire, adherent),
  FOREIGN KEY (exemplaire) REFERENCES Exemplaire (id),
  FOREIGN KEY (adherent) REFERENCES Adherent (login),
  FOREIGN KEY (membre_personnel) REFERENCES Membre_personnel (login)
);


CREATE TABLE Sanction (
  id SERIAL,
  pret_date DATE NOT NULL,
  pret_exemplaire INT NOT NULL,
  pret_adherent VARCHAR(255) NOT NULL,
  type_sanction VARCHAR(255) CHECK (type_sanction IN ('retard', 'dégradation')) NOT NULL,
  nb_jours_retard INT,
  prix_a_payer FLOAT,
  etat_apres_rendu VARCHAR(255) CHECK (etat_apres_rendu IN ('bon', 'abîmé', 'perdu')),
  PRIMARY KEY (id, pret_exemplaire, pret_adherent),
  FOREIGN KEY (pret_date, pret_exemplaire, pret_adherent) REFERENCES PRET (date_pret, exemplaire, adherent),
  CONSTRAINT unique_pret_exemplaire_type UNIQUE (pret_exemplaire, type_sanction),
  CHECK (
    (type_sanction = 'retard' AND nb_jours_retard IS NOT NULL AND etat_apres_rendu IS NULL AND prix_a_payer IS NULL) OR
    (type_sanction = 'dégradation' AND etat_apres_rendu IS NOT NULL AND nb_jours_retard IS NULL AND prix_a_payer IS NULL) OR
    (type_sanction IS NULL AND nb_jours_retard IS NULL AND prix_a_payer IS NULL AND etat_apres_rendu IS NULL) OR
    (type_sanction = 'retard' AND nb_jours_retard IS NOT NULL) OR
    (type_sanction = 'dégradation' AND etat_apres_rendu IS NOT NULL AND prix_a_payer IS NOT NULL AND etat_apres_rendu = 'perdu')
  )
);

CREATE TABLE DemandePret(
    id SERIAL PRIMARY KEY,
    date_demande DATE NOT NULL,
    adherent_login VARCHAR(255) NOT NULL,
    code_ressource INT NOT NULL,
    statut VARCHAR(255) CHECK (statut IN ('en_attente', 'acceptee', 'refusee')) NOT NULL,
    FOREIGN KEY (adherent_login) REFERENCES Adherent(login),
    FOREIGN KEY (code_ressource) REFERENCES Ressource(code)
);

ALTER TABLE PRET
ADD COLUMN demande_pret_id INT,
ADD CONSTRAINT fk_demande_pret FOREIGN KEY (demande_pret_id) REFERENCES DemandePret(id);

INSERT INTO Ressource (titre, date_apparition, editeur, genre, code_classification, type, ISBN, resume, langue) VALUES ('Les Misérables
', '2021-01-15', 'École des Loisirs', 'Roman', 'ABC123', 'livre', '1234567890', 'Les Misérables retrace le destin de Jean Valjean, ancien forçat condamné pour le vol de pain. Au fil de son histoire, le roman fait des détours pour se concentrer les autres personnages, comme Fantine ou Marius.', 'Français');

INSERT INTO Ressource (titre, date_apparition, editeur, genre, code_classification, type, ISBN, resume, langue) VALUES ('Les Trois Mousquetaires', '2023-02-19', 'Le Livre de Poche', 'Roman', 'ABC123', 'livre', '1234567891', 'L histoire raconte les aventures du jeune, courageux et rusé gascon d Artagnan, qui arrive à Paris de sa province natale et qui se lie damitié avec trois des mousquetaires du roi. Ils vont sunir pour déjouer les machinations du cardinal de Richelieu.', 'Français');

INSERT INTO Ressource (titre, date_apparition, editeur, genre, code_classification, type, ISBN, resume, langue) VALUES ('Cyrano de Bergerac', '2022-12-15', 'Petit à Petit', 'Théâtre', 'ABC190', 'livre', '1234567892', 'Cyrano de Bergerac est un mousquetaire de la compagnie des Cadets de Gascogne. Il est amoureux de sa cousine Roxane. Il ne lui dit rien car il se trouve très laid à cause de son nez difforme.', 'Français');

INSERT INTO Ressource (titre, date_apparition, editeur, genre, code_classification, type, synopsis, langue, longueur) VALUES ('Hunger Games', '2023-03-10', 'Studio Babelsberg',
'Action', '123XYZ', 'film', 'Dans un futur sombre, à Panem, un État totalitaire fondé sur les ruines des États-Unis, Katniss Everdeen, une jeune fille de 16 ans habitant dans le district 12, va combattre à mort dans une arène : les Hunger Games', 'Anglais', '02:32:00');

INSERT INTO Ressource (titre, date_apparition, editeur, genre, code_classification, type, synopsis, langue, longueur) VALUES ('Spider-Man', '2020-05-10', 'Universal Studios', 'Fiction', '123UYO', 'film', 'Peter Parker, un jeune étudiant, subit une morsure d une araignée génétiquement modifiée, ce qui lui confère bientôt des pouvoirs extraordinaires : une force et une agilité surhumaines', 'Langue', '02:43:00');

INSERT INTO Ressource (titre, date_apparition, editeur, genre, code_classification, type, longueur) VALUES ('LDVM SYMPHONY', '2021-08-20', 'Teta', 'Rap', 'XYZ456', 'enregistrement_musical', '01:15:00');

INSERT INTO Ressource (titre, date_apparition, editeur, genre, code_classification, type, longueur) VALUES ('Justified', '2012-02-21', 'Jive Records', 'Pop',
'XYZ478', 'enregistrement_musical', '00:55:00');

INSERT INTO Contributeur (nom, prenom, date_naissance, nationalite, type) VALUES ('Lawrence', 'Jennifer', '1990-01-15', 'Américaine', 'acteur');

INSERT INTO Contributeur (nom, prenom, date_naissance, nationalite, type) VALUES ('Hugo', 'Victor', '1828-10-21', 'Française', 'auteur');

INSERT INTO Contributeur (nom, prenom, date_naissance, nationalite, type) VALUES ('Timberlake', 'Justin', '1980-05-10', 'Américaine', 'compositeur');

INSERT INTO Contributeur (nom, prenom, date_naissance, nationalite, type) VALUES ('Webb', 'Marc', '1976-01-12', 'Américaine', 'realisateur');

INSERT INTO Contributeur (nom, prenom, date_naissance, nationalite, type) VALUES ('Gims', 'Maître', '1976-01-12', 'Française', 'interprete');


INSERT INTO Membre_personnel (login, mot_de_passe, nom, prenom, mail) VALUES
('raniaattour1', 'mdp123', 'Attour', 'Rania', 'rania.attour@email.com'),
('emmachoukroun1', 'pass456', 'Choukroun', 'Emma', 'emma.choukroun@email.com'),
('remibouchard1', 'secret789', 'Bouchard', 'Remi', 'remi.bouchard@email.com'),
('arthurmartin1', 'key987', 'Martin', 'Arthur', 'arthur.martin@email.com'),
('utc1', 'utc2023', 'Universite', 'Technologique', 'utc@email.com');

INSERT INTO Adherent (login, mot_de_passe, nom, prenom, mail, ncarte, date_naissance, telephone, actuel, adresse, est_blacklist) VALUES
('jeandupont1', 'pass123', 'Dupont', 'Jean', 'jean.dujardin@email.com', 111111, '1990-05-15', '+1234567890', true, '123 Rue A', false),
('insafsetitra1', 'secret456', 'Setitra', 'Insaf', 'insaf.setitra@email.com', 222222, '1985-08-22', '+9876543210', true, '456 Avenue B', false),
('alessandrocorrea1', 'mdp789', 'Correa', 'Alessandro', 'alessandro.correa@email.com', 333333, '1998-02-10', '+5678901234', true, 'allee C', false),
('alixbeslay1', 'password987', 'Beslay', 'Alix', 'alix.beslay@email.com', 444444, '2000-11-30', '+3456789012', true, '234 B D', false),
('davidbloquet1', 'utc2023', 'Bloquet', 'David', 'david.bloquet@email.com', 555555, '1965-01-01', '+1122334455', true, 'Davissss', false);

INSERT INTO Exemplaire (etat, code_ressource) VALUES
('neuf', 1),
('bon', 2),
('abime', 3),
('perdu', 2),
('neuf', 1);

INSERT INTO PRET (date_pret, exemplaire, adherent, membre_personnel, date_retour, date_maximale) VALUES
('2023-01-10', 1, 'jeandupont1', 'raniaattour1', '2023-01-20', '2023-02-10'),
('2023-02-15', 2, 'insafsetitra1', 'emmachoukroun1', '2023-03-01', '2023-03-15'),
('2023-03-20', 3, 'alessandrocorrea1', 'remibouchard1', NULL, '2023-04-10'),
('2023-04-25', 4, 'alixbeslay1', 'arthurmartin1', NULL, '2023-05-15'),
('2023-05-30', 5, 'davidbloquet1', 'utc1', NULL, '2023-06-20');

INSERT INTO Sanction (pret_date, pret_exemplaire, pret_adherent, type_sanction, nb_jours_retard, prix_a_payer, etat_apres_rendu) VALUES
('2023-01-10', 1, 'jeandupont1', 'retard', 5, 10.50, NULL),
('2023-02-15', 2, 'insafsetitra1', 'retard', 3, 7.75, NULL),
('2023-05-30', 5, 'davidbloquet1', 'retard', 7, 15.25, NULL);


INSERT INTO est_auteur VALUES (1,2);

INSERT INTO est_realisateur VALUES (5,4);

INSERT INTO est_acteur VALUES (4,1);

INSERT INTO est_interprete VALUES (6,5);

INSERT INTO est_compositeur VALUES (7,3);

-- Il y a aussi les insertions que nous avons fait grâce à l'application

-- Nouvelles tables avec JSON

CREATE TABLE Ressource_JSON (
  code SERIAL PRIMARY KEY,
  titre VARCHAR(255) NOT NULL,
  date_apparition DATE NOT NULL,
  editeur VARCHAR(255) NOT NULL,
  genre VARCHAR(255) NOT NULL,
  code_classification VARCHAR(255) NOT NULL,
  type VARCHAR(255) NOT NULL CHECK (type IN ('livre', 'film', 'enregistrement_musical')),
  ISBN VARCHAR(255) UNIQUE,
  resume VARCHAR(255),
  langue VARCHAR(255),
  synopsis VARCHAR(255),
  longueur TIME,
  acteurs JSON,
  realisateurs JSON,
  auteurs JSON,
  interpretes JSON,
  compositeurs JSON,
  exemplaires JSON NOT NULL,
  CHECK (
   NOT (acteurs IS NULL AND realisateurs IS NULL AND auteurs IS NOT NULL AND interpretes IS NOT NULL AND compositeurs IS NOT NULL AND type = 'film') AND 
    NOT (acteurs IS NOT NULL AND realisateurs IS NOT NULL AND auteurs IS NULL AND interpretes IS NOT NULL AND compositeurs IS NOT NULL AND type = 'livre') AND
    NOT (acteurs IS NOT NULL AND realisateurs IS NOT NULL AND auteurs IS NOT NULL AND interpretes IS NULL AND compositeurs IS NULL AND type = 'enregistrement_musical') AND
    NOT (ISBN IS NOT NULL AND (type = 'film' OR type = 'enregistrement_musical')) AND
    NOT (resume IS NOT NULL AND (type = 'film' OR type = 'enregistrement_musical')) AND
    NOT (langue IS NOT NULL AND type = 'enregistrement_musical') AND
    NOT (synopsis IS NOT NULL AND (type = 'livre' OR type = 'enregistrement_musical')) AND
    NOT (longueur IS NOT NULL AND type = 'livre') AND
    ((ISBN IS NOT NULL AND resume IS NOT NULL AND langue IS NOT NULL AND type = 'livre') OR
    (synopsis IS NOT NULL AND langue IS NOT NULL AND longueur IS NOT NULL AND type = 'film') OR
    (longueur IS NOT NULL AND type = 'enregistrement_musical'))
  ),
  CHECK (
    (type = 'livre' AND NOT (type = 'film' OR type = 'enregistrement_musical')) OR
    (type = 'film' AND NOT (type = 'livre' OR type = 'enregistrement_musical')) OR
    (type = 'enregistrement_musical' AND NOT (type = 'livre' OR type = 'film'))
  )
);

CREATE TABLE DemandePret_JSON(
    id SERIAL PRIMARY KEY,
    date_demande DATE NOT NULL,
    adherent_login VARCHAR(255) NOT NULL,
    code_ressource INT NOT NULL,
    statut VARCHAR(255) CHECK (statut IN ('en_attente', 'acceptee', 'refusee')) NOT NULL,
    FOREIGN KEY (adherent_login) REFERENCES Adherent(login),
    FOREIGN KEY (code_ressource) REFERENCES Ressource_JSON(code)
);

CREATE TABLE PRET_JSON(
  date_pret DATE,
  exemplaire INT NOT NULL,
  adherent VARCHAR(255),
  membre_personnel VARCHAR(255) NOT NULL,
  date_retour DATE,
  date_maximale DATE,
  demande_pret_id INT,
  FOREIGN KEY (demande_pret_id) REFERENCES DemandePret_JSON(id),
  PRIMARY KEY (date_pret, exemplaire, adherent),
  FOREIGN KEY (adherent) REFERENCES Adherent (login),
  FOREIGN KEY (membre_personnel) REFERENCES Membre_personnel (login)
);

CREATE TABLE Sanction_JSON (
  id SERIAL,
  pret_date DATE NOT NULL,
  pret_exemplaire INT NOT NULL,
  pret_adherent VARCHAR(255) NOT NULL,
  type_sanction VARCHAR(255) CHECK (type_sanction IN ('retard', 'dégradation')) NOT NULL,
  nb_jours_retard INT,
  prix_a_payer FLOAT,
  etat_apres_rendu VARCHAR(255) CHECK (etat_apres_rendu IN ('bon', 'abîmé', 'perdu')),
  PRIMARY KEY (id, pret_exemplaire, pret_adherent),
  FOREIGN KEY (pret_date, pret_exemplaire, pret_adherent) REFERENCES PRET_JSON (date_pret, exemplaire, adherent),
  CONSTRAINT unique_pret_json_exemplaire_type UNIQUE (pret_exemplaire, pret_date, type_sanction),
  CHECK (
    (type_sanction = 'retard' AND nb_jours_retard IS NOT NULL AND etat_apres_rendu IS NULL AND prix_a_payer IS NULL) OR
    (type_sanction = 'dégradation' AND etat_apres_rendu IS NOT NULL AND nb_jours_retard IS NULL AND prix_a_payer IS NOT NULL) OR
    (type_sanction IS NULL AND nb_jours_retard IS NULL AND prix_a_payer IS NULL AND etat_apres_rendu IS NULL) OR
    (type_sanction = 'retard' AND nb_jours_retard IS NOT NULL) OR
    (type_sanction = 'dégradation' AND etat_apres_rendu IS NOT NULL AND prix_a_payer IS NOT NULL AND etat_apres_rendu = 'perdu')
  )
);

-- Nouvelles insertions

INSERT INTO Ressource_JSON (
  titre,
  date_apparition,
  editeur,
  genre,
  code_classification,
  type,
  ISBN,
  resume,
  langue,
  auteurs,
  exemplaires
) VALUES (
  'Les Misérables',
  '2021-01-15',
  'École des Loisirs',
  'Roman',
  'ABC123',
  'livre',
  '12345DR67890',
  "Les Misérables retrace le destin de Jean Valjean, ancien forçat condamné pour le vol de pain. Au fil de son histoire, le roman fait des détours pour se concentrer sur d\'autres personnages, comme Fantine ou Marius.",
  'Français',
  '[{"nom": "Hugo", "prenom": "Victor", "nationalite": "Française"}]',
  '[{"id": 9, "etat": "neuf"}]'
);

INSERT INTO Ressource_JSON (
  titre,
  date_apparition,
  editeur,
  genre,
  code_classification,
  type,
  ISBN,
  resume,
  langue,
  auteurs,
  exemplaires
) VALUES (
  'Les Trois Mousquetaires',
  '2023-02-19',
  'Le Livre de Poche',
  'Roman',
  'ABC123',
  'livre',
  '123456OP7891',
  'L histoire raconte les aventures du jeune, courageux et rusé gascon d Artagnan, qui arrive à Paris de sa province natale et qui se lie d amitié avec trois des mousquetaires du roi. Ils vont s unir pour déjouer les machinations du cardinal de Richelieu.',
  'Français',
  '[{"nom": "Dumas", "prenom": "Alexandre", "nationalite": "Française"}]',
  '[{"id": 10, "etat": "bon"},{"id": 11, "etat": "neuf"}]'
);

INSERT INTO Ressource_JSON  (
titre,
date_apparition, 
editeur,
genre, 
code_classification, 
type, 
synopsis, 
langue, 
longueur,
acteurs,
realisateurs,
exemplaires) 
VALUES (
'Hunger Games',
 '2023-03-10', 
'Studio Babelsberg',
'Action', 
'123XYZ', 
'film',
 'Dans un futur sombre, à Panem, un État totalitaire fondé sur les ruines des États-Unis, Katniss Everdeen, une jeune fille de 16 ans habitant dans le district 12, va combattre à mort dans une arène : les Hunger Games',
 'Anglais',
 '02:32:00',
 '[{"nom": "Lawrence", "prenom": "Jennifer", "nationalite": "USA"},{"nom": "Hutcherson", "prenom": "Josh", "nationalite": "USA"}]',
'[{"nom": "Lawrence", "prenom": "Francis", "nationalite": "USA"}]',
  '[{"id": 12, "etat": "bon"},{"id": 13, "etat": "perdu"}]');


INSERT INTO Ressource_JSON (
titre,
date_apparition, 
editeur, genre, 
code_classification, 
type, synopsis, 
langue, 
longueur,
acteurs,
realisateurs,
exemplaires) 
VALUES (
'Spider-Man', 
'2020-05-10', 
'Universal Studios',
'Fiction', '123UYO',
 'film', 
'Peter Parker, un jeune étudiant, subit une morsure d une araignée génétiquement modifiée, ce qui lui confère bientôt des pouvoirs extraordinaires : une force et une agilité surhumaines', 'Langue', 
'02:43:00',
 '[{"nom": "Holland", "prenom": "Tom", "nationalite": "USA"},{"nom": "Maguire", "prenom": "Tobey", "nationalite": "USA"}]',
'[{"nom": "Raimi", "prenom": "Sam", "nationalite": "USA"}]',
  '[{"id": 14, "etat": "abime"},{"id": 15, "etat": "neuf"}]');


INSERT INTO Ressource_JSON (
titre, 
date_apparition, 
editeur, genre, 
code_classification, 
type, longueur,
interpretes,
compositeurs,
exemplaires) 
VALUES (
'LDVM SYMPHONY', 
'2021-08-20', 
'Teta', 
'Rap', 
'XYZ456', 
'enregistrement_musical', 
'01:15:00',
 '[{"nom": "Maître", "prenom": "Gims", "nationalite": "Française"}]',
 '[{"nom": "Maître", "prenom": "Gims", "nationalite": "Française"}]',
  '[{"id": 16, "etat": "bon"},{"id": 17, "etat": "neuf"}]');

INSERT INTO Ressource_JSON (
titre, 
date_apparition, 
editeur, 
genre, 
code_classification, 
type, 
longueur,
interpretes,
compositeurs,
exemplaires) 
VALUES (
'Justified', 
'2012-02-21', 
'Jive Records', 
'Pop',
'XYZ478', 
'enregistrement_musical', 
'00:55:00',
'[{"nom": "Timberlake", "prenom": "Justin", "nationalite": "USA"}]',
'[{"nom": "Timberlake", "prenom": "Justin", "nationalite": "USA"}]',
'[{"id": 18, "etat": "neuf"}]');


INSERT INTO Ressource_JSON (
  titre,
  date_apparition,
  editeur,
  genre,
  code_classification,
  type,
  ISBN,
  resume,
  langue,
  auteurs,
  exemplaires
) VALUES (
  '1984',
  '1982-01-01',
  'Hatier',
  'Dystopique',
  'ABCT123',
  'livre',
  '1234JOSI5T997890',
  'Description',
  'Anglais',
  '{"nom": "Gorger", "prenom": "Orwell", "nationalite": "Anglais"}',
  '{"id": 8, "etat": "neuf"}'
);

INSERT INTO DemandePret_JSON(id, date_demande, adherent_login, code_ressource, statut)
VALUES(100, '2023-01-02', 'jeandupont1', 1, 'acceptee');
INSERT INTO DemandePret_JSON(id, date_demande, adherent_login, code_ressource, statut)
VALUES(101, '2023-02-15', 'insafsetitra1', 9, 'acceptee');
INSERT INTO DemandePret_JSON(id, date_demande, adherent_login, code_ressource, statut)
VALUES(102, '2023-03-20', 'alessandrocorrea1', 7, 'acceptee');
INSERT INTO DemandePret_JSON(id, date_demande, adherent_login, code_ressource, statut)
VALUES(103, '2023-04-25', 'alixbeslay1', 7, 'acceptee');
INSERT INTO DemandePret_JSON(id, date_demande, adherent_login, code_ressource, statut)
VALUES(104, '2023-05-30', 'davidbloquet1', 6, 'acceptee');

INSERT INTO PRET_JSON (
date_pret,
exemplaire, 
adherent, 
membre_personnel, 
date_retour, 
date_maximale,
demande_pret_id
) VALUES(
'2023-01-10', 
8, 
'jeandupont1',
'raniaattour1', 
'2023-01-20', 
'2023-02-10',
100
);




INSERT INTO PRET_JSON (
date_pret,
exemplaire, 
adherent, 
membre_personnel, 
date_retour, 
date_maximale,
demande_pret_id
) VALUES(
'2023-02-15', 
18, 
'insafsetitra1', 
'emmachoukroun1', 
'2023-03-01', ;
'2023-03-15',
101
);

INSERT INTO PRET_JSON (
date_pret,
exemplaire, 
adherent, 
membre_personnel, 
date_retour, 
date_maximale,
demande_pret_id
) VALUES(
'2023-03-20', 
16, 
'alessandrocorrea1', 
'remibouchard1', 
NULL, 
'2023-04-10',
102
);


INSERT INTO PRET_JSON (
date_pret,
exemplaire, 
adherent, 
membre_personnel, 
date_retour, 
date_maximale,
demande_pret_id
) VALUES(
'2023-04-25', 
17, 
'alixbeslay1', 
'arthurmartin1', 
NULL, 
'2023-05-15',
103
);



INSERT INTO PRET_JSON (
date_pret,
exemplaire, 
adherent, 
membre_personnel, 
date_retour, 
date_maximale,
demande_pret_id
) VALUES(
'2023-05-30', 
14, 
'davidbloquet1', 
'utc1', 
NULL, 
'2023-06-20',
104
);

-- Il y a également toutes les insertions que nous avons opéré depuis l'application Python
