import tkinter as tk
from tkinter import messagebox
import psycopg2
import os
from PIL import Image, ImageTk
from tkinter import Scrollbar
from tkinter import simpledialog, messagebox
from tkinter.ttk import Combobox
from datetime import datetime, timedelta
import json

class LibraryApp:
    def __init__(self, root):
        self.root = root
        self.root.title("Bibliothèque Municipale")
        icon_path = os.path.abspath("icone_biblio.ico")

        
        icon = Image.open(icon_path)
        icon = ImageTk.PhotoImage(icon)

        
        self.root.tk.call('wm', 'iconphoto', self.root._w, icon)

        self.connection = None
        self.global_username = tk.StringVar()

        self.create_login_screen()

    def create_login_screen(self):
        login_frame = tk.Frame(self.root)
        login_frame.pack(padx=10, pady=10)
        
        tk.Label(login_frame, text="Espace de connexion", font=("Helvetica", 12, "bold")).grid(row=0, column=0, columnspan=2)

        tk.Label(login_frame, text="Nom d'utilisateur:").grid(row=1, column=0, sticky="e", pady=10)
        self.username_entry = tk.Entry(login_frame)
        self.username_entry.grid(row=1, column=1)

        tk.Label(login_frame, text="Mot de passe:").grid(row=2, column=0, sticky="e")
        self.password_entry = tk.Entry(login_frame, show="*")
        self.password_entry.grid(row=2, column=1)

        login_button = tk.Button(login_frame, text="Connexion", font=("Helvetica", 10, "bold"), fg="green", command=self.authenticate_user)
        login_button.grid(row=3, column=1, pady=10)

    def authenticate_user(self):
        username = self.username_entry.get()
        password = self.password_entry.get()

        try:
            self.connection = psycopg2.connect(" dbname=dbnf18a012 host=tuxa.sme.utc user=nf18a012 password=1kRpG5zJYaso")
        except (Exception, psycopg2.Error) as error:
            messagebox.showerror("Erreur de connexion", f"Erreur lors de la connexion à la base de données : {error}")
            return

        result_adherent = self.execute_query(f"SELECT * FROM Adherent WHERE login = '{username}' AND mot_de_passe = '{password}';")
        result_personnel = self.execute_query(f"SELECT * FROM Membre_personnel WHERE login = '{username}' AND mot_de_passe = '{password}';")

        if result_adherent:
            self.global_username.set(username)
            self.show_user_menu("adherent")
        elif result_personnel:
            self.global_username.set(username)
            self.show_user_menu("personnel")
        elif username == "root" and password == "root":
            self.global_username.set(username)
            self.show_user_menu("root")
        else:
            messagebox.showerror("Erreur d'authentification", "Nom d'utilisateur ou mot de passe incorrect.")
    
    def is_adherent(self):
        return self.execute_query(f"SELECT COUNT(*) FROM Adherent WHERE login = '{self.global_username.get()}';", fetchall=False)[0] > 0
    
    def emprunts_possibles(self):
        return self.execute_query(f"SELECT COUNT(*) FROM PRET_JSON WHERE adherent = '{self.global_username.get()}' AND date_retour IS NULL;", fetchall=False)[0] < 5 
    
    def endette(self):
        return self.execute_query(f"SELECT SUM(prix_a_payer) FROM Sanction_JSON WHERE pret_adherent = '{self.global_username.get()}';", fetchall=False)[0] 
         
    def est_retard(self):
        return self.execute_query(f"SELECT COUNT(*) FROM PRET_JSON WHERE adherent = '{self.global_username.get()}' AND date_retour IS NULL AND date_maximale < CURRENT_DATE;", fetchall=False)[0] > 0

    def execute_query(self, query, values=(), fetchall=True):
        try:
            cursor = self.connection.cursor()
            cursor.execute(query, values)
            result = cursor.fetchall() if fetchall else cursor.fetchone()
            self.connection.commit()
            return result
        except (Exception, psycopg2.Error) as error:
            messagebox.showerror("Erreur de requête", f"Erreur lors de l'exécution de la requête : {error}")
            return None
        
    def execute_no_return_query(self, query, values=()):
        try:
            cursor = self.connection.cursor()
            cursor.execute(query, values)
            self.connection.commit()
        except (Exception, psycopg2.Error) as error:
            messagebox.showerror("Erreur de requête", f"Erreur lors de l'exécution de la requête : {error}")

    def show_user_menu(self, user_type):
        for widget in self.root.winfo_children():
            widget.destroy()

        main_frame = tk.Frame(self.root)
        main_frame.pack(padx=10, pady=10)

        tk.Label(main_frame, text=f"Bienvenue, {self.global_username.get()}!", font=("Helvetica", 16, "bold italic")).grid(row=0, column=0, columnspan=2, pady=10)

        menu_frame = tk.Frame(main_frame)
        menu_frame.grid(row=1, column=0, padx=10, pady=10)

        if user_type == "adherent":
            self.create_adherent_menu(menu_frame)
        elif user_type == "personnel":
            self.create_personnel_menu(menu_frame)
        elif user_type == "root":
            self.create_root_menu(menu_frame)

    def create_adherent_menu(self, frame):
        tk.Label(frame, text="Voir les ressources :", font=("Helvetica", 12)).grid(row=1, column=0, columnspan=5, pady=10)
        
        buttons_frame = tk.Frame(frame)
        buttons_frame.grid(row=2, column=0, columnspan=5, pady=5)

        button_width = 20  

        book_button = tk.Button(buttons_frame, text="Livres", command=self.show_books, width=button_width)
        book_button.grid(row=0, column=0, pady=5, padx=1)

        film_button = tk.Button(buttons_frame, text="Films", command=self.show_films, width=button_width)
        film_button.grid(row=0, column=1, pady=5, padx=1)

        music_button = tk.Button(buttons_frame, text="Musiques", command=self.show_music, width=button_width)
        music_button.grid(row=0, column=2, pady=5, padx=1)

        new_button = tk.Button(buttons_frame, text="Nouveautés", command=self.show_ressources_neuves, width=button_width)
        new_button.grid(row=0, column=3, pady=5, padx=1)

        top_ressources_button = tk.Button(buttons_frame, text="Top 10", command=self.show_top_ressources, width=button_width)
        top_ressources_button.grid(row=0, column=4, pady=5, padx=1)

        tk.Label(frame, text="Voir mes prêts :", font=("Helvetica", 12)).grid(row=5, column=0, columnspan=5, pady=10)

        demand_button = tk.Button(frame, text="Demandes d'emprunts", command=self.display_demandes, width=button_width)
        demand_button.grid(row=6, column=1, columnspan=2, pady=5, padx=1)

        loan_button = tk.Button(frame, text="Prêts en cours", command=self.show_emprunt_cours, width=button_width)
        loan_button.grid(row=6, column=1, columnspan=2, pady=5, padx=1)
        
        historique_button = tk.Button(frame, text="Historique", command=self.show_historique_emprunt, width=button_width)
        historique_button.grid(row=6, column=2, columnspan=2, pady=5, padx=1)
        
        tk.Label(frame, text="Voir mes sanctions :", font=("Helvetica", 12)).grid(row=7, column=0, columnspan=5, pady=10)

        demand_button = tk.Button(frame, text="Toutes mes sanctions", command=self.show_sanction_adherent, width=button_width)
        demand_button.grid(row=8, column=1, columnspan=3, pady=5, padx=1)

        password_button = tk.Button(frame, text="Nouveau mot de passe", font=("Helvetica", 10, "bold"), fg="blue", command=self.modify_password, width=button_width)
        password_button.grid(row=10, column=1, columnspan=2, pady=50)
        
        exit_button = tk.Button(frame, text="Déconnexion", font=("Helvetica", 10, "bold"), fg="red", command=self.fin_programe, width=button_width)
        exit_button.grid(row=10, column=2, columnspan=2, pady=50)

    def create_personnel_menu(self, frame):
        self.change()
        tk.Label(frame, text="Gérer les ressources :", font=("Helvetica", 12)).grid(row=1, column=0, columnspan=5, pady=10)
        
        buttons_frame = tk.Frame(frame)
        buttons_frame.grid(row=2, column=0, columnspan=5, pady=5)

        button_width = 20  

        book_button = tk.Button(buttons_frame, text="Livres", command=self.show_books, width=button_width)
        book_button.grid(row=0, column=0, pady=5, padx=1)

        film_button = tk.Button(buttons_frame, text="Films", command=self.show_films, width=button_width)
        film_button.grid(row=0, column=1, pady=5, padx=1)

        music_button = tk.Button(buttons_frame, text="Musiques", command=self.show_music, width=button_width)
        music_button.grid(row=0, column=2, pady=5, padx=1)

        new_button = tk.Button(buttons_frame, text="Nouveautés", command=self.show_ressources_neuves, width=button_width)
        new_button.grid(row=0, column=3, pady=5, padx=1)

        top_ressources_button = tk.Button(buttons_frame, text="Top 10", command=self.show_top_ressources, width=button_width)
        top_ressources_button.grid(row=0, column=4, pady=5, padx=1)

        tk.Label(frame, text="Gérer les prêts :", font=("Helvetica", 12)).grid(row=5, column=0, columnspan=5, pady=10)

        demand_button = tk.Button(frame, text="Demandes d'emprunts", command=self.display_demandes, width=button_width)
        demand_button.grid(row=6, column=1, pady=5, padx=1)

        loan_button = tk.Button(frame, text="Prêts en cours", command=self.show_current_loans, width=button_width)
        loan_button.grid(row=6, column=2, pady=5, padx=1)
        
        historic_button = tk.Button(frame, text="Historique des prêts", command=self.show_historique, width=button_width)
        historic_button.grid(row=6, column=3, pady=5, padx=1)

        tk.Label(frame, text="Gérer les adhérents :", font=("Helvetica", 12)).grid(row=7, column=0, columnspan=5, pady=10)

        adherent_button = tk.Button(frame, text="Voir les adhérents", command=self.show_adherents, width=button_width)
        adherent_button.grid(row=8, column=0, pady=5, padx=1)
        
        adherent_button = tk.Button(frame, text="Toutes les sanctions", command=self.show_sanctions, width=button_width)
        adherent_button.grid(row=8, column=1, pady=5, padx=1)

        somme_button = tk.Button(frame, text="Somme dûe par adhérent", command=self.somme_argent, width=button_width)
        somme_button.grid(row=8, column=2, pady=5, padx=1)

        retard_button = tk.Button(frame, text="Retards", command=self.retard, width=button_width)
        retard_button.grid(row=8, column=3, pady=5, padx=1)

        degradation_button = tk.Button(frame, text="Dégradations", command=self.degradation, width=button_width)
        degradation_button.grid(row=8, column=4, pady=5, padx=1)

        password_button = tk.Button(frame, text="Nouveau mot de passe", font=("Helvetica", 10, "bold"), fg="blue", command=self.modify_password_personnel, width=button_width)
        password_button.grid(row=10, column=1, columnspan=2, pady=50)
        
        exit_button = tk.Button(frame, text="Déconnexion", font=("Helvetica", 10, "bold"), fg="red", command=self.fin_programe, width=button_width)
        exit_button.grid(row=10, column=2, columnspan=2, pady=50)

    def create_root_menu(self, frame):
        tk.Label(frame, text="Menu root", font=("Helvetica", 14)).grid(row=0, column=0, columnspan=2, pady=10)

        create_table_button = tk.Button(frame, text="Créer une table", command=self.create_table)
        create_table_button.grid(row=2, column=0, pady=5, padx=10)

        delete_table_button = tk.Button(frame, text="Supprimer une table", command=self.delete_table)
        delete_table_button.grid(row=3, column=0, pady=5, padx=10)

        exit_button = tk.Button(frame, text="Quitter", command=self.fin_programe)
        exit_button.grid(row=15, column=0, pady=5, padx=10)
        
    def modify_password(self):
        def update_password(current_password, new_password, modify_password_window):
            check_query = "SELECT mot_de_passe FROM Adherent WHERE login = %s"
            check_result = self.execute_query(check_query, (self.global_username.get(),))

            if check_result and len(check_result) > 0:
                if check_result[0][0] == current_password:
                    update_query = "UPDATE Adherent SET mot_de_passe = %s WHERE login = %s"
                    self.execute_no_return_query(update_query, (new_password, self.global_username.get()))
                    self.connection.commit()
                    messagebox.showinfo("Succès", "Le mot de passe a été mis à jour avec succès.")
                    modify_password_window.destroy() 
                else:
                    messagebox.showerror("Erreur", "Mot de passe actuel incorrect.")
            else:
                messagebox.showerror("Erreur", "Erreur lors de la vérification du mot de passe actuel.")

        modify_password_window = tk.Toplevel(self.root)
        modify_password_window.title("Modifier le mot de passe")

        current_password_label = tk.Label(modify_password_window, text="Mot de passe actuel:")
        current_password_label.grid(row=0, column=0, padx=10, pady=5, sticky=tk.W)

        current_password_entry = tk.Entry(modify_password_window, show="*")
        current_password_entry.grid(row=0, column=1, padx=10, pady=5, sticky=tk.W)

        new_password_label = tk.Label(modify_password_window, text="Nouveau mot de passe:")
        new_password_label.grid(row=1, column=0, padx=10, pady=5, sticky=tk.W)

        new_password_entry = tk.Entry(modify_password_window, show="*")
        new_password_entry.grid(row=1, column=1, padx=10, pady=5, sticky=tk.W)

        validate_button = tk.Button(modify_password_window, text="Valider", command=lambda: update_password(current_password_entry.get(), new_password_entry.get(), modify_password_window))
        validate_button.grid(row=2, column=0, columnspan=2, pady=10)
        
    def modify_password_personnel(self):
        def update_password(current_password, new_password, modify_password_window):
            check_query = "SELECT mot_de_passe FROM Membre_personnel WHERE login = %s"
            check_result = self.execute_query(check_query, (self.global_username.get(),))

            if check_result and len(check_result) > 0:
                if check_result[0][0] == current_password:
                    update_query = "UPDATE Membre_personnel SET mot_de_passe = %s WHERE login = %s"
                    self.execute_no_return_query(update_query, (new_password, self.global_username.get()))
                    self.connection.commit()
                    messagebox.showinfo("Succès", "Le mot de passe a été mis à jour avec succès.")
                    modify_password_window.destroy() 
                else:
                    messagebox.showerror("Erreur", "Mot de passe actuel incorrect.")
            else:
                messagebox.showerror("Erreur", "Erreur lors de la vérification du mot de passe actuel.")

        modify_password_window = tk.Toplevel(self.root)
        modify_password_window.title("Modifier le mot de passe")

        current_password_label = tk.Label(modify_password_window, text="Mot de passe actuel:")
        current_password_label.grid(row=0, column=0, padx=10, pady=5, sticky=tk.W)

        current_password_entry = tk.Entry(modify_password_window, show="*")
        current_password_entry.grid(row=0, column=1, padx=10, pady=5, sticky=tk.W)

        new_password_label = tk.Label(modify_password_window, text="Nouveau mot de passe:")
        new_password_label.grid(row=1, column=0, padx=10, pady=5, sticky=tk.W)

        new_password_entry = tk.Entry(modify_password_window, show="*")
        new_password_entry.grid(row=1, column=1, padx=10, pady=5, sticky=tk.W)

        validate_button = tk.Button(modify_password_window, text="Valider", command=lambda: update_password(current_password_entry.get(), new_password_entry.get(), modify_password_window))
        validate_button.grid(row=2, column=0, columnspan=2, pady=10)
        
    def show_ressources_neuves(self):
            result = self.execute_query("SELECT R.* FROM Ressource_JSON AS R, JSON_ARRAY_ELEMENTS(R.exemplaires) AS ex WHERE ex->>'etat' = 'neuf';")
            self.display_all_ressources(result, "Liste des ressources neuves")

    def display_all_ressources(self, result, title):
        popup = tk.Toplevel(self.root)
        popup.title(title)
        
        if result:
            for row in result:
                if row[6]=="film":
                    title_label = tk.Label(popup, text=row[1], font=("Helvetica", 12, "bold"))
                    title_label.grid(padx=10, pady=5, sticky="w")

                    details_label = tk.Label(popup, text=f"Date de sortie : {row[2]}, Production : {row[3]}, Genre : {row[4]}", font=("Helvetica", 10, "italic"))
                    details_label.grid(padx=10, pady=2, sticky="w")

                    description_label = tk.Label(popup, text=row[10])
                    description_label.grid(padx=10, pady=2, sticky="w")

                    duration_label = tk.Label(popup, text=f"Durée : {row[11]}", font=("Helvetica", 10, "italic"))
                    duration_label.grid(padx=10, pady=2, sticky="w")

                    separator = tk.Label(popup, text="-" * 50)
                    separator.grid(padx=10, pady=5, sticky="w")
                    
                elif row[6]=="livre":
                    title_label = tk.Label(popup, text=row[1], font=("Helvetica", 12, "bold"))
                    title_label.grid(padx=10, pady=5, sticky="w")

                    details_label = tk.Label(popup, text=f"{row[2]}, {row[3]}, {row[4]}", font=("Helvetica", 10, "italic"))
                    details_label.grid(padx=10, pady=2, sticky="w")

                    description_label = tk.Label(popup, text=row[8])
                    description_label.grid(padx=10, pady=2, sticky="w")

                    language_label = tk.Label(popup, text=f"Langue : {row[9]}", font=("Helvetica", 10, "italic"))
                    language_label.grid(padx=10, pady=2, sticky="w")

                    separator = tk.Label(popup, text="-" * 50)
                    separator.grid(padx=10, pady=5, sticky="w")
                    
                elif row[6]=="enregistrement_musical":
                    title_label = tk.Label(popup, text=row[1], font=("Helvetica", 12, "bold"))
                    title_label.grid(padx=10, pady=5, sticky="w")

                    details_label = tk.Label(popup, text=f"Date de sortie : {row[2]}, Studio : {row[3]}, Genre : {row[4]}", font=("Helvetica", 10, "italic"))
                    details_label.grid(padx=10, pady=2, sticky="w")

                    duration_label = tk.Label(popup, text=f"Durée : {row[11]}", font=("Helvetica", 10, "italic"))
                    duration_label.grid(padx=10, pady=2, sticky="w")

                    separator = tk.Label(popup, text="-" * 50)
                    separator.grid(padx=10, pady=5, sticky="w")
                else:
                    tk.Label(popup, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)


    def show_all_resources(self):
        result = self.execute_query("SELECT * FROM Ressource")
        self.display_all_ressources(result, "Liste des ressources")

    def show_current_loans(self):
        loans = self.get_current_loans()
            
        popup = tk.Toplevel(self.root)
        popup.title("Prêts en cours")

        canvas = tk.Canvas(popup)
        canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        scrollbar_y = tk.Scrollbar(popup, orient=tk.VERTICAL, command=canvas.yview)
        scrollbar_y.pack(side=tk.RIGHT, fill=tk.Y)
        canvas.configure(yscrollcommand=scrollbar_y.set)

        frame = tk.Frame(canvas)
        canvas.create_window((0, 0), window=frame, anchor=tk.NW)

        if loans:
            for loan in loans:
                
                adherent_label = tk.Label(frame, text=f"Adhérent: {loan[5]} {loan[6]}, login : {loan[0]}", wraplength=300)
                adherent_label.grid(padx=10, pady=5, sticky="w")
                
                ressource_label = tk.Label(frame, text=f"Ressource: {loan[7]}", wraplength=300)
                ressource_label.grid(padx=10, pady=5, sticky="w")
                
                date_label = tk.Label(frame, text=f"Date d'emprunt: {loan[3]}", wraplength=300)
                date_label.grid(padx=10, pady=5, sticky="w")

                btn_return = tk.Button(frame, text="Rendu", command=lambda l=loan: self.handle_return(l))
                btn_return.grid(padx=10, pady=5, sticky="w")

                separator = tk.Label(frame, text="-" * 50)
                separator.grid(padx=10, pady=5, sticky="w")
        else:
            tk.Label(frame, text="Aucun prêt en cours").grid(row=0, column=0, padx=10, pady=5)

        canvas.bind_all("<MouseWheel>", lambda event: canvas.yview_scroll(int(-1 * (event.delta / 120)), "units"))

        frame.update_idletasks()
        canvas.config(scrollregion=canvas.bbox("all"))

    def get_current_loans(self):
        current_loans_query = """
                SELECT 
                    p.adherent, r_subquery.code, p.exemplaire, p.date_pret, p.date_maximale, a.nom, a.prenom, r_subquery.titre 
                FROM PRET_JSON AS p 
                JOIN Adherent AS a ON p.adherent = a.login 
                JOIN(
                    SELECT
                        R.code,
                        CAST(json_array_elements(R.exemplaires)->>'id' AS INTEGER) AS exemplaire_id,
                        R.titre
                    FROM
                        Ressource_JSON AS R
                ) AS r_subquery ON r_subquery.exemplaire_id = p.exemplaire 
                WHERE date_retour IS NULL;"""
        current_loans = self.execute_query(current_loans_query)
        return current_loans

    def handle_return(self, loan):
        options = ['bon', 'abime', 'perdu']

        dialog = tk.Toplevel()
        dialog.title("État de rendu")

        state = tk.StringVar()

        state_combobox = Combobox(dialog, textvariable=state, values=options)
        state_combobox.grid(row=0, column=0, padx=10, pady=10)

        state.set(options[0])

        def confirm_selection():
            dialog.destroy()

        confirm_button = tk.Button(dialog, text="Valider", command=confirm_selection)
        confirm_button.grid(row=1, column=0, padx=10, pady=10)

        dialog.wait_window(dialog)

        state = state.get()
        
        if state:
            if state in ["abime", "perdu"]:
                price_to_pay = simpledialog.askfloat("Montant à payer", "Montant à payer:")
                if price_to_pay is not None:
                    self.process_return(loan, state, price_to_pay)
            else:
                self.process_return(loan, state)
                
    def process_return(self, loan, state, price_to_pay=None):
        current_date = datetime.now().date()

        days_delayed = (current_date - loan[4]).days

        if days_delayed > 0:
            self.apply_delayed_sanction(loan, days_delayed)

        self.update_loan_return(loan, state, price_to_pay)

        self.show_current_loans()
        
    def apply_delayed_sanction(self, loan, days_delayed):
        try:
            query = f"INSERT INTO Sanction_JSON (pret_date, pret_exemplaire, pret_adherent, type_sanction, nb_jours_retard) VALUES ('{loan[3]}', {loan[2]}, '{loan[0]}', 'retard', {days_delayed});"
            self.execute_no_return_query(query)

        except (Exception, psycopg2.Error) as error:
            print(f"Erreur lors de l'application de la sanction pour retard : {error}")
            
    def change(self):
        pass
            
    def update_loan_return(self, loan, state, price_to_pay):
        try:
            result = self.execute_query("""
                SELECT exemplaires FROM Ressource_JSON WHERE code = %s
            """, (loan[1],))[0][0]
            if (isinstance(result, dict)):
                result["etat"] = f"{state}"
            else:
                for e in result:
                    if e["id"] == loan[2]:
                        e["etat"] = f"{state}"
            updated_exemplaires = json.dumps(result)

            self.execute_no_return_query("""
                UPDATE Ressource_JSON 
                SET exemplaires = %s 
                WHERE code = %s;
            """, (updated_exemplaires, loan[1]))
            
            if state in ('abime', 'perdu'):
                
                if state == 'abime': 
                    state = 'abîmé'

                self.execute_no_return_query(f"INSERT INTO Sanction_JSON (pret_date, pret_exemplaire, pret_adherent, type_sanction, prix_a_payer, etat_apres_rendu) VALUES ('{loan[3]}', {loan[2]}, '{loan[0]}', 'dégradation', {price_to_pay}, '{state}');")

            
            self.execute_no_return_query(f"UPDATE PRET_JSON SET date_retour = CURRENT_DATE  WHERE date_pret = '{loan[3]}' AND exemplaire = {loan[2]} AND adherent = '{loan[0]}';")

        except (Exception, psycopg2.Error) as error:
            print(f"Erreur lors de la mise à jour du rendu de la ressource : {error}")
    
    def show_books(self):
        
        result = self.execute_query("SELECT * FROM Ressource_JSON WHERE type='livre';")
        self.display_books(result, "Liste des livres")

    def display_books(self, result, title, show_delete_button=True):
        
        popup = tk.Toplevel(self.root)
        popup.title(title)

        
        canvas = tk.Canvas(popup)
        canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        
        scrollbar_y = tk.Scrollbar(popup, orient=tk.VERTICAL, command=canvas.yview)
        scrollbar_y.pack(side=tk.RIGHT, fill=tk.Y)
        canvas.configure(yscrollcommand=scrollbar_y.set)

        
        frame = tk.Frame(canvas)
        canvas.create_window((0, 0), window=frame, anchor=tk.NW)
        
        if not self.is_adherent():
            add_book_button = tk.Button(frame, text="Ajouter un livre", command=self.add_book)
            add_book_button.grid(row=0, column=0, padx=10, pady=5, sticky="w")

        if result:
            for row in result:
                title_label = tk.Label(frame, text=row[1], font=("Helvetica", 12, "bold"), wraplength=300)  
                title_label.grid(padx=10, pady=5, sticky="w")
                
                if (isinstance(row[14], dict)):
                    auteurs = "Auteurs : "
                    auteurs = auteurs + row[14]['nom'] + " "
                    auteurs = auteurs + row[14]['prenom']
                else:
                    auteurs = "Auteurs : "
                    for auteur in row[14]:
                        auteurs = auteurs + auteur['nom'] + " "
                        auteurs = auteurs + auteur['prenom'] + ", "
                
                authors_label = tk.Label(frame, text=f"{auteurs}", font=("Helvetica", 10, "italic"), wraplength=300)  
                authors_label.grid(padx=10, pady=2, sticky="w")

                details_label = tk.Label(frame, text=f"{row[2]}, {row[3]}, {row[4]}", font=("Helvetica", 10, "italic"), wraplength=300)  
                details_label.grid(padx=10, pady=2, sticky="w")

                description_label = tk.Label(frame, text=row[8], wraplength=300)  
                description_label.grid(padx=10, pady=2, sticky="w")
                
                if not self.is_adherent():
                    edit_button = tk.Button(frame, text="Modifier le résumé", command=lambda code=row[0]: self.edit_book(code))
                    edit_button.grid(pady=5, padx=10, sticky="w")

                language_label = tk.Label(frame, text=f"Langue : {row[9]}", font=("Helvetica", 10, "italic"), wraplength=300)  
                language_label.grid(padx=10, pady=2, sticky="w")
                
                nb_exemplaires = 0
                if (isinstance(row[17], dict)):
                    nb_exemplaires = 1
                else:
                    for e in row[17]:
                        nb_exemplaires += 1
                
                exemplar_count_label = tk.Label(frame, text=f"Nombre d'exemplaires : {nb_exemplaires}")
                exemplar_count_label.grid(padx=10, pady=2, sticky="w")

                if not self.is_adherent():
                    add_exemplar_button = tk.Button(frame, text="Ajouter un exemplaire", command=lambda code=row[0]: self.add_exemplar(code))
                    add_exemplar_button.grid(padx=10, pady=5, sticky="w")
                
                if self.is_adherent():
                    disponibilite = self.verifier_disponibilite(row[0])
                    demande_en_attente = self.verifier_demande_en_attente(row[0])
                    
                    if not self.emprunts_possibles():
                        impossible_label = tk.Label(frame, text="Emprunt impossible : 5 emprunts déjà en cours", fg="red", wraplength=300)
                        impossible_label.grid(padx=10, pady=2, sticky="w")
                    elif self.endette():
                        endette_label = tk.Label(frame, text="Emprunt impossible : vous devez encore de l'argent à la bibliothèque", fg="red", wraplength=300)
                        endette_label.grid(padx=10, pady=2, sticky="w")
                    elif self.est_retard():
                        retard_label = tk.Label(frame, text="Emprunt impossible : vous avez un prêt non rendu en retard", fg="red", wraplength=300)
                        retard_label.grid(padx=10, pady=2, sticky="w")
                    elif demande_en_attente:
                        attente_label = tk.Label(frame, text="Demande d'emprunt envoyée", fg="green", wraplength=300)
                        attente_label.grid(padx=10, pady=2, sticky="w")
                    elif disponibilite:
                        print(row[0])
                        emprunt_button = tk.Button(frame, text="Emprunter", command=lambda code=row[0]: self.effectuer_demande_pret(code))
                        emprunt_button.grid(padx=10, pady=5, sticky="w")
                    else:
                        indisponible_label = tk.Label(frame, text="Emprunt indisponible", fg="orange", wraplength=300)
                        indisponible_label.grid(padx=10, pady=2, sticky="w")

                if not self.is_adherent():
                    delete_button = tk.Button(frame, text="Supprimer", command=lambda code=row[0]: self.delete_book(code))
                    delete_button.grid(pady=5, padx=10, sticky="w")

                separator = tk.Label(frame, text="-" * 50)
                separator.grid(padx=10, pady=5, sticky="w")
        else:
            tk.Label(frame, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)

        
        canvas.bind_all("<MouseWheel>", lambda event: canvas.yview_scroll(int(-1 * (event.delta / 120)), "units"))

        
        frame.update_idletasks()
        canvas.config(scrollregion=canvas.bbox("all"))
        
    def verifier_demande_en_attente(self, code_ressource):
        
        query = "SELECT COUNT(*) FROM DemandePret_JSON WHERE adherent_login = %s AND code_ressource = %s AND statut = 'en_attente'"
        result = self.execute_query(query, (self.global_username.get(), code_ressource))
        return result[0][0] > 0
    
    def verifier_disponibilite(self, code_ressource):
        query = "SELECT exemplaires FROM Ressource_JSON WHERE code = %s;"
        result = self.execute_query(query, (code_ressource, ))[0][0]
        nb_exemplaires_dispo = 0
        if (isinstance(result, dict)):
            if (result["etat"] == "neuf" or result["etat"] == "bon"):
                nb_exemplaires_dispo += 1
        else:
            for e in result:
                if (e["etat"] == "neuf" or e["etat"] == "bon"):
                    nb_exemplaires_dispo += 1
                    
        query = "SELECT COUNT(*) FROM PRET_JSON AS P JOIN DemandePret_JSON AS D ON P.demande_pret_id = D.id WHERE D.code_ressource = %s;"
        result = self.execute_query(query, (code_ressource, ))[0][0]
        return nb_exemplaires_dispo > result
    
    def effectuer_demande_pret(self, code_ressource):
        
        if self.verifier_demande_en_attente(code_ressource):
            messagebox.showinfo("Demande en attente", "Vous avez déjà une demande d'emprunt en attente pour cette ressource.")
            return

        
        query_insert = "INSERT INTO DemandePret_JSON (date_demande, adherent_login, code_ressource, statut) VALUES (CURRENT_DATE, %s, %s, 'en_attente')"
        self.execute_no_return_query(query_insert, (self.global_username.get(), code_ressource))
        self.connection.commit()

        
        messagebox.showinfo("Demande d'emprunt envoyée", "Votre demande d'emprunt a été envoyée avec succès.")

    def obtenir_demandes_en_attente(self):
        
        query = """
            SELECT dp.id, dp.code_ressource, dp.adherent_login, a.nom AS adherent_nom, a.prenom AS adherent_prenom,
                   COALESCE(s.prix_a_payer, 0) AS sanctions, COALESCE(a.est_blacklist, FALSE) AS est_blacklist
            FROM DemandePret_JSON dp
            JOIN Adherent a ON dp.adherent_login = a.login
            LEFT JOIN Sanction_JSON s ON dp.adherent_login = s.pret_adherent AND dp.code_ressource = s.pret_exemplaire
            WHERE dp.statut = 'en_attente'
        """
        result = self.execute_query(query)
        demandes_en_attente = []

        for row in result:
            demande = {
                'id': row[0],
                'code_ressource': row[1],
                'adherent_login': row[2],
                'adherent_nom': row[3],
                'adherent_prenom': row[4],
                'sanctions': row[5],
                'est_blacklist': row[6],
            }
            demandes_en_attente.append(demande)

        return demandes_en_attente

    def display_demandes(self):
        
        popup = tk.Toplevel(self.root)
        popup.title("Gestion des demandes d'emprunts")

        
        canvas = tk.Canvas(popup)
        canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        
        scrollbar_y = tk.Scrollbar(popup, orient=tk.VERTICAL, command=canvas.yview)
        scrollbar_y.pack(side=tk.RIGHT, fill=tk.Y)
        canvas.configure(yscrollcommand=scrollbar_y.set)

        
        frame = tk.Frame(canvas)
        canvas.create_window((0, 0), window=frame, anchor=tk.NW)
        
        demandes_en_attente = self.obtenir_demandes_en_attente()

        if demandes_en_attente != []:
            for demande in demandes_en_attente:
                adherent_label = tk.Label(frame, text=f"{self.execute_query('SELECT titre FROM Ressource_JSON WHERE code = %s;', (demande['code_ressource'],))[0][0]}", font=("Helvetica", 12, "bold"), wraplength=300)  
                adherent_label.grid(padx=10, pady=2, sticky="w")
                
                adherent_label = tk.Label(frame, text=f"Adhérent : {demande['adherent_nom']} {demande['adherent_prenom']}", wraplength=300)  
                adherent_label.grid(padx=10, pady=2, sticky="w")

                sanction_label = tk.Label(frame, text=f"Sanctions : {demande['sanctions']}", wraplength=300)  
                sanction_label.grid(padx=10, pady=2, sticky="w")
                
                if demande['est_blacklist']:
                    blacklist_label = tk.Label(frame, text=f"BLACKLISTÉ", font=("Helvetica", 12, "bold"), fg="red", wraplength=300)  
                    blacklist_label.grid(padx=10, pady=2, sticky="w")

                accept_button = tk.Button(frame, text="Accepter", command=lambda d=demande: self.accepter_demande(d))
                accept_button.grid(pady=5, padx=10, sticky="w")
                
                accept_button = tk.Button(frame, text="Refuser", command=lambda d=demande: self.refuser_demande(d))
                accept_button.grid(pady=5, padx=10, sticky="w")

                separator = tk.Label(frame, text="-" * 50)
                separator.grid(padx=10, pady=5, sticky="w")
        else:
            tk.Label(frame, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)

        
        canvas.bind_all("<MouseWheel>", lambda event: canvas.yview_scroll(int(-1 * (event.delta / 120)), "units"))

        
        frame.update_idletasks()
        canvas.config(scrollregion=canvas.bbox("all"))
        
    def accepter_demande(self, demande):
        
        id_demande = demande['id']
        adherent_login = demande['adherent_login']
        code_ressource = demande['code_ressource']

        try:
            
            exemplaire_disponible = self.get_premier_exemplaire_disponible(code_ressource)

            if exemplaire_disponible:
                
                date_pret = datetime.now().date()

                
                date_maximale = date_pret + timedelta(days=14)  

                
                self.creer_pret(date_pret, exemplaire_disponible, adherent_login, date_maximale, id_demande)

                
                self.met_a_jour_statut_demande(id_demande, 'acceptee')

                
                messagebox.showinfo("Demande acceptée", "La demande a été acceptée. Un prêt a été créé.")
            else:
                
                messagebox.showwarning("Aucun exemplaire disponible", "Il n'y a pas d'exemplaire disponible en bon état ou neuf.")

        except Exception as e:
            
            messagebox.showerror("Erreur", f"Erreur lors de l'acceptation de la demande : {e}")

    def get_premier_exemplaire_disponible(self, code_ressource):
        
        query = "SELECT exemplaires FROM Ressource_JSON WHERE code = %s;"
        result = self.execute_query(query, (code_ressource, ))[0][0]
        id_exemplaire = None
        if (isinstance(result, dict)):
            if result["etat"] == "bon" or result["etat"] == "neuf":
                id_exemplaire = result["id"]                
        else:
            for e in result:
                if e["etat"] == "bon" or e["etat"] == "neuf":
                    id_exemplaire = e["id"]
                    query = "SELECT COUNT(*) FROM PRET_JSON WHERE exemplaire = %s AND date_retour < CURRENT_DATE"
                    res = self.execute_query(query, (code_ressource,))[0][0]
                    if res == 0:
                        break
                    id_exemplaire = None        
        return id_exemplaire

    def creer_pret(self, date_pret, exemplaire_id, adherent_login, date_maximale, demande):
        
        query = f"INSERT INTO PRET_JSON (date_pret, exemplaire, adherent, membre_personnel, date_maximale, demande_pret_id) VALUES (%s, %s, %s, '{self.global_username.get()}', %s, %s)"
        self.execute_no_return_query(query, (date_pret, exemplaire_id, adherent_login, date_maximale, demande))
        self.connection.commit()
        
    def refuser_demande(self, demande):
        
        code_ressource = demande['id']

        
        self.met_a_jour_statut_demande(code_ressource, 'refusee')

        messagebox.showinfo("Demande refusée", "La demande a été refusée.")

    def met_a_jour_statut_demande(self, code_ressource, nouveau_statut):
        
        query = "UPDATE DemandePret_JSON SET statut = %s WHERE id = %s"
        self.execute_no_return_query(query, (nouveau_statut, code_ressource))
        self.connection.commit()
        
    def get_exemplar_count(self, book_code):        
        query = "SELECT exemplaires FROM Ressource_JSON WHERE code = %s"
        result = self.execute_query(query, (book_code,))[0][0]
        nb = 0
        if (isinstance(result, dict)):
            nb = 1
        else:
            for e in result:
                nb += 1
        return nb
    
    def get_good_condition_exemplar_count(self, book_code):
        query = "SELECT exemplaires FROM Ressource_JSON WHERE code = %s"
        result = self.execute_query(query, (book_code,))[0][0]
        nb = 0
        if (isinstance(result, dict)):
            if result["etat"] in {'bon', 'neuf'}:
                nb = 1
        else:
            for e in result:
                if result["etat"] in {'bon', 'neuf'}:
                    nb += 1
        return nb

    def add_exemplar(self, book_code):
        query = "SELECT exemplaires FROM Ressource_JSON WHERE code = %s"
        result = self.execute_query(query, (book_code,))[0][0]
        if (isinstance(result, dict)):
            new_id = book_code*100 + result["id"] + 1
        else:
            maxi = 0
            for e in result:
                if e["id"] > maxi:
                    maxi = e["id"]
            new_id = book_code * 100 + maxi + 1
        etat = simpledialog.askstring("Ajouter un exemplaire", "État de l'exemplaire (neuf, bon, abime, perdu):")
        
        if etat is not None and etat in ('neuf', 'bon', 'abime', 'perdu'):
            if (isinstance(result, dict)):
                result = [result, {"id":new_id, "etat":f"{etat}"}]
            else:
                result.append({"id":new_id, "etat":f"{etat}"})

            try:
                result = json.dumps(result)
                query = f"UPDATE Ressource_JSON SET exemplaires = '{result}' WHERE code = {book_code}"
                self.execute_no_return_query(query)
                messagebox.showinfo("Succès", "Exemplaire ajouté avec succès.")
                result = self.execute_query(f"SELECT type FROM Ressource_JSON WHERE code = {book_code}")[0][0]
                if (result == 'livre'):
                    self.show_books()
                if (result == 'film'):
                    self.show_films()
                if (result == 'enregistrement_musical'):
                    self.show_music()
            except Exception as e:
                messagebox.showerror("Erreur", "Erreur lors de l'ajout de l'exemplaire.")
        
    def edit_book(self, book_code):
        existing_description = self.get_existing_description(book_code)

        
        def apply_changes():
            new_description = text_widget.get("1.0", tk.END).strip()

            if new_description is not None:
                
                success = self.update_description(book_code, new_description)

                if success:
                    messagebox.showinfo("Succès", "Description mise à jour avec succès.")
                    self.show_books()
                else:
                    messagebox.showerror("Erreur", "Erreur lors de la mise à jour de la description.")
            
            dialog.destroy()

        
        dialog = tk.Toplevel(self.root)
        dialog.title("Modifier le résumé")

        
        text_widget = tk.Text(dialog, height=10, width=40)
        text_widget.pack()

        
        text_widget.insert(tk.END, existing_description)

        
        apply_button = tk.Button(dialog, text="Appliquer les changements", command=apply_changes)
        apply_button.pack()
                
    def get_existing_description(self, book_code):
        
        query = "SELECT resume FROM Ressource_JSON WHERE code = %s"        
        result = self.execute_query(query, (book_code,))

        
        if result:
            return result[0][0]
        else:
            return None

    def update_description(self, book_code, new_description):
        
        query = "UPDATE Ressource_JSON SET resume = %s WHERE code = %s"

        try:
            self.execute_no_return_query(query, (new_description, book_code))
            self.connection.commit()
            return True
        except Exception as e:
            print(f"Erreur lors de la mise à jour de la description : {e}")
            self.connection.rollback()
            return False
        
    def delete_book(self, book_code):       
        delete_query = "DELETE FROM Ressource_JSON WHERE code = %s;"
        self.execute_no_return_query(delete_query, (book_code,))

        self.connection.commit()
        messagebox.showinfo("Succès", "Livre supprimé avec succès.")
        self.show_books()

    def add_book(self):
        
        book_title = simpledialog.askstring("Ajouter un livre", "Titre du livre:")
        if not book_title:
            return  

        
        date_apparition = simpledialog.askstring("Ajouter un livre", "Date d'apparition (AAAA-MM-JJ):")
        editeur = simpledialog.askstring("Ajouter un livre", "Éditeur:")
        genre = simpledialog.askstring("Ajouter un livre", "Genre:")
        code_classification = simpledialog.askstring("Ajouter un livre", "Code de classification:")
        isbn = simpledialog.askstring("Ajouter un livre", "ISBN:")
        resume = simpledialog.askstring("Ajouter un livre", "Résumé:")
        langue = simpledialog.askstring("Ajouter un livre", "Langue:")
        auteurs = simpledialog.askstring("Ajouter les auteurs", 'Auteurs (prénom puis nom, auteurs séparés par une virgule sans espace):') 
        exemplaire = simpledialog.askstring("Ajouter l'état de l'exemplaire", 'Etat (neuf, bon, abime)') 
        
        auteursjson = []
        auteurs = auteurs.split(",")
        for auteur in auteurs:
            auteur = auteur.split(" ")
            auteursjson.append({'nom': auteur[1], 'prenom': auteur[0]})
        auteursjson = json.dumps(auteursjson)
        
        exemplaires = [{"id":None, "etat":"neuf"}]
        exemplaires = json.dumps(exemplaires)
        
        if None in (date_apparition, editeur, genre, code_classification, isbn, resume, langue, auteurs, exemplaires):
            return  
        
        if exemplaire not in ["neuf", "bon", "abime"]:
            messagebox.showerror("Erreur", "L'état de l'exemplaire peut soit être neuf, bon ou abime")
            return
        
        insert_query = """
        INSERT INTO Ressource_JSON (titre, date_apparition, editeur, genre, code_classification, type, ISBN, resume, langue, auteurs, exemplaires)
        VALUES (%s, %s, %s, %s, %s, 'livre', %s, %s, %s, %s, %s)
        RETURNING code;
        """
        values = (book_title, date_apparition, editeur, genre, code_classification, isbn, resume, langue, auteursjson, exemplaires)

        try:            
            code = self.execute_query(insert_query, values)[0][0]
            exemplaires = [{"id":100*code, "etat":exemplaire}]
            exemplaires = json.dumps(exemplaires)
            self.execute_no_return_query("UPDATE Ressource_JSON SET exemplaires = %s WHERE code = %s", (exemplaires, code))
        except Exception as e:
            messagebox.showerror("Erreur", f"Erreur lors de l'ajout du livre : {e}")
            
        self.show_books()

    def show_films(self):
        
        result = self.execute_query("SELECT * FROM Ressource_JSON WHERE type='film';")
        self.display_films(result, "Liste des films")

    def display_films(self, result, title):
        
        popup = tk.Toplevel(self.root)
        popup.title(title)

        
        canvas = tk.Canvas(popup)
        canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        
        scrollbar_y = tk.Scrollbar(popup, orient=tk.VERTICAL, command=canvas.yview)
        scrollbar_y.pack(side=tk.RIGHT, fill=tk.Y)
        canvas.configure(yscrollcommand=scrollbar_y.set)
        
        frame = tk.Frame(canvas)
        canvas.create_window((0, 0), window=frame, anchor=tk.NW)
        
        if not self.is_adherent():
            add_film_button = tk.Button(frame, text="Ajouter un film", command=self.add_film)
            add_film_button.grid(row=0, column=0, padx=10, pady=5, sticky="w")

        if result:
            for row in result:
                title_label = tk.Label(frame, text=row[1], font=("Helvetica", 12, "bold"), wraplength=300)  
                title_label.grid(padx=10, pady=5, sticky="w")

                details_label_text = f"Date de sortie : {row[2]}, Production : {row[3]}, Genre : {row[4]}"
                details_label = tk.Label(frame, text=details_label_text, font=("Helvetica", 10, "italic"), wraplength=300)  
                details_label.grid(padx=10, pady=2, sticky="w")

                description_label = tk.Label(frame, text=row[10], wraplength=300)  
                description_label.grid(padx=10, pady=2, sticky="w")
                
                if not self.is_adherent():
                    edit_button = tk.Button(frame, text="Modifier le synopsis", command=lambda code=row[0]: self.edit_film(code))
                    edit_button.grid(pady=5, padx=10, sticky="w")

                duration_label = tk.Label(frame, text=f"Durée : {row[11]}", font=("Helvetica", 10, "italic"), wraplength=300)  
                duration_label.grid(padx=10, pady=2, sticky="w")
                
                nb_exemplaires = 0
                if (isinstance(row[17], dict)):
                    nb_exemplaires = 1
                else:
                    for e in row[17]:
                        nb_exemplaires += 1
                
                exemplar_count_label = tk.Label(frame, text=f"Nombre d'exemplaires : {nb_exemplaires}")
                exemplar_count_label.grid(padx=10, pady=2, sticky="w")

                if not self.is_adherent():
                    add_exemplar_button = tk.Button(frame, text="Ajouter un exemplaire", command=lambda code=row[0]: self.add_exemplar(code))
                    add_exemplar_button.grid(padx=10, pady=5, sticky="w")
                
                if self.is_adherent():
                    disponibilite = self.verifier_disponibilite(row[0])
                    demande_en_attente = self.verifier_demande_en_attente(row[0])
                    
                    if not self.emprunts_possibles():
                        impossible_label = tk.Label(frame, text="Emprunt impossible : 5 emprunts déjà en cours", fg="red", wraplength=300)
                        impossible_label.grid(padx=10, pady=2, sticky="w")
                    elif self.endette():
                        endette_label = tk.Label(frame, text="Emprunt impossible : vous devez encore de l'argent à la bibliothèque", fg="red", wraplength=300)
                        endette_label.grid(padx=10, pady=2, sticky="w")
                    elif self.est_retard():
                        retard_label = tk.Label(frame, text="Emprunt impossible : vous avez un prêt non rendu en retard", fg="red", wraplength=300)
                        retard_label.grid(padx=10, pady=2, sticky="w")
                    elif demande_en_attente:
                        attente_label = tk.Label(frame, text="Demande d'emprunt envoyée", fg="green", wraplength=300)
                        attente_label.grid(padx=10, pady=2, sticky="w")
                    elif disponibilite:
                        emprunt_button = tk.Button(frame, text="Emprunter", command=lambda: self.effectuer_demande_pret(row[0]))
                        emprunt_button.grid(padx=10, pady=5, sticky="w")
                    else:
                        indisponible_label = tk.Label(frame, text="Emprunt indisponible", fg="orange", wraplength=300)
                        indisponible_label.grid(padx=10, pady=2, sticky="w")
                
                if not self.is_adherent():
                    delete_button = tk.Button(frame, text="Supprimer", command=lambda code=row[0]: self.delete_film(code))
                    delete_button.grid(pady=5, padx=10, sticky="w")

                separator = tk.Label(frame, text="-" * 50)
                separator.grid(padx=10, pady=5, sticky="w")
        else:
            tk.Label(frame, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)

        
        canvas.bind_all("<MouseWheel>", lambda event: canvas.yview_scroll(int(-1 * (event.delta / 120)), "units"))

        
        frame.update_idletasks()
        canvas.config(scrollregion=canvas.bbox("all"))
        
    def edit_film(self, film_code):
        existing_synopsis = self.get_existing_synopsis(film_code)

        
        def apply_changes():
            new_synopsis = text_widget.get("1.0", tk.END).strip()

            if new_synopsis is not None:
                
                success = self.update_synopsis(film_code, new_synopsis)

                if success:
                    messagebox.showinfo("Succès", "Synopsis mis à jour avec succès.")
                    self.show_films()
                else:
                    messagebox.showerror("Erreur", "Erreur lors de la mise à jour de la synopsys.")
            
            dialog.destroy()

        
        dialog = tk.Toplevel(self.root)
        dialog.title("Modifier le synopsis")

        
        text_widget = tk.Text(dialog, height=10, width=40)
        text_widget.pack()

        
        text_widget.insert(tk.END, existing_synopsis)

        
        apply_button = tk.Button(dialog, text="Appliquer les changements", command=apply_changes)
        apply_button.pack()
                
    def get_existing_synopsis(self, film_code):
        
        query = "SELECT synopsis FROM Ressource_JSON WHERE code = %s"        
        result = self.execute_query(query, (film_code,))

        
        if result:
            return result[0][0]
        else:
            return None

    def update_synopsis(self, film_code, new_description):
        
        query = "UPDATE Ressource_JSON SET synopsis = %s WHERE code = %s"

        try:
            self.execute_no_return_query(query, (new_description, film_code))
            self.connection.commit()
            return True
        except Exception as e:
            print(f"Erreur lors de la mise à jour du synopsis : {e}")
            self.connection.rollback()
            return False
        
    def delete_film(self, film_code):
        delete_query = "DELETE FROM Ressource_JSON WHERE code = %s;"
        self.execute_no_return_query(delete_query, (film_code,))

        self.connection.commit()
        messagebox.showinfo("Succès", "Film supprimé avec succès.")
        self.show_films()
        
    def add_film(self):
        
        film_title = simpledialog.askstring("Ajouter un film", "Titre du film:")
        if not film_title:
            return  

        
        date_apparition = simpledialog.askstring("Ajouter un film", "Date d'apparition (AAAA-MM-JJ):")
        editeur = simpledialog.askstring("Ajouter un film", "Éditeur:")
        genre = simpledialog.askstring("Ajouter un film", "Genre:")
        code_classification = simpledialog.askstring("Ajouter un film", "Code de classification:")
        synopsis = simpledialog.askstring("Ajouter un film", "Synopsis:")
        langue = simpledialog.askstring("Ajouter un film", "Langue:")
        duration = simpledialog.askstring("Ajouter un film", "Durée (HH:MM:SS):")
        realisateurs = simpledialog.askstring("Ajouter les réalisateurs", 'Réalisateurs (prénom puis nom, auteurs séparés par une virgule sans espace):') 
        acteurs = simpledialog.askstring("Ajouter les acteurs", 'Acteurs (prénom puis nom, auteurs séparés par une virgule sans espace):') 
        exemplaire = simpledialog.askstring("Ajouter l'état de l'exemplaire", 'Etat (neuf, bon, abime)') 
        
        realisateursjson = []
        realisateurs = realisateurs.split(",")
        for realisateur in realisateurs:
            realisateur = realisateur.split(" ")
            realisateursjson.append({'nom': realisateur[1], 'prenom': realisateur[0]})
        realisateursjson = json.dumps(realisateursjson)
        
        acteursjson = []
        acteurs = acteurs.split(",")
        for acteur in acteurs:
            acteur = acteur.split(" ")
            acteursjson.append({'nom': acteur[1], 'prenom': acteur[0]})
        acteursjson = json.dumps(acteursjson)
        
        exemplaires = [{"id":None, "etat":"neuf"}]
        exemplaires = json.dumps(exemplaires)
        
        if None in (date_apparition, editeur, genre, code_classification, synopsis, langue, duration, realisateurs, acteurs, exemplaires):
            return  
        
        if exemplaire not in ["neuf", "bon", "abime"]:
            messagebox.showerror("Erreur", "L'état de l'exemplaire peut soit être neuf, bon ou abime")
            return
        
        insert_query = """
        INSERT INTO Ressource_JSON (titre, date_apparition, editeur, genre, code_classification, type, synopsis, langue, longueur, realisateurs, acteurs, exemplaires)
        VALUES (%s, %s, %s, %s, %s, 'film', %s, %s, %s, %s, %s, %s)
        RETURNING code;
        """
        values = (film_title, date_apparition, editeur, genre, code_classification, synopsis, langue, duration, realisateursjson, acteursjson, exemplaires)

        try:            
            code = self.execute_query(insert_query, values)[0][0]
            exemplaires = [{"id":100*code, "etat":exemplaire}]
            exemplaires = json.dumps(exemplaires)
            self.execute_no_return_query("UPDATE Ressource_JSON SET exemplaires = %s WHERE code = %s", (exemplaires, code))
        except Exception as e:
            messagebox.showerror("Erreur", f"Erreur lors de l'ajout du film : {e}")
            
        self.show_films()

    def show_music(self):        
        result = self.execute_query("SELECT * FROM Ressource_JSON WHERE type='enregistrement_musical';")
        self.display_music(result, "Liste des enregistrements musicaux")

    def display_music(self, result, title):
        
        popup = tk.Toplevel(self.root)
        popup.title(title)

        
        canvas = tk.Canvas(popup)
        canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        
        scrollbar_y = tk.Scrollbar(popup, orient=tk.VERTICAL, command=canvas.yview)
        scrollbar_y.pack(side=tk.RIGHT, fill=tk.Y)
        canvas.configure(yscrollcommand=scrollbar_y.set)

        
        frame = tk.Frame(canvas)
        canvas.create_window((0, 0), window=frame, anchor=tk.NW)
        
        if not self.is_adherent():
            add_music_button = tk.Button(frame, text="Ajouter une musique", command=self.add_music)
            add_music_button.grid(row=0, column=0, padx=10, pady=5, sticky="w")

        if result:
            for row in result:
                title_label = tk.Label(frame, text=row[1], font=("Helvetica", 12, "bold"), wraplength=300)  
                title_label.grid(padx=10, pady=5, sticky="w")

                details_label_text = f"Date de sortie : {row[2]}, Studio : {row[3]}, Genre : {row[4]}"
                details_label = tk.Label(frame, text=details_label_text, font=("Helvetica", 10, "italic"), wraplength=300)  
                details_label.grid(padx=10, pady=2, sticky="w")

                duration_label = tk.Label(frame, text=f"Durée : {row[11]}", font=("Helvetica", 10, "italic"), wraplength=300)  
                duration_label.grid(padx=10, pady=2, sticky="w")
                
                nb_exemplaires = 0
                if (isinstance(row[17], dict)):
                    nb_exemplaires = 1
                else:
                    for e in row[17]:
                        nb_exemplaires += 1
                
                exemplar_count_label = tk.Label(frame, text=f"Nombre d'exemplaires : {nb_exemplaires}")
                exemplar_count_label.grid(padx=10, pady=2, sticky="w")

                if not self.is_adherent():
                    add_exemplar_button = tk.Button(frame, text="Ajouter un exemplaire", command=lambda code=row[0]: self.add_exemplar(code))
                    add_exemplar_button.grid(padx=10, pady=5, sticky="w")
                
                if self.is_adherent():
                    disponibilite = self.verifier_disponibilite(row[0])
                    demande_en_attente = self.verifier_demande_en_attente(row[0])
                    
                    if not self.emprunts_possibles():
                        impossible_label = tk.Label(frame, text="Emprunt impossible : 5 emprunts déjà en cours", fg="red", wraplength=300)
                        impossible_label.grid(padx=10, pady=2, sticky="w")
                    elif self.endette():
                        endette_label = tk.Label(frame, text="Emprunt impossible : vous devez encore de l'argent à la bibliothèque", fg="red", wraplength=300)
                        endette_label.grid(padx=10, pady=2, sticky="w")
                    elif self.est_retard():
                        retard_label = tk.Label(frame, text="Emprunt impossible : vous avez un prêt non rendu en retard", fg="red", wraplength=300)
                        retard_label.grid(padx=10, pady=2, sticky="w")
                    elif demande_en_attente:
                        attente_label = tk.Label(frame, text="Demande d'emprunt envoyée", fg="green", wraplength=300)
                        attente_label.grid(padx=10, pady=2, sticky="w")
                    elif disponibilite:
                        emprunt_button = tk.Button(frame, text="Emprunter", command=lambda: self.effectuer_demande_pret(row[0]))
                        emprunt_button.grid(padx=10, pady=5, sticky="w")
                    else:
                        indisponible_label = tk.Label(frame, text="Emprunt indisponible", fg="orange", wraplength=300)
                        indisponible_label.grid(padx=10, pady=2, sticky="w")
                
                if not self.is_adherent():
                    delete_button = tk.Button(frame, text="Supprimer", command=lambda code=row[0]: self.delete_music(code))
                    delete_button.grid(pady=5, padx=10, sticky="w")

                separator = tk.Label(frame, text="-" * 50)
                separator.grid(padx=10, pady=5, sticky="w")
        else:
            tk.Label(frame, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)

        
        canvas.bind_all("<MouseWheel>", lambda event: canvas.yview_scroll(int(-1 * (event.delta / 120)), "units"))

        
        frame.update_idletasks()
        canvas.config(scrollregion=canvas.bbox("all"))
        
    def delete_music(self, music_code):
        delete_query = "DELETE FROM Ressource_JSON WHERE code = %s;"
        self.execute_no_return_query(delete_query, (music_code,))

        self.connection.commit()
        messagebox.showinfo("Succès", "Musique supprimée avec succès.")
        self.show_music()
        
    def add_music(self):
        
        music_title = simpledialog.askstring("Ajouter une musique", "Titre de la musique:")
        if not music_title:
            return  

        
        date_apparition = simpledialog.askstring("Ajouter une musique", "Date d'apparition (AAAA-MM-JJ):")
        editeur = simpledialog.askstring("Ajouter une musique", "Éditeur:")
        genre = simpledialog.askstring("Ajouter une musique", "Genre:")
        code_classification = simpledialog.askstring("Ajouter une musique", "Code de classification:")
        duration = simpledialog.askstring("Ajouter une musique", "Durée (HH:MM:SS):")
        compositeurs = simpledialog.askstring("Ajouter les compositeurs", 'Compositeurs (prénom puis nom, auteurs séparés par une virgule sans espace):') 
        interpretes = simpledialog.askstring("Ajouter les interprètes", 'Interprètes (prénom puis nom, auteurs séparés par une virgule sans espace):') 
        exemplaire = simpledialog.askstring("Ajouter l'état de l'exemplaire", 'Etat (neuf, bon, abime)') 
        
        compositeursjson = []
        compositeurs = compositeurs.split(",")
        for compositeur in compositeurs:
            compositeur = compositeur.split(" ")
            compositeursjson.append({'nom': compositeur[1], 'prenom': compositeur[0]})
        compositeursjson = json.dumps(compositeursjson)
        
        interpretesjson = []
        interpretes = interpretes.split(",")
        for interprete in interpretes:
            interprete = interprete.split(" ")
            interpretesjson.append({'nom': interprete[1], 'prenom': interprete[0]})
        interpretesjson = json.dumps(interpretesjson)
        
        exemplaires = [{"id":None, "etat":"neuf"}]
        exemplaires = json.dumps(exemplaires)
        
        if None in (date_apparition, editeur, genre, code_classification, duration, compositeurs, interpretes, exemplaires):
            return  
        
        if exemplaire not in ["neuf", "bon", "abime"]:
            messagebox.showerror("Erreur", "L'état de l'exemplaire peut soit être neuf, bon ou abime")
            return
        
        insert_query = """
        INSERT INTO Ressource_JSON (titre, date_apparition, editeur, genre, code_classification, type, longueur, compositeurs, interpretes, exemplaires)
        VALUES (%s, %s, %s, %s, %s, 'enregistrement_musical', %s, %s, %s, %s)
        RETURNING code;
        """
        values = (music_title, date_apparition, editeur, genre, code_classification, duration, compositeursjson, interpretesjson, exemplaires)

        try:            
            code = self.execute_query(insert_query, values)[0][0]
            exemplaires = [{"id":100*code, "etat":exemplaire}]
            exemplaires = json.dumps(exemplaires)
            self.execute_no_return_query("UPDATE Ressource_JSON SET exemplaires = %s WHERE code = %s", (exemplaires, code))
        except Exception as e:
            messagebox.showerror("Erreur", f"Erreur lors de l'ajout de la musique : {e}")
            
        self.show_music()
    
    def show_top_ressources(self):
        result = self.execute_query("""
            SELECT
                R.titre,
                R.type,
                COUNT(P.date_pret) AS nombre_emprunts
            FROM
                Ressource_JSON AS R, JSON_ARRAY_ELEMENTS(R.exemplaires) AS ex
            JOIN
                PRET_JSON AS P ON CAST(ex->>'id' AS INTEGER) = P.exemplaire
            GROUP BY
                R.code
            ORDER BY
                nombre_emprunts DESC
            LIMIT
                10;
            """)
        self.display_top_ressources(result, "Top des ressources les plus empruntées")


    def display_top_ressources(self, result, title):
        popup = tk.Toplevel(self.root)
        popup.title(title)
            
        if result:
            for row in result:
                title_label = tk.Label(popup, text=row[0], font=("Helvetica", 12, "bold"))
                title_label.grid(padx=10, pady=5, sticky="w")
                type_label = tk.Label(popup, text=f"Type de ressource : {row[1]}")
                type_label.grid(padx=10, pady=5, sticky="w")
                    
        else:
            tk.Label(popup, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)

    def show_meilleurs_contributeurs(self):
        result = self.execute_query("""
            SELECT
                R.auteurs,
                COUNT(R.code) AS nombre_de_ressources
            FROM
                Ressource_JSON AS R
            GROUP BY
                R.auteurs
            ORDER BY
                nombre_de_ressources DESC
            LIMIT
                10;
            """)
        self.display_result(result, "Liste des meilleurs contributeurs")
        
    def display_meilleurs_contributeur(self, result, title):
        popup = tk.Toplevel(self.root)
        popup.title(title)
        
        if result:
            for row in result:
                title_label = tk.Label(popup, text=f"{row[0]} {row[1]}", font=("Helvetica", 12, "bold"))
                title_label.grid(padx=10, pady=5, sticky="w")
                details_label = tk.Label(popup, text=f"{row[2]} a participé à {row[3]} ressource(s)")
                details_label.grid(padx=10, pady=2, sticky="w")
        else:
            tk.Label(popup, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)
 
 
    def show_historique_emprunt(self):
        result = self.execute_query(f"SELECT P.date_pret, P.date_retour, R.titre FROM PRET_JSON AS P JOIN DemandePret_JSON AS D ON P.demande_pret_id = D.id JOIN RESSOURCE_JSON AS R ON D.code_ressource = R.code WHERE P.adherent = '{self.global_username.get()}';") 
        self.display_historique_emprunt(result, "Historique emprunt")
        
    def display_historique_emprunt(self, result, title):
        popup = tk.Toplevel(self.root)
        popup.title(title)
        if result:
            for row in result:
                title_label = tk.Label(popup, text=row[2], font=("Helvetica", 12, "bold"))
                title_label.grid(padx=10, pady=5, sticky="w")
                if not row[1]:
                    details_label = tk.Label(popup, text=f"Date du prêt {row[0]}, Ressource : {row[2]}")
                    details_label.grid(padx=10, pady=2, sticky="w")
                    now_label = tk.Label(popup, text=f"Prêt en cours", fg="green")
                    now_label.grid(padx=10, pady=2, sticky="w")
                else:
                    details_label = tk.Label(popup, text=f"Date du prêt {row[0]}, Date de retour du prêt {row[1]}, Ressource : {row[2]}")
                    details_label.grid(padx=10, pady=2, sticky="w")
        else:
            tk.Label(popup, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)
            
    def show_historique(self):
        result = self.execute_query("""
            SELECT P.date_pret, P.date_retour, R.titre, A.login FROM PRET_JSON AS P JOIN DemandePret_JSON AS D ON P.demande_pret_id = D.id JOIN RESSOURCE_JSON AS R ON D.code_ressource = R.code JOIN Adherent AS A ON P.adherent = A.login;
            """) 
        self.display_historique(result, "Historique des prêts")
        
    def display_historique(self, result, title):
        popup = tk.Toplevel(self.root)
        popup.title(title)
        if result:
            for row in result:
                title_label = tk.Label(popup, text=row[2], font=("Helvetica", 12, "bold"))
                title_label.grid(padx=10, pady=5, sticky="w")
                if not row[1]:
                    details_label = tk.Label(popup, text=f"Date du prêt {row[0]}, Adhérent (login) : {row[3]}")
                    details_label.grid(padx=10, pady=2, sticky="w")
                    now_label = tk.Label(popup, text=f"Prêt en cours", fg="green")
                    now_label.grid(padx=10, pady=2, sticky="w")
                else:
                    details_label = tk.Label(popup, text=f"Date du prêt {row[0]}, Date de retour du prêt {row[1]}, Adhérent (login) : {row[3]}")
                    details_label.grid(padx=10, pady=2, sticky="w")
        else:
            tk.Label(popup, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)
            
            
    def show_emprunt_cours(self):
        result = self.execute_query(f"SELECT P.date_pret, P.date_maximale, R.titre, P.adherent FROM PRET_JSON AS P JOIN DemandePret_JSON AS D ON P.demande_pret_id = D.id JOIN RESSOURCE_JSON AS R ON D.code_ressource = R.code WHERE P.adherent = '{self.global_username.get()}' AND P.date_retour IS NULL;")
        self.display_emprunt_cours(result, "Emprunt en cours")
        
    def display_emprunt_cours(self, result, title):
        popup = tk.Toplevel(self.root)
        popup.title(title)
        if result:
            for row in result:
                title_label = tk.Label(popup, text=row[2], font=("Helvetica", 12, "bold"))
                title_label.grid(padx=10, pady=5, sticky="w")
                details_label = tk.Label(popup, text=f"Date du prêt {row[0]}, Date maximale de retour du prêt {row[1]}, État initial de la ressource : {row[2]}")
                details_label.grid(padx=10, pady=2, sticky="w")
        else:
            tk.Label(popup, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)
            
    
    def show_sanction_adherent(self):
        result = self.execute_query(f"SELECT s.pret_date, s.type_sanction, s.nb_jours_retard, s.prix_a_payer, r.titre FROM Sanction_JSON AS s JOIN (SELECT CAST(json_array_elements(exemplaires)->>'id' AS INTEGER) AS exemplaire_id, titre FROM Ressource_JSON) AS r ON r.exemplaire_id = s.pret_exemplaire WHERE s.pret_adherent = '{self.global_username.get()}';")
        self.display_sanction_ad(result, "Liste de mes sanctions")
        
    def display_sanction_ad(self, result, title):
        popup = tk.Toplevel(self.root)
        popup.title(title)
        if result:
            for row in result:
                if row[1] == "retard":
                    title_label = tk.Label(popup, text=f"Retard", font=("Helvetica", 12, "bold"))
                    title_label.grid(padx=10, pady=5, sticky="w")
                    details_label = tk.Label(popup, text=f"Date du prêt : {row[0]}, Sanction : {row[1]}, Nombre de jours : {row[2]}, Ressource : {row[4]}")
                    details_label.grid(padx=10, pady=2, sticky="w")
                else:
                    title_label = tk.Label(popup, text=f"Dégradation", font=("Helvetica", 12, "bold"))
                    title_label.grid(padx=10, pady=5, sticky="w")
                    details_label = tk.Label(popup, text=f"Date du prêt : {row[0]}, Sanction : {row[1]}, Somme restante à payer : {row[3]}, Ressource : {row[4]}")
                    details_label.grid(padx=10, pady=2, sticky="w")
        else:
            tk.Label(popup, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)
        
    def show_sanctions(self):
        result = self.execute_query("""
            SELECT
                s.pret_date,
                s.type_sanction,
                a.login,
                r_subquery.titre,
                a.nom,
                a.prenom
            FROM
                Sanction_JSON AS s
            JOIN
                Adherent AS a ON s.pret_adherent = a.login
            JOIN
                (
                    SELECT
                        R.code,
                        CAST(json_array_elements(R.exemplaires)->>'id' AS INTEGER) AS exemplaire_id,
                        R.titre
                    FROM
                        Ressource_JSON AS R
                ) AS r_subquery ON r_subquery.exemplaire_id = s.pret_exemplaire;
            """)
        self.display_sanction(result, "Liste des sanctions")
        
    def display_sanction(self, result, title):
        popup = tk.Toplevel(self.root)
        popup.title(title)
        if result:
            for row in result:
                title_label = tk.Label(popup, text=f"{row[4]} {row[5]}", font=("Helvetica", 12, "bold"))
                title_label.grid(padx=10, pady=5, sticky="w")
                details_label = tk.Label(popup, text=f"Date du prêt : {row[0]}, Sanction : {row[1]}, Adhérent (login) : {row[2]}, Ressource : {row[3]}")
                details_label.grid(padx=10, pady=2, sticky="w")
        else:
            tk.Label(popup, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)
    
    def somme_argent(self):
        result= self.execute_query("""
            SELECT
                SUM(S.prix_a_payer), P.adherent, A.nom, A.prenom
            FROM
                Sanction_JSON AS S
            JOIN
                Adherent AS A ON S.pret_adherent = A.login
            JOIN
                PRET_JSON AS P ON S.pret_date = P.date_pret AND S.pret_exemplaire = P.exemplaire AND S.pret_adherent = P.adherent
            GROUP BY 
                P.adherent, A.nom, A.prenom;
                """)
        self.display_somme(result, "Somme dûe par adherent")

    def retard(self):
        result= self.execute_query("""
            SELECT
                a.nom,
                a.prenom,
                s.pret_date,
                s.nb_jours_retard,
                a.login
            FROM
                Sanction_JSON AS s
            JOIN
                Adherent AS a ON s.pret_adherent = a.login
            JOIN
                (
                    SELECT
                        R.code,
                        CAST(json_array_elements(R.exemplaires)->>'id' AS INTEGER) AS exemplaire_id,
                        R.titre
                    FROM
                        Ressource_JSON AS R
                ) AS r_subquery ON r_subquery.exemplaire_id = s.pret_exemplaire
            WHERE
                s.type_sanction = 'retard';
            """)
        self.display_retard(result, "Liste des retards")
        
    def degradation(self):
        result= self.execute_query("""
            SELECT
                a.nom,
                a.prenom,
                s.pret_date,
                s.etat_apres_rendu,
                a.login,
                s.prix_a_payer,
                s.id
            FROM
                Sanction_JSON AS s
            JOIN
                Adherent AS a ON s.pret_adherent = a.login
            JOIN
                (
                    SELECT
                        R.code,
                        CAST(json_array_elements(R.exemplaires)->>'id' AS INTEGER) AS exemplaire_id,
                        R.titre
                    FROM
                        Ressource_JSON AS R
                ) AS r_subquery ON r_subquery.exemplaire_id = s.pret_exemplaire
            WHERE
                s.type_sanction = 'dégradation';
            """ )
        self.display_degradation(result, "Liste de toutes les dégradations")
        
    def display_retard(self, result, title):
        popup = tk.Toplevel(self.root)
        popup.title(title)
        if result:
            for row in result:
                title_label = tk.Label(popup, text=f"{row[0]} {row[1]}", font=("Helvetica", 12, "bold"))
                title_label.grid(padx=10, pady=5, sticky="w")
                details_label = tk.Label(popup, text=f"Date du prêt : {row[2]}, Adhérent (login) : {row[4]}, Jours de retard : {row[3]}")
                details_label.grid(padx=10, pady=2, sticky="w")
        else:
            tk.Label(popup, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)

    def display_degradation(self, result, title):
        popup = tk.Toplevel(self.root)
        popup.title(title)
        if result:
            for row in result:
                title_label = tk.Label(popup, text=f"{row[0]} {row[1]}", font=("Helvetica", 12, "bold"))
                title_label.grid(padx=10, pady=5, sticky="w")
                if row[5] == 0:
                    details_text = f"Date du prêt : {row[2]}, Adhérent (login) : {row[4]}, État après rendu : {row[3]}"
                    details_label = tk.Label(popup, text=details_text)
                    details_label.grid(padx=10, pady=2, sticky="w")
                    rest_to_pay_label = tk.Label(popup, text=f"Remboursement effectué", fg="green")
                    rest_to_pay_label.grid(padx=10, pady=2, sticky="w")
                else:
                    details_text = f"Date du prêt : {row[2]}, Adhérent (login) : {row[4]}, État après rendu : {row[3]}"
                    details_label = tk.Label(popup, text=details_text)
                    details_label.grid(padx=10, pady=2, sticky="w")
                    rest_to_pay_label = tk.Label(popup, text=f"Reste à payer : {row[5]}", fg="red")
                    rest_to_pay_label.grid(padx=10, pady=2, sticky="w")
                    paiement_button = tk.Button(popup, text="Payé", command=lambda code=row[6]: self.payer(code))
                    paiement_button.grid(padx=10, pady=5, sticky="w")
        else:
            tk.Label(popup, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)
    
    def payer(self, code):
        try:
            self.execute_no_return_query(f"UPDATE Sanction_JSON SET prix_a_payer = 0 WHERE id = {code}")
            self.degradation()
            tk.messagebox.showinfo("Succès", "Le paiement a bien été enregistré")

        except Exception as e:
            tk.messagebox.showerror("Erreur", f"Erreur lors du paiement : {e}")

    def display_somme(self, result, title):
        popup = tk.Toplevel(self.root)
        popup.title(title)
        if result:
            for row in result:
                title_label = tk.Label(popup, text=f"{row[2]} {row[3]}", font=("Helvetica", 12, "bold"))
                title_label.grid(padx=10, pady=5, sticky="w")
                details_label = tk.Label(popup, text=f"Adhérent (login) : {row[1]}, Somme dûe : {row[0]}")
                details_label.grid(padx=10, pady=2, sticky="w")
        else:
            tk.Label(popup, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)
    
    def fin_programe(self):
        for widget in self.root.winfo_children():
            widget.destroy()
        self.create_login_screen()


    def show_adherents(self):
        result = self.execute_query("SELECT * FROM Adherent;")
        self.display_adherents(result, "Liste des adhérents")
        
    def display_adherents(self, result, title):
        self.popup = tk.Toplevel(self.root)
        self.popup.title(title)

        
        filter_frame = tk.Frame(self.popup)
        filter_frame.pack(side=tk.TOP, fill=tk.X)

        all_button = tk.Button(filter_frame, text="Tous", command=lambda: self.display_filtered_adherents(result, "Adhérents"))
        all_button.pack(side=tk.LEFT)

        active_button = tk.Button(filter_frame, text="Actifs", command=lambda: self.display_filtered_adherents(self.filter_adherents(result, 1), "Adhérents actifs"))
        active_button.pack(side=tk.LEFT)

        inactive_button = tk.Button(filter_frame, text="Inactifs", command=lambda: self.display_filtered_adherents(self.filter_adherents(result, 0), "Adhérents inactifs"))
        inactive_button.pack(side=tk.LEFT)

        blacklisted_button = tk.Button(filter_frame, text="Blacklistés", command=lambda: self.display_filtered_adherents(self.filter_adherents(result, -1), "Adhérents blacklistés"))
        blacklisted_button.pack(side=tk.LEFT)
        
        add_button = tk.Button(filter_frame, text="Rechercher", command=lambda: self.open_search_window())
        add_button.pack(side=tk.LEFT)
        
        add_button = tk.Button(filter_frame, text="Ajouter", command=lambda: self.open_add_adherent_window())
        add_button.pack(side=tk.LEFT)

        
        self.canvas = tk.Canvas(self.popup)
        self.canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        
        scrollbar_y = tk.Scrollbar(self.popup, orient=tk.VERTICAL, command=self.canvas.yview)
        scrollbar_y.pack(side=tk.RIGHT, fill=tk.Y)
        self.canvas.configure(yscrollcommand=scrollbar_y.set)

        
        self.frame = tk.Frame(self.canvas)
        self.canvas.create_window((0, 0), window=self.frame, anchor=tk.NW)

        self.display_filtered_adherents(result, "Adhérents")  

        
        self.canvas.bind_all("<MouseWheel>", lambda event: self.canvas.yview_scroll(int(-1 * (event.delta / 120)), "units"))

        
        self.frame.update_idletasks()
        self.canvas.config(scrollregion=self.canvas.bbox("all"))

    def filter_adherents(self, result, status):
        if status == -1:
            return [row for row in result if row[10] == 1]
        else:            
            return [row for row in result if row[8] == status]

    def display_filtered_adherents(self, filtered_result, title):
        
        for widget in self.frame.winfo_children():
            widget.destroy()

        if filtered_result:
            for row in filtered_result:
                title_label = tk.Label(self.frame, text=f"{row[2]} {row[3]}", font=("Helvetica", 12, "bold"), justify=tk.LEFT)
                title_label.pack(padx=10, pady=5, anchor=tk.W)

                login_label = tk.Label(self.frame, text=row[0], font=("Helvetica", 10, "italic"), justify=tk.LEFT)
                login_label.pack(padx=10, pady=2, anchor=tk.W)

                if row[8] == 1:
                    actuel_label = tk.Label(self.frame, text="Adhérent actif", fg="green", font=("Helvetica", 10, "bold"), justify=tk.LEFT)
                    actuel_label.pack(padx=10, pady=2, anchor=tk.W)
                else:
                    actuel_label = tk.Label(self.frame, text="Adhérent inactif", fg="red", font=("Helvetica", 10, "bold"), justify=tk.LEFT)
                    actuel_label.pack(padx=10, pady=2, anchor=tk.W)

                if row[10] == 1:
                    blacklist_label = tk.Label(self.frame, text="BLACKLISTÉ", fg="red", font=("Helvetica", 12, "bold"), justify=tk.LEFT)
                    blacklist_label.pack(padx=10, pady=2, anchor=tk.W)

                ncarte_label = tk.Label(self.frame, text=f"N° carte : {row[5]}", font=("Helvetica", 10, "bold"), justify=tk.LEFT)
                ncarte_label.pack(padx=10, pady=2, anchor=tk.W)

                details_label_text = f"Mail : {row[4]}\nDate de naissance : {row[6]}\nTéléphone : {row[7]}\nAdresse : {row[9]}\n"
                details_label = tk.Label(self.frame, text=details_label_text, font=("Helvetica", 10), justify=tk.LEFT)
                details_label.pack(padx=10, pady=2, anchor=tk.W)
                
                modify_button = tk.Button(self.frame, text="Modifier les informations", command=lambda r=row: self.open_modify_adherent_window(r))
                modify_button.pack(padx=10, pady=5, anchor=tk.W)
                
                self.display_adherent_info(row[0])

                separator = tk.Label(self.frame, text="-" * 50, justify=tk.LEFT)
                separator.pack(padx=10, pady=5, anchor=tk.W)
        else:
            tk.Label(self.frame, text="Aucun résultat", font=("Helvetica", 10)).pack(padx=10, pady=5, anchor=tk.W)

        
        self.frame.update_idletasks()
        self.canvas.config(scrollregion=self.canvas.bbox("all"))
        
    def open_modify_adherent_window(self, adherent_data):
        
        modify_window = tk.Toplevel(self.root)
        modify_window.title("Modifier les informations")

        
        nom_var = tk.StringVar(value=adherent_data[2])
        prenom_var = tk.StringVar(value=adherent_data[3])
        mail_var = tk.StringVar(value=adherent_data[4])
        telephone_var = tk.StringVar(value=adherent_data[7])
        adresse_var = tk.StringVar(value=adherent_data[9])

        
        tk.Label(modify_window, text="Nom:").grid(row=0, column=0, padx=10, pady=5, sticky=tk.W)
        tk.Entry(modify_window, textvariable=nom_var).grid(row=0, column=1, padx=10, pady=5, sticky=tk.W)

        tk.Label(modify_window, text="Prénom:").grid(row=1, column=0, padx=10, pady=5, sticky=tk.W)
        tk.Entry(modify_window, textvariable=prenom_var).grid(row=1, column=1, padx=10, pady=5, sticky=tk.W)

        tk.Label(modify_window, text="Mail:").grid(row=2, column=0, padx=10, pady=5, sticky=tk.W)
        tk.Entry(modify_window, textvariable=mail_var).grid(row=2, column=1, padx=10, pady=5, sticky=tk.W)

        tk.Label(modify_window, text="Téléphone:").grid(row=3, column=0, padx=10, pady=5, sticky=tk.W)
        tk.Entry(modify_window, textvariable=telephone_var).grid(row=3, column=1, padx=10, pady=5, sticky=tk.W)

        tk.Label(modify_window, text="Adresse:").grid(row=4, column=0, padx=10, pady=5, sticky=tk.W)
        tk.Entry(modify_window, textvariable=adresse_var).grid(row=4, column=1, padx=10, pady=5, sticky=tk.W)

        
        validate_button = tk.Button(modify_window, text="Valider les changements", command=lambda: self.update_adherent(adherent_data[0], nom_var.get(), prenom_var.get(), mail_var.get(), telephone_var.get(), adresse_var.get(), modify_window))
        validate_button.grid(row=5, column=0, columnspan=2, pady=10)

    def update_adherent(self, login, nom, prenom, mail, telephone, adresse, modify_window):
        
        query = """
        UPDATE Adherent
        SET nom = %s, prenom = %s, mail = %s, telephone = %s, adresse = %s
        WHERE login = %s
        """
        values = (nom, prenom, mail, telephone, adresse, login)

        try:
            self.execute_no_return_query(query, values)
            self.connection.commit()
            modify_window.destroy()
            self.show_adherents()
            messagebox.showinfo("Succès", "Les informations de l'adhérent ont été mises à jour.")
        except Exception as e:
            messagebox.showerror("Erreur", f"Erreur lors de la mise à jour des informations de l'adhérent : {e}")
        
    def open_search_window(self):
        search_window = tk.Toplevel(self.root)
        search_window.title("Recherche Adhérent")

        
        login_label = tk.Label(search_window, text="Login:")
        login_label.grid(row=0, column=0, padx=10, pady=5, sticky=tk.W)

        login_entry = tk.Entry(search_window)
        login_entry.grid(row=0, column=1, padx=10, pady=5, sticky=tk.W)
        
        name_label = tk.Label(search_window, text="Nom:")
        name_label.grid(row=1, column=0, padx=10, pady=5, sticky=tk.W)

        name_entry = tk.Entry(search_window)
        name_entry.grid(row=1, column=1, padx=10, pady=5, sticky=tk.W)
        
        firstname_label = tk.Label(search_window, text="Prénom:")
        firstname_label.grid(row=2, column=0, padx=10, pady=5, sticky=tk.W)

        firstname_entry = tk.Entry(search_window)
        firstname_entry.grid(row=2, column=1, padx=10, pady=5, sticky=tk.W)

        
        search_button = tk.Button(search_window, text="Rechercher", command=lambda: self.search_adherents(login_entry.get(), name_entry.get(), firstname_entry.get()))
        search_button.grid(row=3, column=0, columnspan=2, pady=10)

    def search_adherents(self, login, nom, prenom):
        try:
            search_query = f"SELECT * FROM Adherent WHERE login = '{login}' OR nom = '{nom}' OR prenom = '{prenom}';"
            result = self.execute_query(search_query)
            self.display_filtered_adherents(result, "Résultats de la recherche")

        except Exception as e:
            tk.messagebox.showerror("Erreur", f"Erreur lors de la recherche des adhérents : {e}")
        
    def open_add_adherent_window(self):
        add_adherent_window = tk.Toplevel(self.root)
        add_adherent_window.title("Ajouter un adhérent")

        
        login_label = tk.Label(add_adherent_window, text="Login:")
        login_label.grid(row=0, column=0, padx=10, pady=5, sticky=tk.W)

        login_entry = tk.Entry(add_adherent_window)
        login_entry.grid(row=0, column=1, padx=10, pady=5, sticky=tk.W)
        
        password_label = tk.Label(add_adherent_window, text="Mot de passe:")
        password_label.grid(row=1, column=0, padx=10, pady=5, sticky=tk.W)

        password_entry = tk.Entry(add_adherent_window)
        password_entry.grid(row=1, column=1, padx=10, pady=5, sticky=tk.W)
        
        name_label = tk.Label(add_adherent_window, text="Nom:")
        name_label.grid(row=2, column=0, padx=10, pady=5, sticky=tk.W)

        name_entry = tk.Entry(add_adherent_window)
        name_entry.grid(row=2, column=1, padx=10, pady=5, sticky=tk.W)
        
        firstname_label = tk.Label(add_adherent_window, text="Prénom:")
        firstname_label.grid(row=3, column=0, padx=10, pady=5, sticky=tk.W)

        firstname_entry = tk.Entry(add_adherent_window)
        firstname_entry.grid(row=3, column=1, padx=10, pady=5, sticky=tk.W)
        
        mail_label = tk.Label(add_adherent_window, text="Mail:")
        mail_label.grid(row=4, column=0, padx=10, pady=5, sticky=tk.W)

        mail_entry = tk.Entry(add_adherent_window)
        mail_entry.grid(row=4, column=1, padx=10, pady=5, sticky=tk.W)
        
        card_label = tk.Label(add_adherent_window, text="Numéro de carte:")
        card_label.grid(row=5, column=0, padx=10, pady=5, sticky=tk.W)

        card_entry = tk.Entry(add_adherent_window)
        card_entry.grid(row=5, column=1, padx=10, pady=5, sticky=tk.W)

        date_label = tk.Label(add_adherent_window, text="Date de naissance (AAAA-MM-JJ):")
        date_label.grid(row=6, column=0, padx=10, pady=5, sticky=tk.W)

        date_entry = tk.Entry(add_adherent_window)
        date_entry.grid(row=6, column=1, padx=10, pady=5, sticky=tk.W)
        
        phone_label = tk.Label(add_adherent_window, text="Téléphone:")
        phone_label.grid(row=7, column=0, padx=10, pady=5, sticky=tk.W)

        phone_entry = tk.Entry(add_adherent_window)
        phone_entry.grid(row=7, column=1, padx=10, pady=5, sticky=tk.W)
        
        adress_label = tk.Label(add_adherent_window, text="Adresse:")
        adress_label.grid(row=8, column=0, padx=10, pady=5, sticky=tk.W)

        adress_entry = tk.Entry(add_adherent_window)
        adress_entry.grid(row=8, column=1, padx=10, pady=5, sticky=tk.W)

        
        add_button = tk.Button(add_adherent_window, text="Ajouter", command=lambda: self.add_adherent(add_adherent_window, login_entry.get(), password_entry.get(), name_entry.get(), firstname_entry.get(), mail_entry.get(), card_entry.get(), date_entry.get(), phone_entry.get(), adress_entry.get()))
        add_button.grid(row=10, column=0, columnspan=2, pady=10)
        
    def add_adherent(self, add_adherent_window, login, password, nom, prenom, mail, ncarte, date_naissance, telephone, adresse):
        try:
            
            insert_query = f"INSERT INTO Adherent (login, mot_de_passe, nom, prenom, mail, ncarte, date_naissance, telephone, actuel, adresse, est_blacklist) VALUES ('{login}', '{password}', '{nom}', '{prenom}', '{mail}', {ncarte}, '{date_naissance}', '{telephone}', TRUE, '{adresse}', FALSE);"
            self.execute_no_return_query(insert_query)

            
            add_adherent_window.destroy()
            tk.messagebox.showinfo("Succès", "Adhérent ajouté avec succès.")
        except Exception as e:
            tk.messagebox.showerror("Erreur", f"Erreur lors de l'ajout de l'adhérent : {e}")
        
    def display_adherent_info(self, login):
        
        is_blacklisted = self.execute_query(f"SELECT est_blacklist FROM Adherent WHERE login = '{login}'")[0][0]

        
        result_retard = self.execute_query(f"SELECT COUNT(*) FROM Sanction_JSON WHERE pret_adherent = '{login}' AND type_sanction = 'retard'")
        result_degradation = self.execute_query(f"SELECT COUNT(*) FROM Sanction_JSON WHERE pret_adherent = '{login}' AND type_sanction = 'dégradation'")
        result_somme = self.execute_query(f"SELECT SUM(prix_a_payer) FROM Sanction_JSON WHERE pret_adherent = '{login}'")

        
        info_label_text = f"Retards: {result_retard[0][0]}, Dégradations: {result_degradation[0][0]}, Somme à payer: {result_somme[0][0]}"
        info_label = tk.Label(self.frame, text=info_label_text, font=("Helvetica", 10), justify=tk.LEFT)
        info_label.pack(padx=10, pady=2, anchor=tk.W)

        
        if not is_blacklisted:
            blacklist_button = tk.Button(self.frame, text="Blacklister", command=lambda: self.toggle_blacklist(login, True))
            blacklist_button.pack(padx=10, pady=2, anchor=tk.W)

        
        if is_blacklisted:
            unblacklist_button = tk.Button(self.frame, text="Déblacklister", command=lambda: self.toggle_blacklist(login, False))
            unblacklist_button.pack(padx=10, pady=2, anchor=tk.W)  
            
        
        is_active = self.execute_query(f"SELECT actuel FROM Adherent WHERE login = '{login}'")[0][0]

        
        if is_active:
            resign_button = tk.Button(self.frame, text="Résilier adhésion", command=lambda: self.toggle_membership(login, False))
            resign_button.pack(padx=10, pady=2, anchor=tk.W)
        
        else:
            reactivate_button = tk.Button(self.frame, text="Réactiver adhésion", command=lambda: self.toggle_membership(login, True))
            reactivate_button.pack(padx=10, pady=2, anchor=tk.W)
            
    def toggle_membership(self, login, is_active):
        
        self.execute_no_return_query(f"UPDATE Adherent SET actuel = {is_active} WHERE login = '{login}'")
        action = "résilié" if not is_active else "réactivé"
        messagebox.showinfo("Résiliation adhésion", f"L'adhérent {login} a été {action} avec succès.")
        
        self.show_adherents()

    def toggle_blacklist(self, login, is_blacklisted):
        
        self.execute_no_return_query(f"UPDATE Adherent SET est_blacklist = {is_blacklisted} WHERE login = '{login}'")
        action = "blacklisté" if is_blacklisted else "déblacklisté"
        messagebox.showinfo("Blacklist", f"L'adhérent {login} a été {action} avec succès.")
        
        self.show_adherents()

    def create_table(self):
        
        popup = tk.Toplevel(self.root)
        popup.title("Créer une table")

        tk.Label(popup, text="Nom de la table:").grid(row=0, column=0, padx=10, pady=10)
        table_name_entry = tk.Entry(popup)
        table_name_entry.grid(row=0, column=1, padx=10, pady=10)

        tk.Label(popup, text="Colonnes (séparées par des virgules):").grid(row=1, column=0, padx=10, pady=10)
        columns_entry = tk.Entry(popup)
        columns_entry.grid(row=1, column=1, padx=10, pady=10)

        create_button = tk.Button(popup, text="Créer la table", command=lambda: self.execute_create_table(table_name_entry.get(), columns_entry.get(), popup))
        create_button.grid(row=2, column=0, columnspan=2, pady=10)

    def delete_table(self):
        
        popup = tk.Toplevel(self.root)
        popup.title("Supprimer une table")

        tk.Label(popup, text="Nom de la table à supprimer:").grid(row=0, column=0, padx=10, pady=10)
        table_name_entry = tk.Entry(popup)
        table_name_entry.grid(row=0, column=1, padx=10, pady=10)

        delete_button = tk.Button(popup, text="Supprimer la table", command=lambda: self.execute_delete_table(table_name_entry.get(), popup))
        delete_button.grid(row=1, column=0, columnspan=2, pady=10)

    def execute_create_table(self, table_name, columns, popup):
        try:
            create_table_query = f"CREATE TABLE IF NOT EXISTS {table_name} ({columns});"
            self.execute_no_return_query(create_table_query)
            self.connection.commit()
            popup.destroy()
            self.show_message(f"Table '{table_name}' créée avec succès.")
        except (Exception, psycopg2.Error) as error:
            self.show_message(f"Erreur lors de la création de la table '{table_name}': {error}")

    def execute_delete_table(self, table_name, popup):
        try:
            drop_table_query = f"DROP TABLE IF EXISTS {table_name};"
            self.execute_no_return_query(drop_table_query)
            self.connection.commit()
            popup.destroy()
            self.show_message(f"Table '{table_name}' supprimée avec succès.")
        except (Exception, psycopg2.Error) as error:
            self.show_message(f"Erreur lors de la suppression de la table '{table_name}': {error}")

    def display_result(self, result, title):
        
        popup = tk.Toplevel(self.root)
        popup.title(title)

        if result:
            for i, row in enumerate(result):
                tk.Label(popup, text=row).grid(row=i, column=0, padx=10, pady=5)
        else:
            tk.Label(popup, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)

    def show_message(self, message):
        
        popup = tk.Toplevel(self.root)
        popup.title("Message")
        tk.Label(popup, text=message).pack(padx=10, pady=10)
        close_button = tk.Button(popup, text="Fermer", command=popup.destroy)
        close_button.pack(pady=10)


if __name__ == "__main__":
    root = tk.Tk()
    app = LibraryApp(root)
    root.mainloop()
