*** MLD ***

Dans notre passage en MLD, ‘#’ désigne la clé primaire d’une table. Cet attribut Primary Key satisfait forcément la condition d’unicité UNIQUE, NOT NULL mais également l’aspect immuable. 

** Ressource, Livre, Film, Enregistrement_musical **
On choisit de traiter l’héritage entre Ressource et ses classes filles (Livre, Film et Enregistrement_musical) par classe mère. En effet, nous sommes dans le cas d’une classe mère abstraite où l’héritage est presque complet et exclusif. Un héritage par classes filles a des avantages pour les relations des classes filles avec les contributeurs. Or, selon complexifierait Exemplaire. Un passage par référence serait trop lourd et complexe. On préfère passer par un héritage par classe mère qui gère correctement l’exclusivité, simplifie la gestion des exemplaires, et permet tout de même des liens convenables avec les contributeurs et les classes filles.

Ressource(#code : int, titre : string, date_apparition : date, editeur : string, genre : string, code_classification : string, type : {livre, film, enregistrement_musical}, ISBN : string, resume : string, langue : string, synopsis : sting, longueur : time)

Contraintes pour la classe Ressource : 
- NOT (ISBN AND ((type=film) OR (type= enregistrement_musical)))
- NOT (resume AND ((type=film) OR (type= enregistrement_musical)))
- NOT (langue AND (type= enregistrement_musical))
- NOT (synopsis AND ((type=livre) OR (type= enregistrement_musical)))
- NOT (longueur AND (type=livre))
- (ISBN, resume, langue NOT NULL AND type = livre) OR (synopsis, langue, longueur NOT NULL AND type = film) OR (longueur NOT NULL AND type = enregistrement_musical)
- (type = livre) XOR (type = film) XOR (type = enregistrement_musical)

Vues : 
- vLivre = Projection(Restriction(Ressource, type = livre), code, titre, date_apparition, editeur, genre, code_classification, ISBN, resume, langue)
- vFilm = Projection(Restriction(Ressource, type = film), code, titre, date_apparition, editeur, genre, code_classification, synopsis, langue, longueur)
- vEnregistrement_musical = Projection(Restriction(Ressource, type = enregistrement_musical), code, titre, date_apparition, editeur, genre, code_classification, longueur)

** Contributeur, Acteur, Auteur, Réalisateur, Interprète, Compositeur **
Il faut gérer les héritages entre Contributeur et les classe Auteur, Acteur, Realisateur, Interprete et Compositeur. Tout d’abord, nous savons pertinemment qu’il n’est pas possible d’utiliser la méthode d’héritage par les classes filles. En effet, nous sommes dans le cas d’héritage non exclusif, un contributeur peut, s’il le souhaite être à la fois Acteur, Auteur et Compositeur par exemple. Nous avions ensuite pensé à l’héritage par référence par rapport aux relations existantes entre les filles de contributeur et les filles de ressources. Cependant, cette méthode apporte une complexité spatiale et qui n'optimise pas nos ressources. De plus, par rapport à la modélisation par classe mère précédente de Ressource, ces relations seront directement affectées à cette classe. Ainsi, nous pouvons une nouvelle fois procéder par héritage par classe mère ou les classes filles seront modélisées par des types de contributeur. 

Contributeur (#id : int (autoincrement), nom : string, prenom : string, date_naissance : date, nationalite : string, type{Auteur, Acteur, Realisateur, Interprete, Compositeur}) 

Contraintes pour la classe Contributeur :
- nom, prenom, date_naissance, nationalite, type NOT NULL
- Un contributeur peut être auteur si et seulement si la ressource est un livre : (type <> auteur) OR (type = auteur AND type = livre)
- Un contributeur peut être acteur et réalisateur si la ressource est un film : (type <> acteur AND type <> realisateur) OR ((type = acteur OR type = realisateur) AND type = film)
- Un contributeur peut être compositeur et interprète si la ressource est un enregistrement musical : (type <> compositeur AND type <> interprete) OR ((type = compositeur OR type = interprete) AND type = enregistrement_musical)

Vues : 
- vAuteur = Projection(Restriction(Contributeur, type = auteur), id, nom, prenom, date_naissance, nationalite)
- vActeur = Projection(Restriction(Contributeur, type = acteur), id, nom, prenom, date_naissance, nationalite)
- vRealisateur = Projection(Restriction(Contributeur, type = realisateur), id, nom, prenom, date_naissance, nationalite)
- vInterprete = Projection(Restriction(Contributeur, type = interprete), id, nom, prenom, date_naissance, nationalite)
- vCompositeur = Projection(Restriction(Contributeur, type = compositeur), id, nom, prenom, date_naissance, nationalite)

** Liaisons Ressource et Contributeur **
On choisit de créer des tables de liaisons entre chaque type de contributeur et la table Ressource. 

est_auteur(#livre=>Ressource, #auteur=>Contributeur)

Contrainte pour la classe est_auteur : 
- PROJECTION(est_auteur, auteur) = PROJECTION(Restriction(Contributeur, Contributeur.type = auteur), Contributeur.id)

est_acteur(#film=>Ressource, #acteur=>Contributeur)

Contrainte pour la classe est_acteur : 
- PROJECTION(est_acteur, acteur) = PROJECTION(Restriction(Contributeur, Contributeur.type = acteur), Contributeur.id)

est_realisateur(#film=>Ressource, #realisateur=>Contributeur)

Contrainte pour la classe est_realisateur : 
- PROJECTION(est_realisateur, realisateur) = PROJECTION(Restriction(Contributeur, Contributeur.type = realisateur), Contributeur.id)

est_interprete(#enregistrement_musical=>Ressource, #interprete=>Contributeur)

Contrainte pour la classe est_interprete : 
- PROJECTION(est_interprete, interprete) = PROJECTION(Restriction(Contributeur, Contributeur.type = interprete), Contributeur.id)

est_compositeur(#enregistrement_musical=>Ressource, #compositeur=>Contributeur)

Contrainte pour la classe est_compositeur : 
- PROJECTION(est_compositeur, compositeur) = PROJECTION(Restriction(Contributeur, Contributeur.type = compositeur), Contributeur.id)

** Utilisateur, Membre_personnel, Adhérent **
Un utilisateur peut être membre du personnel et adhérent. Il aura un login différent et unique pour son compte membre du personnel et son compte adhérent. Le login est donc bien une clé primaire et le problème de non exclusivité est réglé. Membre_personnel et Adherent hérite d’Utilisateur. Il faut désormais gérer cet héritage. On choisit donc un héritage par classes filles, plus simple au vu des relations de Prêt avec Membre_personnel et Adhérent. 

Membre_personnel(#login : string, mot_de_passe : string, nom : string, prenom : string, mail : string)

Contraintes pour la classe Membre_personnel : 
- mot_de_passe, nom, prenom, mail NOT NULL

Adherent(#login : string, mot_de_passe : string, nom : string, prenom : string, mail : string, ncarte : int, date_naissance : date, telephone : string, actuel : bool, adresse : string, est_blacklist : bool)

Contraintes pour la classe Adherent : 
- mot_de_passe, nom, prenom, mail, carte, date_naissance, telephone, actuel, est_blacklist NOT NULL
- ncarte UNIQUE

Contrainte classe Membre_personnel et Adherent : 
- Intersection(Projection(Membre_personnel, login), Projection(Adherent, login)) = {}

- Vue : 
vUtilisateur(Union(Projection(Membre_personnel, mot_de_passe, nom, prenom, mail), Projection(Adherent, mot_de_passe, nom, prenom, mail))

** Sanction, Retard, Dégradation **
Pour l'héritage entre Sanction et ses classes filles (Retard et Dégradation), on choisit l’héritage par classe mère car nous pouvons le considérer comme un héritage presque complet, de plus les classe fille Retard et Dégradation non pas de clé propre.

Sanction (#sanctionID : int, pret => Pret, type_sanction : {retard, dégradation}, nb_jours_retard : int, prix_a_payer : float, etat_apres_rendu : {bon, abîmé, perdu})

Contraintes pour la classe Sanction : 
- pret NOT NULL
- NOT (etat_apres_rendu AND type=retard)
- NOT (prix_a_payer AND type=retard)
- NOT (nb_jours AND type=dégration)
- type=retard AND nb_jours NOT NULL
- type=dégradation AND etat_apres_rendu NOT NULL
- etat_apres_rendu=perdu AND prix_a_payer

Vues : 
- vRetard = Projection(Restriction(Sanction, type = retard), sanctionID, nb_jours_retard)
- vDegradation = Projection(Restriction(Sanction, type = degradation), sanctionID, prix_a_payer, etat_apres_rendu)

** Prêt **
Pour la classe Prêt, on choisit (date_pret, exemplaire, adherent) comme clé primaire, de cette manière on s’assure de ne pas pouvoir créer deux prêts identiques le même jour du même exemplaire. 

Pret(#date_pret : date, #exemplaire => Exemplaire(id), #adherent => Adherent, membre_personnel => Membre_personnel(login), date_retour : date, date_maximale : date)

Contraintes pour la classe Pret  : 
- membre_personnel NOT NULL
- PROJECTION(Pret, membre_personnel) ⊆ PROJECTION(Membre_personnel, login)
- PROJECTION(Pret, adherent) ⊆ PROJECTION(Adherent, login)

** Exemplaire **
Pour gérer les exemplaires, il faut que chaque exemplaire ait une clé unique. L’état ou le code_ressource n’étant pas forcément uniques, il faut créer une clé primaire artificielle : id. 

Exemplaire(#id : int , etat :  {neuf, bon, abîmé, perdu}, code_ressource => Ressource(code)) 

Contraintes pour la classe Exemplaire : 
- code_ressource NOT NULL
- Projection(Ressource, code) = Projection(Exemplaire, code_ressource)

