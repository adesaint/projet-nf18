#!/usr/bin/python3

#import psycopg2

import tkinter as tk
from tkinter import messagebox, simpledialog

# Informations d'identification pour les utilisateurs
user_credentials = {
    "adherent": "mdp_adh",
    "personnel": "mdp_per"
}

# Fonction pour vérifier les informations de connexion
def check_login():
    username = username_entry.get()
    password = password_entry.get()

    if username in user_credentials and user_credentials[username] == password:
        user_type_var.set(username)  # Définir le type d'utilisateur en fonction du nom d'utilisateur
        show_menu()
        login_frame.pack_forget()  # Masquer le cadre de connexion après la connexion
        logout_button.pack(side="bottom", anchor="w")  # Afficher le bouton de déconnexion en bas à gauche
    else:
        messagebox.showerror("Erreur de connexion", "Nom d'utilisateur ou mot de passe incorrect.")

# Fonction pour afficher le menu en fonction du type d'utilisateur
def show_menu():
    user_type = user_type_var.get()
    if user_type == "adherent":
        adherent_menu()
    elif user_type == "personnel":
        personnel_menu()
    else:
        messagebox.showerror("Erreur de sélection", "Veuillez sélectionner un type d'utilisateur valide.")

# Fonction de déconnexion
def logout():
    user_type_var.set("")  # Réinitialiser le type d'utilisateur
    menu_listbox.delete(0, tk.END)  # Efface le menu à la déconnexion
    login_frame.pack()  # Afficher le cadre de connexion
    logout_button.pack_forget()  # Masquer le bouton de déconnexion
    clear_content()  # Masquer le contenu

# Menu pour les adhérents
def adherent_menu():
    menu_label.config(text="### Menu Adhérent ###")
    menu_listbox.delete(0, tk.END)  # Efface le menu précédent
    menu_listbox.insert(tk.END, "1 - Historique")
    menu_listbox.insert(tk.END, "2 - Prêt(s) en cours")
    menu_listbox.insert(tk.END, "3 - Recommandations personnelles")
    menu_listbox.insert(tk.END, "4 - Top ressources")
    menu_listbox.insert(tk.END, "5 - Rechercher une ressource")
    logout_button.pack(side="bottom", anchor="w")  # Afficher le bouton de déconnexion en bas à gauche

# Menu pour le personnel
def personnel_menu():
    menu_label.config(text="### Menu Personnel ###")
    menu_listbox.delete(0, tk.END)  # Efface le menu précédent
    menu_listbox.insert(tk.END, "1 - Gérer les ressources")
    menu_listbox.insert(tk.END, "2 - Gérer les adhérents")
    menu_listbox.insert(tk.END, "3 - Gérer les sanctions")
    menu_listbox.insert(tk.END, "4 - Gérer les prêts")
    logout_button.pack(side="bottom", anchor="w")  # Afficher le bouton de déconnexion en bas à gauche

# Fonction pour effacer le contenu de la zone de contenu
def clear_content():
    for widget in content_frame.winfo_children():
        widget.destroy()

# Fonction pour afficher le titre du menu sélectionné
def show_menu_title(menu_item):
    title_label.config(text="Titre: " + menu_item)

# Fonction pour gérer le double-clic sur un élément du menu
def menu_double_click(event):
    selected_item = menu_listbox.get(tk.ACTIVE)
    if selected_item == "1 - Gérer les ressources":
        add_resource()
    else:
        show_menu_title(selected_item)

def add_resource():
    resource_title = simpledialog.askstring("Ajouter une ressource", "Entrez le titre de la ressource:")
    resource_description = simpledialog.askstring("Ajouter une ressource", "Entrez la description de la ressource:")

# Créer une fenêtre
window = tk.Tk()
window.title("Page de connexion")
window.attributes('-fullscreen', True)  # Ouvrir en plein écran

# Créer une variable pour stocker le type d'utilisateur
user_type_var = tk.StringVar()

# Cadre de connexion
login_frame = tk.Frame(window)
login_frame.pack()

# Créer des étiquettes et des champs de saisie pour le nom d'utilisateur et le mot de passe
username_label = tk.Label(login_frame, text="Nom d'utilisateur")
username_label.pack()
username_entry = tk.Entry(login_frame)
username_entry.pack()

password_label = tk.Label(login_frame, text="Mot de passe")
password_label.pack()
password_entry = tk.Entry(login_frame, show="*")  # Pour masquer le mot de passe
password_entry.pack()

# Créer un bouton de connexion
login_button = tk.Button(login_frame, text="Se connecter", command=check_login)
login_button.pack()

# Bouton de déconnexion (masqué initialement)
logout_button = tk.Button(window, text="Se déconnecter", command=logout)

# Cadre pour le menu
menu_frame = tk.Frame(window)
menu_frame.pack(side="left", fill="y")

# Étiquette pour afficher le menu sélectionné
menu_label = tk.Label(menu_frame, text="Menu")
menu_label.pack()

# Liste pour afficher le menu
menu_listbox = tk.Listbox(menu_frame, selectmode=tk.SINGLE)
menu_listbox.pack(fill=tk.BOTH, expand=True)

# Ajouter un gestionnaire d'événements pour le double-clic sur le menu
menu_listbox.bind('<Double-1>', menu_double_click)

# Cadre pour le contenu à droite du menu
content_frame = tk.Frame(window)
content_frame.pack(side="right", fill="both", expand=True)

# Étiquette pour afficher le titre du menu sélectionné
title_label = tk.Label(content_frame, text="Titre:")
title_label.pack()

# Exécutez la boucle principale
window.mainloop()

'''
import tkinter as tk
from tkinter import messagebox

# Informations d'identification pour les utilisateurs
user_credentials = {
    "adherent": "mdp_adh",
    "personnel": "mdp_per"
}

# Fonction pour vérifier les informations de connexion
def check_login():
    username = username_entry.get()
    password = password_entry.get()

    if username in user_credentials and user_credentials[username] == password:
        user_type_var.set(username)  # Définir le type d'utilisateur en fonction du nom d'utilisateur
        show_menu()
        login_frame.pack_forget()  # Masquer le cadre de connexion après la connexion
        logout_button.pack(side="bottom", anchor="w")  # Afficher le bouton de déconnexion en bas à gauche
    else:
        messagebox.showerror("Erreur de connexion", "Nom d'utilisateur ou mot de passe incorrect.")

# Fonction pour afficher le menu en fonction du type d'utilisateur
def show_menu():
    user_type = user_type_var.get()
    if user_type == "adherent":
        adherent_menu()
    elif user_type == "personnel":
        personnel_menu()
    else:
        messagebox.showerror("Erreur de sélection", "Veuillez sélectionner un type d'utilisateur valide.")

# Fonction de déconnexion
def logout():
    user_type_var.set("")  # Réinitialiser le type d'utilisateur
    window.destroy()  # Fermer la fenêtre principale pour quitter l'application

# Menu pour les adhérents
def adherent_menu():
    menu_label.config(text="### Menu Adhérent ###")
    menu_listbox.delete(0, tk.END)  # Efface le menu précédent
    menu_listbox.insert(tk.END, "1 - Historique")
    menu_listbox.insert(tk.END, "2 - Prêt(s) en cours")
    menu_listbox.insert(tk.END, "3 - Recommandations personnelles")
    menu_listbox.insert(tk.END, "4 - Top ressources")
    menu_listbox.insert(tk.END, "5 - Rechercher une ressource")
    logout_button.pack(side="bottom", anchor="w")  # Afficher le bouton de déconnexion en bas à gauche
    menu_listbox.pack(side="left", fill="y")

# Menu pour le personnel
def personnel_menu():
    menu_label.config(text="### Menu Personnel ###")
    menu_listbox.delete(0, tk.END)  # Efface le menu précédent
    menu_listbox.insert(tk.END, "1 - Gérer les ressources")
    menu_listbox.insert(tk.END, "2 - Gérer les adhérents")
    menu_listbox.insert(tk.END, "3 - Gérer les sanctions")
    menu_listbox.insert(tk.END, "4 - Gérer les prêts")
    logout_button.pack(side="bottom", anchor="w")  # Afficher le bouton de déconnexion en bas à gauche
    menu_listbox.pack(side="left", fill="y")

# Fonction pour afficher la gestion des ressources
def show_manage_resources():
    clear_content()
    
    # Afficher le titre "Gestion des ressources"
    title_label = tk.Label(content_frame, text="Gestion des ressources", font=("Helvetica", 16, "bold"))
    title_label.pack()

    if user_type_var.get() == "personnel":
        # Bouton "Ajouter ressource"
        add_button = tk.Button(content_frame, text="Ajouter ressource", command=add_resource)
        add_button.pack()

        # Bouton "Modifier ressource"
        edit_button = tk.Button(content_frame, text="Modifier ressource", command=edit_resource)
        edit_button.pack()

        # Bouton "Supprimer ressource"
        delete_button = tk.Button(content_frame, text="Supprimer ressource", command=delete_resource)
        delete_button.pack()

# Fonction pour afficher le formulaire d'ajout de ressource
def add_resource():
    clear_content()

    # Afficher le titre "Ajouter ressource"
    title_label = tk.Label(content_frame, text="Ajouter ressource", font=("Helvetica", 16, "bold"))
    title_label.pack()

    # Champs de saisie pour le titre et la description
    title_label = tk.Label(content_frame, text="Titre de la ressource")
    title_label.pack()
    title_entry = tk.Entry(content_frame)
    title_entry.pack()

    description_label = tk.Label(content_frame, text="Description de la ressource")
    description_label.pack()
    description_entry = tk.Entry(content_frame)
    description_entry.pack()

    # Bouton pour ajouter la ressource
    add_button = tk.Button(content_frame, text="Ajouter", command=save_resource)
    add_button.pack()

# Fonction pour enregistrer la ressource
def save_resource():
    # Récupérer le titre et la description saisis par l'utilisateur
    title = title_entry.get()
    description = description_entry.get()

    # Ajouter ici la logique pour enregistrer la ressource (par exemple, dans une base de données)

    # Afficher un message de confirmation
    messagebox.showinfo("Ressource ajoutée", "La ressource a été ajoutée avec succès.")

# Fonction pour effacer le contenu de la zone de contenu
def clear_content():
    for widget in content_frame.winfo_children():
        widget.destroy()

# Créer une fenêtre
window = tk.Tk()
window.title("Page de connexion")
window.attributes('-fullscreen', True)  # Ouvrir en plein écran

# Créer une variable pour stocker le type d'utilisateur
user_type_var = tk.StringVar()

# Cadre de connexion
login_frame = tk.Frame(window)
login_frame.pack()

# Créer des étiquettes et des champs de saisie pour le nom d'utilisateur et le mot de passe
username_label = tk.Label(login_frame, text="Nom d'utilisateur")
username_label.pack()
username_entry = tk.Entry(login_frame)
username_entry.pack()

password_label = tk.Label(login_frame, text="Mot de passe")
password_label.pack()
password_entry = tk.Entry(login_frame, show="*")  # Pour masquer le mot de passe
password_entry.pack()

# Créer un bouton de connexion
login_button = tk.Button(login_frame, text="Se connecter", command=check_login)
login_button.pack()

# Bouton de déconnexion (masqué initialement)
logout_button = tk.Button(window, text="Se déconnecter", command=logout)

# Cadre pour le menu
menu_frame = tk.Frame(window)
menu_frame.pack(side="left", fill="y")

# Cadre pour le contenu
content_frame = tk.Frame(window)
content_frame.pack(side="right", fill="both", expand=True)

# Étiquette pour afficher le menu sélectionné
menu_label = tk.Label(window, text="Menu")

# Liste pour afficher le menu
menu_listbox = tk.Listbox(window)

# Exécutez la boucle principale
window.mainloop()

HOST = "localhost"
USER = "me"
PASSWORD = "secret"
DATABASE = "mydb"

# Open connection
conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))

# Open a cursor to send SQL commands
cur = conn.cursor()

# Menu python
print("#### MENU ####\n")
print("1 - ")

# Execute a SQL INSERT command
sql = "INSERT INTO t VALUES ('Hello',1)"
cur.execute(sql)

# Execute a SQL SELECT command
sql = "SELECT * FROM t"
cur.execute(sql)

# Fetch data line by line
raw = cur.fetchone()
while raw:
    print (raw[0])
    print (raw[1])
    raw = cur.fetchone()

# Commit (transactionnal mode is by default)
conn.commit()

# Close connection
conn.close()
'''