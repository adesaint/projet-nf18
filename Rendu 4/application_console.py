import psycopg2
from getpass import getpass
import os

def clear_terminal():
    os.system('clear')
    
def wait_for_enter():
    input("Appuyez sur Entrée pour continuer...")
    clear_terminal()

def connect_db():
    try:
        HOST="tuxa.sme.utc"
        USER= "nf18a012"
        DATABASE="dbnf18a012"
        PAS="1kRpG5zJYaso"
        connection = psycopg2.connect(" dbname =%s host=%s user=%s password=%s" % (DATABASE, HOST, USER, PAS))
        return connection
    except (Exception, psycopg2.Error) as error:
        print("Erreur lors de la connexion à la base de données :", error)
        return None

def execute_query(connection, query, values = ()):
    try:
        cursor = connection.cursor()
        cursor.execute(query, values)
        result = cursor.fetchall()
        connection.commit()
        return result
    except (Exception, psycopg2.Error) as error:
        print("Erreur lors de l'exécution de la requête :", error)
        return None

def execute_update_query(connection, query, values):
    try:
        cursor = connection.cursor()
        cursor.execute(query, values)
        connection.commit()
    except (Exception, psycopg2.Error) as error:
        print("Erreur lors de l'exécution de la requête :", error)
        
global_username = ""
        
def authenticate_user(connection):
    global global_username
    
    while True:
        clear_terminal()
        print("###########################")
        print("# BIBLIOTHEQUE MUNICIPALE #")
        print("###########################\n\n")
        print("## Espace de connexion ##\n")
        username = input("Nom d'utilisateur : ")
        password = getpass("Mot de passe : ")

        result_adherent = execute_query(connection, f"SELECT * FROM Adherent WHERE login = '{username}' AND mot_de_passe = '{password}';")

        result_personnel = execute_query(connection, f"SELECT * FROM Membre_personnel WHERE login = '{username}' AND mot_de_passe = '{password}';")

        if result_adherent:
            global_username = username
            return "adherent"
        elif result_personnel:
            global_username = username
            return "personnel"
        elif username == "root" and password == "root":
            global_username = username
            return "root"
        else:
            print("Nom d'utilisateur ou mot de passe incorrect.")
            input("Appuyez sur Entrée pour réessayer...")

def main():
    connection = connect_db()

    if connection:
        user_type = authenticate_user(connection)
        clear_terminal()
        
        while True:            
            if user_type == "adherent":
                print("###########################")
                print("# BIBLIOTHEQUE MUNICIPALE #")
                print("###########################\n\n")
                print("## Espace adhérent ##")
                print("Menu principal :\n")
                print("1. Afficher toutes les ressources")
                print("2. Afficher les ressources de type livre")
                print("3. Afficher les ressources de type film")
                print("4. Afficher les ressources de type enregistrement musical")
                print("5. Afficher les ressources neuves")
                print("15. Top des ressources les plus empruntées")
                print("16. Liste des meilleurs contributeurs")
                print("26. Historique des emprunts")
                print("27. Emprunt(s) en cours")
                print("28. Liste des sanctions")
                print("Q. Quitter")
            elif user_type == "personnel" : 
                print("###########################")
                print("# BIBLIOTHEQUE MUNICIPALE #")
                print("###########################\n\n")
                print("## Espace membre du personnel ##")
                print("Menu principal :\n")
                print("1. Afficher toutes les ressources")
                print("2. Afficher les ressources de type livre")
                print("3. Afficher les ressources de type film")
                print("4. Afficher les ressources de type enregistrement musical")
                print("5. Afficher les ressources neuves")
                print("6. Afficher tous les adhérents")
                print("7. Afficher tous les membres du personnel")
                print("8. Modifier la description d'un film")
                print("9. Nombre de prêts total")
                print("10. Nombre de prêts en cours")
                print("11. Historique des prêts")
                print("12. Prêts en cours")
                print("13. Liste des adhérents non blacklistés")
                print("14. Liste des adhérents blacklistés")
                print("15. Top des ressources les plus empruntées")
                print("16. Liste des meilleurs contributeurs")
                print("17. Somme d'argent dûe par chaque adhérent")
                print("18. Liste de tous les retards")
                print("19. Liste de toutes les dégradations")
                print("20. Supprimer une ressource")
                print("21. Ajouter un retard")
                print("22. Ajouter une dégradation")
                print("23. Liste des sanctions pour un adhérent")
                print("Q. Quitter")
            elif user_type == "root":
                print("###########################")
                print("# BIBLIOTHEQUE MUNICIPALE #")
                print("###########################\n\n")
                print("## Espace root ##")
                print("Menu principal :\n")
                print("24. Créer une table")
                print("25. Supprimer une table")
                print("Q. Quitter")
                

            choice = input("Choisissez une option : ")
            
            if user_type == "adherent" and choice != "Q" and not (1 <= int(choice) <= 5) and not (15 <= int(choice) <= 16) and not (26 <= int(choice) <= 28):
                print("Option invalide. Veuillez choisir une option valide.")
            elif user_type == "personnel" and choice != "Q" and not (int(choice) <= 23):
                print("Option invalide. Veuillez choisir une option valide.")
            elif user_type == "root" and choice != "Q" and not (24 <= int(choice) <= 25):
                print("Option invalide. Veuillez choisir une option valide.")              
            elif choice == "1":
                result = execute_query(connection, "SELECT * FROM Ressource;")
                print("\n## Liste des ressources ##\n")
                if result:
                    for row in result:
                        print(row)
                else: 
                    print("Aucune ressource")
                print("\n\n")
            elif choice == "2":
                result = execute_query(connection, "SELECT * FROM Ressource WHERE type='livre';")
                print("\n## Liste des livres ##\n")
                if result:
                    for row in result:
                        print(row)
                else:
                    print("Aucun livre")
                print("\n\n")
            elif choice == "3":
                result = execute_query(connection, "SELECT * FROM Ressource WHERE type='film';")
                print("\n## Liste des films ##\n")
                if result:
                    for row in result:
                        print(row)
                else:
                    print("Aucun film")
                print("\n\n")
            elif choice == "4":
                result = execute_query(connection, "SELECT * FROM Ressource WHERE type='enregistrement_musical';")
                print("\n## Liste des enregistrements musicaux ##\n")
                if result:
                    for row in result:
                        print(row)
                else:
                    print("Aucun enregistrement musical")
                print("\n\n")
            elif choice == "5":
                result = execute_query(connection, "SELECT R.* FROM Ressource AS R JOIN Exemplaire AS E ON R.code = E.code_ressource WHERE E.etat = 'neuf';")
                print("\n## Liste des ressources neuves ##\n")
                if result:
                    for row in result:
                        print(row)
                else:
                    print("Aucune ressource neuve")
                print("\n\n")
            elif choice == "6":
                result = execute_query(connection, "SELECT * FROM Adherent;")
                print("\n## Liste des adhérents ##\n")
                if result:
                    for row in result:
                        print(row)
                else:
                    print("Aucun adhérent")
                print("\n\n")
            elif choice == "7":
                result = execute_query(connection, "SELECT * FROM Membre_personnel;")
                print("\n## Liste des membres du personnel ##\n")
                if result:
                    for row in result:
                        print(row)
                else:
                    print("Aucun membre du personnel")
                print("\n\n")
            elif choice == "8":
                print("\n## Modification de la description d'un film ##\n")
                liste_films = execute_query(connection, "SELECT * FROM Ressource WHERE type = 'film';")
                if liste_films :
                    print("Voici la liste des films : \n")
                    for row in liste_films:
                        print(row)
                    film = input("\nQuelle description de film voulez-vous modifier ? (entrez le code du film) : ")
                    description = input("\nNouvelle description : ")
                    update_query = "UPDATE Ressource SET synopsis = %s WHERE code = %s;"
                    update_values = (description, film)
                    execute_update_query(connection, update_query, update_values)
                    print("Description modifiée avec succès !\n\n")
                else :
                    print("Aucun film dans la base de données\n\n")
            elif choice == "9":
                result = execute_query(connection, "SELECT COUNT(R.code), Pret.adherent FROM Ressource AS R INNER JOIN Exemplaire ON Exemplaire.code_ressource = R.code INNER JOIN Pret ON Pret.exemplaire = Exemplaire.id GROUP BY Pret.adherent;")
                print("\n## Nombre de prêts total ##\n")
                if result:
                    for row in result:
                        print(row)
                    print("\n\n")
            elif choice == "10":
                result = execute_query(connection, "SELECT COUNT(R.code), Pret.adherent FROM Ressource AS R INNER JOIN Exemplaire ON Exemplaire.code_ressource = R.code INNER JOIN Pret ON Pret.exemplaire = Exemplaire.id WHERE Pret.date_retour > CURRENT_DATE GROUP BY Pret.adherent;")
                print("\n## Nombre de prêts en cours ##\n")
                if result:
                    for row in result:
                        print(row)
                print("Aucun prêt en cours")
                print("\n\n")
            elif choice == "11":
                result = execute_query(connection, "SELECT R.titre, R.type, pret.adherent FROM Ressource AS R INNER JOIN Exemplaire ON Exemplaire.code_ressource = R.code INNER JOIN Pret ON Pret.exemplaire = Exemplaire.id GROUP BY R.titre, R.type, Pret.adherent;")
                print("\n## Historique des prêts ##\n")
                if result:
                    for row in result:
                        print(row)
                else:
                    print("Aucun prêt")
                print("\n\n")
            elif choice == "12":
                result = execute_query(connection, "SELECT R.titre, R.type, pret.adherent FROM Ressource AS R INNER JOIN Exemplaire ON Exemplaire.code_ressource = R.code INNER JOIN Pret ON Pret.exemplaire = Exemplaire.id WHERE Pret.date_retour > CURRENT_DATE GROUP BY R.titre, R.type, Pret.adherent;")
                print("\n## Liste des prêts en cours ##\n")
                if result:
                    for row in result:
                        print(row)
                else:
                    print("Aucun prêt")
                print("\n\n")
            elif choice == "13":
                result = execute_query(connection, "SELECT A.nom, A.prenom, A.login, A.ncarte FROM adherent as A WHERE NOT est_blacklist;")
                print("\n## Liste des adhérents non blacklistés ##\n")
                if result:
                    for row in result:
                        print(row)
                else:
                    print("Aucun adhérent non blacklisté")
                print("\n\n")
            elif choice == "14":
                result = execute_query(connection, "SELECT A.nom, A.prenom, A.login, A.ncarte FROM adherent as A WHERE est_blacklist;")
                print("\n## Liste des adhérents blacklistés ##\n")
                if result:
                    for row in result:
                        print(row)
                else:
                    print("Aucun adhérent blacklisté")
                print("\n\n")
            elif choice == "15":
                result = execute_query(connection, "SELECT R.titre, COUNT(Pret.exemplaire) AS fois_empruntee FROM Ressource AS R INNER JOIN Exemplaire ON Exemplaire.code_ressource = R.code INNER JOIN Pret ON Pret.exemplaire = Exemplaire.id GROUP BY R.titre ORDER BY fois_empruntee DESC LIMIT 10;")
                print("\n## Top ressources les plus empruntées ##\n")
                if result:
                    for row in result:
                        print(row)
                else:
                    print("Aucune ressource empruntée")
                print("\n\n")
            elif choice == "16":
                result = execute_query(connection, "SELECT C.nom, C.prenom, C.type, COUNT(Exemplaire.id) AS nombre_de_ressources FROM Contributeur AS C LEFT JOIN Ressource ON C.id = Ressource.code LEFT JOIN Exemplaire ON Ressource.code = Exemplaire.code_ressource GROUP BY C.nom, C.prenom, C.type ORDER BY nombre_de_ressources DESC LIMIT 10;")
                print("\n## Liste des meilleurs contributeurs ##\n")
                if result:
                    for row in result:
                        print(row)
                else:
                    print("Aucun contributeur")
                print("\n\n")
            elif choice == "17":
                result = execute_query(connection, "SELECT SUM(S.prix_a_payer) AS total_prix_a_payer, P.adherent FROM Sanction AS S INNER JOIN Pret AS P ON S.pret_date = P.date_pret AND S.pret_exemplaire = P.exemplaire AND S.pret_adherent = P.adherent GROUP BY P.adherent; ")
                print("\n## Somme dûe par chaque adhérent ##\n")
                if result:
                    for row in result:
                        print(row)
                else:
                    print("Aucune somme dûe")
                print("\n\n")
            elif choice == "18":
                result = execute_query(connection, "SELECT P.*, S.type_sanction, S.nb_jours_retard, S.prix_a_payer, S.etat_apres_rendu FROM Pret AS P INNER JOIN Sanction AS S ON P.date_pret = S.pret_date AND P.exemplaire = S.pret_exemplaire AND P.adherent = S.pret_adherent WHERE S.type_sanction = 'retard'; ")
                print("\n## Liste des retards ##\n")
                if result:
                    for row in result:
                        print(row)
                print("Aucun retard")
                print("\n\n")
            elif choice == "19":
                result = execute_query(connection, "SELECT P.*, S.type_sanction, S.nb_jours_retard, S.prix_a_payer, S.etat_apres_rendu FROM Pret AS P INNER JOIN Sanction AS S ON P.date_pret = S.pret_date AND P.exemplaire = S.pret_exemplaire AND P.adherent = S.pret_adherent WHERE S.type_sanction = 'degradation'; ")
                print("\n## Liste des dégradations ##\n")
                if result:
                    for row in result:
                        print(row)
                print("Aucune dégradation")
                print("\n\n")
            elif choice == "20":
                liste_ressources = execute_query(connection, "SELECT * FROM Ressource;")
                print("\n## Suppression d'une ressource ##\n")
                if liste_ressources :
                    print("Voici la liste des ressources : \n")
                    for row in liste_ressources:
                        print(row)
                    ressource = input("Quelle ressource voulez-vous supprimer ? (entrez le code de la ressource) : ")
                    update_query = "UPDATE EXEMPLAIRE SET etat='perdu' WHERE id=%s;"
                    update_values = (ressource)
                    execute_update_query(connection, update_query, update_values)
                else :
                    print("Aucune ressource dans la base de données")
                print("\n\n")
            elif choice == "21":
                liste_prets = execute_query(connection, "SELECT date_pret, exemplaire, adherent FROM Pret WHERE date_maximale > CURRENT_DATE;")
                print("\n## Ajouter un retard ##\n")
                if liste_ressources :
                    print("Voici la liste des prêts en retard non rendus : \n")
                    for row in liste_prets :
                        print(row)
                    date = input("Date du prêt : ")
                    exemplaire = input("Exemplaire : ")
                    adherent = input("Adhérent : ")
                    retard = input("Nombre de jours de retard : ")
                    update_query = "INSERT INTO Sanction (pret_date, pret_exemplaire, pret_adherent, type_sanction, nb_jours_retard) VALUES (%s, %s, %s, 'retard', %s);"
                    update_values = (date, exemplaire, adherent, retard)
                    execute_update_query(connection, update_query, update_values)
                else :
                    print("Aucun prêt en retard")
                print("\n\n")
            elif choice == "22":
                liste_prets = execute_query(connection, "SELECT date_pret, exemplaire, adherent FROM Pret WHERE date_retour IS NULL;")
                print("\n## Ajouter une dégradation ##\n")
                if liste_ressources :
                    print("Voici la liste des prêts en cours : \n")
                    for row in liste_prets :
                        print(row)
                    date = input("Date du prêt : ")
                    exemplaire = input("Exemplaire : ")
                    adherent = input("Adhérent : ")
                    etat = input("Nouvel état (bon, abîmé, perdu) : ")
                    update_query = "INSERT INTO Sanction (pret_date, pret_exemplaire, pret_adherent, type_sanction, etat_apres_rendu) VALUES (%s, %s, %s, 'dégradation', %s);"
                    update_values = (date, exemplaire, adherent, etat)
                    execute_update_query(connection, update_query, update_values)
                else :
                    print("Aucun prêt en cours")
                print("\n\n")
            elif choice == "23":
                liste_adherents = execute_query(connection, "SELECT * FROM Adherent;")
                print("\n## Liste des sanctions pour un adhérent ##\n")
                if liste_adherents :
                    print("Voici la liste des adhérents : \n")
                    for row in liste_adherents :
                        print(row)
                    login = input("Login : ")
                    update_query = "SELECT * FROM Sanction WHERE pret_adherent = %s;"
                    update_values = (login,)
                    sanctions = execute_query(connection, update_query, update_values)
                    if sanctions:
                        print("\n## Liste des sanctions pour l'adhérent ##\n")
                        for row in sanctions:
                            print(row)
                    else:
                        print(f"Aucune sanction trouvée")
                else :
                    print("Aucun adhérent")
                print("\n\n")
            elif choice == "24":
                try:
                    cursor = connection.cursor()
                    table_name = input("Entrez le nom de la table à créer: ")
                    columns_input = input("Entrez les colonnes de la table avec leurs types de données (séparées par des virgules): ")
                    columns = [col.strip() for col in columns_input.split(',')]
                    create_table_query = f"CREATE TABLE IF NOT EXISTS {table_name} ({', '.join(columns)});"
                    cursor.execute(create_table_query)
                    connection.commit()
                    print(f"Table '{table_name}' créée avec succès.")
                except (Exception, psycopg2.Error) as error:
                    print(f"Erreur lors de la création de la table '{table_name}': {error}")
            elif choice == "25":
                try:
                    cursor = connection.cursor()
                    table_name = input("Entrez le nom de la table à détruire: ")
                    drop_table_query = f"DROP TABLE IF EXISTS {table_name};"
                    cursor.execute(drop_table_query)
                    connection.commit()
                    print(f"Table '{table_name}' détruite avec succès.")
                except (Exception, psycopg2.Error) as error:
                    print(f"Erreur lors de la suppression de la table '{table_name}': {error}")
            elif choice == "26":
                print("\n## Historique des emprunts ##\n")
                result = execute_query(connection, f"SELECT P.date_pret, P.date_retour, R.titre, E.etat FROM Pret AS P JOIN Exemplaire AS E ON P.exemplaire = E.id JOIN RESSOURCE AS R ON E.code_ressource = R.code WHERE P.adherent = '{global_username}';")
                if result:
                    for row in result:
                        print(row)
                else:
                    print("Aucune ressource empruntée")
                print("\n\n")
            elif choice == "27":
                print("\n## Emprunts en cours ##\n")
                result = execute_query(connection, f"SELECT P.date_pret, P.date_retour, R.titre, E.etat FROM Pret AS P JOIN Exemplaire AS E ON P.exemplaire = E.id JOIN RESSOURCE AS R ON E.code_ressource = R.code WHERE P.adherent = '{global_username}' AND P.date_retour IS NULL;")
                if result:
                    for row in result:
                        print(row)
                else:
                    print("Aucune ressource empruntée")
                print("\n\n")
            elif choice == "28":
                print("\n## Sanctions ##\n")
                result = execute_query(connection, f"SELECT * FROM Sanction WHERE pret_adherent = '{global_username}';")
                if result:
                    for row in result:
                        print(row)
                else:
                    print("Aucune sanction")
                print("\n\n")
            elif choice == "Q":
                break
            else:
                print("\n\n/!\ Option invalide. Veuillez choisir une option valide.\n")

            wait_for_enter()
            
        connection.close()
        print("Programme terminé.")

if __name__ == "__main__":
    main()
