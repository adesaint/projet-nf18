import tkinter as tk
from tkinter import messagebox
import psycopg2
import os
from PIL import Image, ImageTk
from tkinter import Scrollbar
from tkinter import simpledialog, messagebox
from tkinter.ttk import Combobox
from datetime import datetime, timedelta


class LibraryApp:
    def __init__(self, root):
        self.root = root
        self.root.title("Bibliothèque Municipale")
        icon_path = os.path.abspath("icone_biblio.ico")

        
        icon = Image.open(icon_path)
        icon = ImageTk.PhotoImage(icon)

        
        self.root.tk.call('wm', 'iconphoto', self.root._w, icon)

        self.connection = None
        self.global_username = tk.StringVar()

        self.create_login_screen()

    def create_login_screen(self):
        login_frame = tk.Frame(self.root)
        login_frame.pack(padx=10, pady=10)
        
        tk.Label(login_frame, text="Espace de connexion", font=("Helvetica", 12, "bold")).grid(row=0, column=0, columnspan=2)

        tk.Label(login_frame, text="Nom d'utilisateur:").grid(row=1, column=0, sticky="e", pady=10)
        self.username_entry = tk.Entry(login_frame)
        self.username_entry.grid(row=1, column=1)

        tk.Label(login_frame, text="Mot de passe:").grid(row=2, column=0, sticky="e")
        self.password_entry = tk.Entry(login_frame, show="*")
        self.password_entry.grid(row=2, column=1)

        login_button = tk.Button(login_frame, text="Connexion", font=("Helvetica", 10, "bold"), fg="green", command=self.authenticate_user)
        login_button.grid(row=3, column=1, pady=10)

    def authenticate_user(self):
        username = self.username_entry.get()
        password = self.password_entry.get()

        try:
            self.connection = psycopg2.connect(" dbname=dbnf18a012 host=tuxa.sme.utc user=nf18a012 password=1kRpG5zJYaso")
        except (Exception, psycopg2.Error) as error:
            messagebox.showerror("Erreur de connexion", f"Erreur lors de la connexion à la base de données : {error}")
            return

        result_adherent = self.execute_query(f"SELECT * FROM Adherent WHERE login = '{username}' AND mot_de_passe = '{password}';")
        result_personnel = self.execute_query(f"SELECT * FROM Membre_personnel WHERE login = '{username}' AND mot_de_passe = '{password}';")

        if result_adherent:
            self.global_username.set(username)
            self.show_user_menu("adherent")
        elif result_personnel:
            self.global_username.set(username)
            self.show_user_menu("personnel")
        elif username == "root" and password == "root":
            self.global_username.set(username)
            self.show_user_menu("root")
        else:
            messagebox.showerror("Erreur d'authentification", "Nom d'utilisateur ou mot de passe incorrect.")
    
    def is_adherent(self):
        return self.execute_query(f"SELECT COUNT(*) FROM Adherent WHERE login = '{self.global_username.get()}';", fetchall=False)[0] > 0
    
    def emprunts_possibles(self):
        return self.execute_query(f"SELECT COUNT(*) FROM Pret WHERE adherent = '{self.global_username.get()}' AND date_retour IS NULL;", fetchall=False)[0] < 5 
    
    def endette(self):
        return self.execute_query(f"SELECT SUM(prix_a_payer) FROM Sanction WHERE pret_adherent = '{self.global_username.get()}';", fetchall=False)[0] 
         
    def est_retard(self):
        return self.execute_query(f"SELECT COUNT(*) FROM Pret WHERE adherent = '{self.global_username.get()}' AND date_retour IS NULL AND date_maximale < CURRENT_DATE;", fetchall=False)[0] > 0

    def execute_query(self, query, values=(), fetchall=True):
        try:
            cursor = self.connection.cursor()
            cursor.execute(query, values)
            result = cursor.fetchall() if fetchall else cursor.fetchone()
            self.connection.commit()
            return result
        except (Exception, psycopg2.Error) as error:
            messagebox.showerror("Erreur de requête", f"Erreur lors de l'exécution de la requête : {error}")
            return None
        
    def execute_no_return_query(self, query, values=()):
        try:
            cursor = self.connection.cursor()
            cursor.execute(query, values)
            self.connection.commit()
        except (Exception, psycopg2.Error) as error:
            messagebox.showerror("Erreur de requête", f"Erreur lors de l'exécution de la requête : {error}")

    def show_user_menu(self, user_type):
        for widget in self.root.winfo_children():
            widget.destroy()

        main_frame = tk.Frame(self.root)
        main_frame.pack(padx=10, pady=10)

        tk.Label(main_frame, text=f"Bienvenue, {self.global_username.get()}!", font=("Helvetica", 16, "bold italic")).grid(row=0, column=0, columnspan=2, pady=10)

        menu_frame = tk.Frame(main_frame)
        menu_frame.grid(row=1, column=0, padx=10, pady=10)

        if user_type == "adherent":
            self.create_adherent_menu(menu_frame)
        elif user_type == "personnel":
            self.create_personnel_menu(menu_frame)
        elif user_type == "root":
            self.create_root_menu(menu_frame)

    def create_adherent_menu(self, frame):
        tk.Label(frame, text="Voir les ressources :", font=("Helvetica", 12)).grid(row=1, column=0, columnspan=5, pady=10)
        
        buttons_frame = tk.Frame(frame)
        buttons_frame.grid(row=2, column=0, columnspan=5, pady=5)

        button_width = 20  

        book_button = tk.Button(buttons_frame, text="Livres", command=self.show_books, width=button_width)
        book_button.grid(row=0, column=0, pady=5, padx=1)

        film_button = tk.Button(buttons_frame, text="Films", command=self.show_films, width=button_width)
        film_button.grid(row=0, column=1, pady=5, padx=1)

        music_button = tk.Button(buttons_frame, text="Musiques", command=self.show_music, width=button_width)
        music_button.grid(row=0, column=2, pady=5, padx=1)

        new_button = tk.Button(buttons_frame, text="Nouveautés", command=self.show_ressources_neuves, width=button_width)
        new_button.grid(row=0, column=3, pady=5, padx=1)

        top_ressources_button = tk.Button(buttons_frame, text="Top 10", command=self.show_top_ressources, width=button_width)
        top_ressources_button.grid(row=0, column=4, pady=5, padx=1)

        tk.Label(frame, text="Contributeurs :", font=("Helvetica", 12)).grid(row=3, column=0, columnspan=5, pady=10)

        top_contributeurs_button = tk.Button(frame, text="Meilleurs contributeurs", command=self.show_meilleurs_contributeurs, width=button_width)
        top_contributeurs_button.grid(row=4, column=0, columnspan=5, pady=5, padx=1)

        tk.Label(frame, text="Voir mes prêts :", font=("Helvetica", 12)).grid(row=5, column=0, columnspan=5, pady=10)

        demand_button = tk.Button(frame, text="Demandes d'emprunts", command=self.display_demandes, width=button_width)
        demand_button.grid(row=6, column=1, columnspan=2, pady=5, padx=1)

        loan_button = tk.Button(frame, text="Prêts en cours", command=self.show_emprunt_cours, width=button_width)
        loan_button.grid(row=6, column=1, columnspan=2, pady=5, padx=1)
        
        historique_button = tk.Button(frame, text="Historique", command=self.show_historique_emprunt, width=button_width)
        historique_button.grid(row=6, column=2, columnspan=2, pady=5, padx=1)

        password_button = tk.Button(frame, text="Nouveau mot de passe", font=("Helvetica", 10, "bold"), fg="blue", command=self.modify_password, width=button_width)
        password_button.grid(row=10, column=1, columnspan=2, pady=50)
        
        exit_button = tk.Button(frame, text="Déconnexion", font=("Helvetica", 10, "bold"), fg="red", command=self.fin_programe, width=button_width)
        exit_button.grid(row=10, column=2, columnspan=2, pady=50)

    def create_personnel_menu(self, frame):
        tk.Label(frame, text="Gérer les ressources :", font=("Helvetica", 12)).grid(row=1, column=0, columnspan=5, pady=10)
        
        buttons_frame = tk.Frame(frame)
        buttons_frame.grid(row=2, column=0, columnspan=5, pady=5)

        button_width = 20  

        book_button = tk.Button(buttons_frame, text="Livres", command=self.show_books, width=button_width)
        book_button.grid(row=0, column=0, pady=5, padx=1)

        film_button = tk.Button(buttons_frame, text="Films", command=self.show_films, width=button_width)
        film_button.grid(row=0, column=1, pady=5, padx=1)

        music_button = tk.Button(buttons_frame, text="Musiques", command=self.show_music, width=button_width)
        music_button.grid(row=0, column=2, pady=5, padx=1)

        new_button = tk.Button(buttons_frame, text="Nouveautés", command=self.show_ressources_neuves, width=button_width)
        new_button.grid(row=0, column=3, pady=5, padx=1)

        top_ressources_button = tk.Button(buttons_frame, text="Top 10", command=self.show_top_ressources, width=button_width)
        top_ressources_button.grid(row=0, column=4, pady=5, padx=1)

        tk.Label(frame, text="Contributeurs :", font=("Helvetica", 12)).grid(row=3, column=0, columnspan=5, pady=10)

        top_contributeurs_button = tk.Button(frame, text="Meilleurs contributeurs", command=self.show_meilleurs_contributeurs, width=button_width)
        top_contributeurs_button.grid(row=4, column=0, columnspan=5, pady=5, padx=1)

        tk.Label(frame, text="Gérer les prêts :", font=("Helvetica", 12)).grid(row=5, column=0, columnspan=5, pady=10)

        demand_button = tk.Button(frame, text="Demandes d'emprunts", command=self.display_demandes, width=button_width)
        demand_button.grid(row=6, column=1, pady=5, padx=1)

        loan_button = tk.Button(frame, text="Prêts en cours", command=self.show_current_loans, width=button_width)
        loan_button.grid(row=6, column=2, pady=5, padx=1)
        
        historic_button = tk.Button(frame, text="Historique des prêts", command=self.show_historique, width=button_width)
        historic_button.grid(row=6, column=3, pady=5, padx=1)

        tk.Label(frame, text="Gérer les adhérents :", font=("Helvetica", 12)).grid(row=7, column=0, columnspan=5, pady=10)

        adherent_button = tk.Button(frame, text="Voir les adhérents", command=self.show_adherents, width=button_width)
        adherent_button.grid(row=8, column=0, pady=5, padx=1)
        
        adherent_button = tk.Button(frame, text="Toutes les sanctions", command=self.show_sanctions, width=button_width)
        adherent_button.grid(row=8, column=1, pady=5, padx=1)

        somme_button = tk.Button(frame, text="Somme dûe par adhérent", command=self.somme_argent, width=button_width)
        somme_button.grid(row=8, column=2, pady=5, padx=1)

        retard_button = tk.Button(frame, text="Retards", command=self.retard, width=button_width)
        retard_button.grid(row=8, column=3, pady=5, padx=1)

        degradation_button = tk.Button(frame, text="Dégradations", command=self.degradation, width=button_width)
        degradation_button.grid(row=8, column=4, pady=5, padx=1)

        password_button = tk.Button(frame, text="Nouveau mot de passe", font=("Helvetica", 10, "bold"), fg="blue", command=self.modify_password_personnel, width=button_width)
        password_button.grid(row=10, column=1, columnspan=2, pady=50)
        
        exit_button = tk.Button(frame, text="Déconnexion", font=("Helvetica", 10, "bold"), fg="red", command=self.fin_programe, width=button_width)
        exit_button.grid(row=10, column=2, columnspan=2, pady=50)

    def create_root_menu(self, frame):
        tk.Label(frame, text="Menu root", font=("Helvetica", 14)).grid(row=0, column=0, columnspan=2, pady=10)

        create_table_button = tk.Button(frame, text="Créer une table", command=self.create_table)
        create_table_button.grid(row=2, column=0, pady=5, padx=10)

        delete_table_button = tk.Button(frame, text="Supprimer une table", command=self.delete_table)
        delete_table_button.grid(row=3, column=0, pady=5, padx=10)

        exit_button = tk.Button(frame, text="Quitter", command=self.fin_programe)
        exit_button.grid(row=15, column=0, pady=5, padx=10)
        
    def modify_password(self):
        def update_password(current_password, new_password, modify_password_window):
            check_query = "SELECT mot_de_passe FROM Adherent WHERE login = %s"
            check_result = self.execute_query(check_query, (self.global_username.get(),))

            if check_result and len(check_result) > 0:
                if check_result[0][0] == current_password:
                    update_query = "UPDATE Adherent SET mot_de_passe = %s WHERE login = %s"
                    self.execute_no_return_query(update_query, (new_password, self.global_username.get()))
                    self.connection.commit()
                    messagebox.showinfo("Succès", "Le mot de passe a été mis à jour avec succès.")
                    modify_password_window.destroy() 
                else:
                    messagebox.showerror("Erreur", "Mot de passe actuel incorrect.")
            else:
                messagebox.showerror("Erreur", "Erreur lors de la vérification du mot de passe actuel.")

        modify_password_window = tk.Toplevel(self.root)
        modify_password_window.title("Modifier le mot de passe")

        current_password_label = tk.Label(modify_password_window, text="Mot de passe actuel:")
        current_password_label.grid(row=0, column=0, padx=10, pady=5, sticky=tk.W)

        current_password_entry = tk.Entry(modify_password_window, show="*")
        current_password_entry.grid(row=0, column=1, padx=10, pady=5, sticky=tk.W)

        new_password_label = tk.Label(modify_password_window, text="Nouveau mot de passe:")
        new_password_label.grid(row=1, column=0, padx=10, pady=5, sticky=tk.W)

        new_password_entry = tk.Entry(modify_password_window, show="*")
        new_password_entry.grid(row=1, column=1, padx=10, pady=5, sticky=tk.W)

        validate_button = tk.Button(modify_password_window, text="Valider", command=lambda: update_password(current_password_entry.get(), new_password_entry.get(), modify_password_window))
        validate_button.grid(row=2, column=0, columnspan=2, pady=10)
        
    def modify_password_personnel(self):
        def update_password(current_password, new_password, modify_password_window):
            check_query = "SELECT mot_de_passe FROM Membre_personnel WHERE login = %s"
            check_result = self.execute_query(check_query, (self.global_username.get(),))

            if check_result and len(check_result) > 0:
                if check_result[0][0] == current_password:
                    update_query = "UPDATE Membre_personnel SET mot_de_passe = %s WHERE login = %s"
                    self.execute_no_return_query(update_query, (new_password, self.global_username.get()))
                    self.connection.commit()
                    messagebox.showinfo("Succès", "Le mot de passe a été mis à jour avec succès.")
                    modify_password_window.destroy() 
                else:
                    messagebox.showerror("Erreur", "Mot de passe actuel incorrect.")
            else:
                messagebox.showerror("Erreur", "Erreur lors de la vérification du mot de passe actuel.")

        modify_password_window = tk.Toplevel(self.root)
        modify_password_window.title("Modifier le mot de passe")

        current_password_label = tk.Label(modify_password_window, text="Mot de passe actuel:")
        current_password_label.grid(row=0, column=0, padx=10, pady=5, sticky=tk.W)

        current_password_entry = tk.Entry(modify_password_window, show="*")
        current_password_entry.grid(row=0, column=1, padx=10, pady=5, sticky=tk.W)

        new_password_label = tk.Label(modify_password_window, text="Nouveau mot de passe:")
        new_password_label.grid(row=1, column=0, padx=10, pady=5, sticky=tk.W)

        new_password_entry = tk.Entry(modify_password_window, show="*")
        new_password_entry.grid(row=1, column=1, padx=10, pady=5, sticky=tk.W)

        validate_button = tk.Button(modify_password_window, text="Valider", command=lambda: update_password(current_password_entry.get(), new_password_entry.get(), modify_password_window))
        validate_button.grid(row=2, column=0, columnspan=2, pady=10)
        
    def show_ressources_neuves(self):
            result = self.execute_query("SELECT DISTINCT R.* FROM Ressource AS R JOIN Exemplaire AS E ON R.code = E.code_ressource WHERE E.etat = 'neuf';")
            self.display_all_ressources(result, "Liste des ressources neuves")

    def display_all_ressources(self, result, title):
        popup = tk.Toplevel(self.root)
        popup.title(title)
        
        if result:
            for row in result:
                if row[6]=="film":
                    title_label = tk.Label(popup, text=row[1], font=("Helvetica", 12, "bold"))
                    title_label.grid(padx=10, pady=5, sticky="w")

                    details_label = tk.Label(popup, text=f"Date de sortie : {row[2]}, Production : {row[3]}, Genre : {row[4]}", font=("Helvetica", 10, "italic"))
                    details_label.grid(padx=10, pady=2, sticky="w")

                    description_label = tk.Label(popup, text=row[10])
                    description_label.grid(padx=10, pady=2, sticky="w")

                    duration_label = tk.Label(popup, text=f"Durée : {row[11]}", font=("Helvetica", 10, "italic"))
                    duration_label.grid(padx=10, pady=2, sticky="w")

                    separator = tk.Label(popup, text="-" * 50)
                    separator.grid(padx=10, pady=5, sticky="w")
                    
                elif row[6]=="livre":
                    title_label = tk.Label(popup, text=row[1], font=("Helvetica", 12, "bold"))
                    title_label.grid(padx=10, pady=5, sticky="w")

                    details_label = tk.Label(popup, text=f"{row[2]}, {row[3]}, {row[4]}", font=("Helvetica", 10, "italic"))
                    details_label.grid(padx=10, pady=2, sticky="w")

                    description_label = tk.Label(popup, text=row[8])
                    description_label.grid(padx=10, pady=2, sticky="w")

                    language_label = tk.Label(popup, text=f"Langue : {row[9]}", font=("Helvetica", 10, "italic"))
                    language_label.grid(padx=10, pady=2, sticky="w")

                    separator = tk.Label(popup, text="-" * 50)
                    separator.grid(padx=10, pady=5, sticky="w")
                    
                elif row[6]=="enregistrement_musical":
                    title_label = tk.Label(popup, text=row[1], font=("Helvetica", 12, "bold"))
                    title_label.grid(padx=10, pady=5, sticky="w")

                    details_label = tk.Label(popup, text=f"Date de sortie : {row[2]}, Studio : {row[3]}, Genre : {row[4]}", font=("Helvetica", 10, "italic"))
                    details_label.grid(padx=10, pady=2, sticky="w")

                    duration_label = tk.Label(popup, text=f"Durée : {row[11]}", font=("Helvetica", 10, "italic"))
                    duration_label.grid(padx=10, pady=2, sticky="w")

                    separator = tk.Label(popup, text="-" * 50)
                    separator.grid(padx=10, pady=5, sticky="w")
                else:
                    tk.Label(popup, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)


    def show_all_resources(self):
        result = self.execute_query("SELECT * FROM Ressource")
        self.display_all_ressources(result, "Liste des ressources")

    def show_current_loans(self):
        loans = self.get_current_loans()
            
        popup = tk.Toplevel(self.root)
        popup.title("Prêts en cours")

        canvas = tk.Canvas(popup)
        canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        scrollbar_y = tk.Scrollbar(popup, orient=tk.VERTICAL, command=canvas.yview)
        scrollbar_y.pack(side=tk.RIGHT, fill=tk.Y)
        canvas.configure(yscrollcommand=scrollbar_y.set)

        frame = tk.Frame(canvas)
        canvas.create_window((0, 0), window=frame, anchor=tk.NW)

        if loans:
            for loan in loans:
                
                adherent_label = tk.Label(frame, text=f"Adhérent: {loan[5]} {loan[6]}, login : {loan[0]}", wraplength=300)
                adherent_label.grid(padx=10, pady=5, sticky="w")
                
                ressource_label = tk.Label(frame, text=f"Ressource: {loan[7]}", wraplength=300)
                ressource_label.grid(padx=10, pady=5, sticky="w")
                
                date_label = tk.Label(frame, text=f"Date d'emprunt: {loan[3]}", wraplength=300)
                date_label.grid(padx=10, pady=5, sticky="w")

                btn_return = tk.Button(frame, text="Rendu", command=lambda l=loan: self.handle_return(l))
                btn_return.grid(padx=10, pady=5, sticky="w")

                separator = tk.Label(frame, text="-" * 50)
                separator.grid(padx=10, pady=5, sticky="w")
        else:
            tk.Label(frame, text="Aucun prêt en cours").grid(row=0, column=0, padx=10, pady=5)

        canvas.bind_all("<MouseWheel>", lambda event: canvas.yview_scroll(int(-1 * (event.delta / 120)), "units"))

        frame.update_idletasks()
        canvas.config(scrollregion=canvas.bbox("all"))

    def get_current_loans(self):
        current_loans_query = "SELECT p.adherent, e.code_ressource, p.exemplaire, p.date_pret, p.date_maximale, a.nom, a.prenom, r.titre FROM PRET AS p JOIN Exemplaire AS e ON p.exemplaire = e.id JOIN Adherent AS a ON p.adherent = a.login JOIN Ressource AS r ON e.code_ressource = r.code WHERE date_retour IS NULL"
        current_loans = self.execute_query(current_loans_query)
        return current_loans

    def handle_return(self, loan):
        options = ['bon', 'abime', 'perdu']

        dialog = tk.Toplevel()
        dialog.title("État de rendu")

        state = tk.StringVar()

        state_combobox = Combobox(dialog, textvariable=state, values=options)
        state_combobox.grid(row=0, column=0, padx=10, pady=10)

        state.set(options[0])

        def confirm_selection():
            dialog.destroy()

        confirm_button = tk.Button(dialog, text="Valider", command=confirm_selection)
        confirm_button.grid(row=1, column=0, padx=10, pady=10)

        dialog.wait_window(dialog)

        state = state.get()
        
        if state:
            if state in ["abime", "perdu"]:
                price_to_pay = simpledialog.askfloat("Montant à payer", "Montant à payer:")
                if price_to_pay is not None:
                    self.process_return(loan, state, price_to_pay)
            else:
                self.process_return(loan, state)
                
    def process_return(self, loan, state, price_to_pay=None):
        current_date = datetime.now().date()

        days_delayed = (current_date - loan[4]).days

        if days_delayed > 0:
            self.apply_delayed_sanction(loan, days_delayed)

        self.update_loan_return(loan, state, price_to_pay)

        self.show_current_loans()
        
    def apply_delayed_sanction(self, loan, days_delayed):
        try:
            query = f"INSERT INTO Sanction (pret_date, pret_exemplaire, pret_adherent, type_sanction, nb_jours_retard) VALUES ('{loan[3]}', {loan[2]}, '{loan[0]}', 'retard', {days_delayed});"
            self.execute_no_return_query(query)

        except (Exception, psycopg2.Error) as error:
            print(f"Erreur lors de l'application de la sanction pour retard : {error}")
            
    def update_loan_return(self, loan, state, price_to_pay):
        try:
            self.execute_no_return_query("""
                UPDATE Exemplaire
                SET etat = %s
                WHERE id = %s
            """, (state, loan[2]))
            if state in ('abime', 'perdu'):
                
                if state == 'abime': 
                    state = 'abîmé'

                self.execute_no_return_query(f"INSERT INTO Sanction (pret_date, pret_exemplaire, pret_adherent, type_sanction, prix_a_payer, etat_apres_rendu) VALUES ('{loan[3]}', {loan[2]}, '{loan[0]}', 'dégradation', {price_to_pay}, '{state}');")

            
            self.execute_no_return_query(f"UPDATE PRET SET date_retour = CURRENT_DATE  WHERE date_pret = '{loan[3]}' AND exemplaire = {loan[2]} AND adherent = '{loan[0]}';")

        except (Exception, psycopg2.Error) as error:
            print(f"Erreur lors de la mise à jour du rendu de la ressource : {error}")
    
    def show_books(self):
        
        result = self.execute_query("SELECT * FROM Ressource WHERE type='livre';")
        self.display_books(result, "Liste des livres")

    def display_books(self, result, title, show_delete_button=True):
        
        popup = tk.Toplevel(self.root)
        popup.title(title)

        
        canvas = tk.Canvas(popup)
        canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        
        scrollbar_y = tk.Scrollbar(popup, orient=tk.VERTICAL, command=canvas.yview)
        scrollbar_y.pack(side=tk.RIGHT, fill=tk.Y)
        canvas.configure(yscrollcommand=scrollbar_y.set)

        
        frame = tk.Frame(canvas)
        canvas.create_window((0, 0), window=frame, anchor=tk.NW)
        
        if not self.is_adherent():
            add_book_button = tk.Button(frame, text="Ajouter un livre", command=self.add_book)
            add_book_button.grid(row=0, column=0, padx=10, pady=5, sticky="w")

        if result:
            for row in result:
                title_label = tk.Label(frame, text=row[1], font=("Helvetica", 12, "bold"), wraplength=300)  
                title_label.grid(padx=10, pady=5, sticky="w")

                details_label = tk.Label(frame, text=f"{row[2]}, {row[3]}, {row[4]}", font=("Helvetica", 10, "italic"), wraplength=300)  
                details_label.grid(padx=10, pady=2, sticky="w")

                description_label = tk.Label(frame, text=row[8], wraplength=300)  
                description_label.grid(padx=10, pady=2, sticky="w")
                
                if not self.is_adherent():
                    edit_button = tk.Button(frame, text="Modifier le résumé", command=lambda code=row[0]: self.edit_book(code))
                    edit_button.grid(pady=5, padx=10, sticky="w")

                language_label = tk.Label(frame, text=f"Langue : {row[9]}", font=("Helvetica", 10, "italic"), wraplength=300)  
                language_label.grid(padx=10, pady=2, sticky="w")
                
                exemplar_count_label = tk.Label(frame, text=f"Nombre d'exemplaires : {self.get_exemplar_count(row[0])} dont {self.get_good_condition_exemplar_count(row[0])} en bon état ou neuf")
                exemplar_count_label.grid(padx=10, pady=2, sticky="w")

                if not self.is_adherent():
                    add_exemplar_button = tk.Button(frame, text="Ajouter un exemplaire", command=lambda code=row[0]: self.add_exemplar(code))
                    add_exemplar_button.grid(padx=10, pady=5, sticky="w")
                
                if self.is_adherent():
                    disponibilite = self.verifier_disponibilite_emprunt(row[0])
                    demande_en_attente = self.verifier_demande_en_attente(row[0])
                    print(self.endette())
                    
                    if not self.emprunts_possibles():
                        impossible_label = tk.Label(frame, text="Emprunt impossible : 5 emprunts déjà en cours", fg="red", wraplength=300)
                        impossible_label.grid(padx=10, pady=2, sticky="w")
                    elif self.endette():
                        endette_label = tk.Label(frame, text="Emprunt impossible : vous devez encore de l'argent à la bibliothèque", fg="red", wraplength=300)
                        endette_label.grid(padx=10, pady=2, sticky="w")
                    elif self.est_retard():
                        retard_label = tk.Label(frame, text="Emprunt impossible : vous avez un prêt non rendu en retard", fg="red", wraplength=300)
                        retard_label.grid(padx=10, pady=2, sticky="w")
                    elif demande_en_attente:
                        attente_label = tk.Label(frame, text="Demande d'emprunt envoyée", fg="green", wraplength=300)
                        attente_label.grid(padx=10, pady=2, sticky="w")
                    elif disponibilite:
                        emprunt_button = tk.Button(frame, text="Emprunter", command=lambda: self.effectuer_demande_pret(row[0]))
                        emprunt_button.grid(padx=10, pady=5, sticky="w")
                    else:
                        indisponible_label = tk.Label(frame, text="Emprunt indisponible", fg="orange", wraplength=300)
                        indisponible_label.grid(padx=10, pady=2, sticky="w")

                if not self.is_adherent():
                    delete_button = tk.Button(frame, text="Supprimer", command=lambda code=row[0]: self.delete_book(code))
                    delete_button.grid(pady=5, padx=10, sticky="w")

                separator = tk.Label(frame, text="-" * 50)
                separator.grid(padx=10, pady=5, sticky="w")
        else:
            tk.Label(frame, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)

        
        canvas.bind_all("<MouseWheel>", lambda event: canvas.yview_scroll(int(-1 * (event.delta / 120)), "units"))

        
        frame.update_idletasks()
        canvas.config(scrollregion=canvas.bbox("all"))
        
    def verifier_demande_en_attente(self, code_ressource):
        
        query = "SELECT COUNT(*) FROM DemandePret WHERE adherent_login = %s AND code_ressource = %s AND statut = 'en_attente'"
        result = self.execute_query(query, (self.global_username.get(), code_ressource))
        return result[0][0] > 0
        
    def verifier_disponibilite_emprunt(self, code_ressource):
        
        query = """
            SELECT COUNT(*) 
            FROM Exemplaire e
            LEFT JOIN Pret p ON e.id = p.exemplaire
            WHERE e.code_ressource = %s AND e.etat IN ('bon', 'neuf') AND p.date_retour IS NOT NULL
        """
        result = self.execute_query(query, (code_ressource,))

        
        return result[0][0] > 0
    
    def effectuer_demande_pret(self, code_ressource):
        
        if self.verifier_demande_en_attente(code_ressource):
            messagebox.showinfo("Demande en attente", "Vous avez déjà une demande d'emprunt en attente pour cette ressource.")
            return

        
        query_insert = "INSERT INTO DemandePret (date_demande, adherent_login, code_ressource, statut) VALUES (CURRENT_DATE, %s, %s, 'en_attente')"
        self.execute_no_return_query(query_insert, (self.global_username.get(), code_ressource))
        self.connection.commit()

        
        messagebox.showinfo("Demande d'emprunt envoyée", "Votre demande d'emprunt a été envoyée avec succès.")

    def obtenir_demandes_en_attente(self):
        
        query = """
            SELECT dp.id, dp.code_ressource, dp.adherent_login, a.nom AS adherent_nom, a.prenom AS adherent_prenom,
                   COALESCE(s.prix_a_payer, 0) AS sanctions, COALESCE(a.est_blacklist, FALSE) AS est_blacklist
            FROM DemandePret dp
            JOIN Adherent a ON dp.adherent_login = a.login
            LEFT JOIN Sanction s ON dp.adherent_login = s.pret_adherent AND dp.code_ressource = s.pret_exemplaire
            WHERE dp.statut = 'en_attente'
        """
        result = self.execute_query(query)
        demandes_en_attente = []

        for row in result:
            demande = {
                'id': row[0],
                'code_ressource': row[1],
                'adherent_login': row[2],
                'adherent_nom': row[3],
                'adherent_prenom': row[4],
                'sanctions': row[5],
                'est_blacklist': row[6],
            }
            demandes_en_attente.append(demande)

        return demandes_en_attente

    def display_demandes(self):
        
        popup = tk.Toplevel(self.root)
        popup.title("Gestion des demandes d'emprunts")

        
        canvas = tk.Canvas(popup)
        canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        
        scrollbar_y = tk.Scrollbar(popup, orient=tk.VERTICAL, command=canvas.yview)
        scrollbar_y.pack(side=tk.RIGHT, fill=tk.Y)
        canvas.configure(yscrollcommand=scrollbar_y.set)

        
        frame = tk.Frame(canvas)
        canvas.create_window((0, 0), window=frame, anchor=tk.NW)
        
        demandes_en_attente = self.obtenir_demandes_en_attente()

        if demandes_en_attente != []:
            for demande in demandes_en_attente:
                adherent_label = tk.Label(frame, text=f"Adhérent : {demande['adherent_nom']} {demande['adherent_prenom']}", wraplength=300)  
                adherent_label.grid(padx=10, pady=2, sticky="w")

                sanction_label = tk.Label(frame, text=f"Sanctions : {demande['sanctions']}", wraplength=300)  
                sanction_label.grid(padx=10, pady=2, sticky="w")
                
                blacklist_label = tk.Label(frame, text=f"Blacklisté : {demande['est_blacklist']}", wraplength=300)  
                blacklist_label.grid(padx=10, pady=2, sticky="w")

                accept_button = tk.Button(frame, text="Accepter", command=lambda d=demande: self.accepter_demande(d))
                accept_button.grid(pady=5, padx=10, sticky="w")
                
                accept_button = tk.Button(frame, text="Refuser", command=lambda d=demande: self.refuser_demande(d))
                accept_button.grid(pady=5, padx=10, sticky="w")

                separator = tk.Label(frame, text="-" * 50)
                separator.grid(padx=10, pady=5, sticky="w")
        else:
            tk.Label(frame, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)

        
        canvas.bind_all("<MouseWheel>", lambda event: canvas.yview_scroll(int(-1 * (event.delta / 120)), "units"))

        
        frame.update_idletasks()
        canvas.config(scrollregion=canvas.bbox("all"))
        
    def accepter_demande(self, demande):
        
        id_demande = demande['id']
        adherent_login = demande['adherent_login']
        code_ressource = demande['code_ressource']

        try:
            
            exemplaire_disponible = self.get_premier_exemplaire_disponible(code_ressource)

            if exemplaire_disponible:
                
                date_pret = datetime.now().date()

                
                date_maximale = date_pret + timedelta(days=14)  

                
                self.creer_pret(date_pret, exemplaire_disponible, adherent_login, date_maximale)

                
                self.met_a_jour_statut_demande(id_demande, 'acceptee')

                
                messagebox.showinfo("Demande acceptée", "La demande a été acceptée. Un prêt a été créé.")
            else:
                
                messagebox.showwarning("Aucun exemplaire disponible", "Il n'y a pas d'exemplaire disponible en bon état ou neuf.")

        except Exception as e:
            
            messagebox.showerror("Erreur", f"Erreur lors de l'acceptation de la demande : {e}")

    def get_premier_exemplaire_disponible(self, code_ressource):
        
        query = """
            SELECT id
            FROM Exemplaire
            WHERE code_ressource = %s
            AND etat IN ('neuf', 'bon')
            AND id NOT IN (
                SELECT exemplaire
                FROM PRET
                WHERE exemplaire IS NOT NULL
                AND (date_retour IS NULL OR date_retour > CURRENT_DATE)
            )
            LIMIT 1
        """
        result = self.execute_query(query, (code_ressource,))
        return result[0][0] if result else None

    def creer_pret(self, date_pret, exemplaire_id, adherent_login, date_maximale):
        
        query = f"INSERT INTO PRET (date_pret, exemplaire, adherent, membre_personnel, date_maximale) VALUES (%s, %s, %s, '{self.global_username.get()}', %s)"
        self.execute_no_return_query(query, (date_pret, exemplaire_id, adherent_login, date_maximale))
        self.connection.commit()
        
    def refuser_demande(self, demande):
        
        code_ressource = demande['id']

        
        self.met_a_jour_statut_demande(code_ressource, 'refusee')

        messagebox.showinfo("Demande refusée", "La demande a été refusée.")

    def met_a_jour_statut_demande(self, code_ressource, nouveau_statut):
        
        query = "UPDATE DemandePret SET statut = %s WHERE id = %s"
        self.execute_no_return_query(query, (nouveau_statut, code_ressource))
        self.connection.commit()
        
    def get_exemplar_count(self, book_code):
        
        query = "SELECT COUNT(*) FROM Exemplaire WHERE code_ressource = %s"
        result = self.execute_query(query, (book_code,))
        return result[0][0] if result else 0
    
    def get_good_condition_exemplar_count(self, book_code):
        
        query = "SELECT COUNT(*) FROM Exemplaire WHERE code_ressource = %s AND etat IN ('bon', 'neuf')"
        result = self.execute_query(query, (book_code,))
        return result[0][0] if result else 0

    def add_exemplar(self, book_code):
        
        etat = simpledialog.askstring("Ajouter un exemplaire", "État de l'exemplaire (neuf, bon, abime, perdu):")
        
        if etat is not None and etat in ('neuf', 'bon', 'abime', 'perdu'):
            query = "INSERT INTO Exemplaire (etat, code_ressource) VALUES (%s, %s)"
            values = (etat, book_code)

            try:
                self.execute_no_return_query(query, values)
                self.connection.commit()
                messagebox.showinfo("Succès", "Exemplaire ajouté avec succès.")
                
                self.show_books()
            except Exception as e:
                print(f"Erreur lors de l'ajout de l'exemplaire : {e}")
                self.connection.rollback()
                messagebox.showerror("Erreur", "Erreur lors de l'ajout de l'exemplaire.")
        
    def edit_book(self, book_code):
        existing_description = self.get_existing_description(book_code)

        
        def apply_changes():
            new_description = text_widget.get("1.0", tk.END).strip()

            if new_description is not None:
                
                success = self.update_description(book_code, new_description)

                if success:
                    messagebox.showinfo("Succès", "Description mise à jour avec succès.")
                    self.show_books()
                else:
                    messagebox.showerror("Erreur", "Erreur lors de la mise à jour de la description.")
            
            dialog.destroy()

        
        dialog = tk.Toplevel(self.root)
        dialog.title("Modifier le résumé")

        
        text_widget = tk.Text(dialog, height=10, width=40)
        text_widget.pack()

        
        text_widget.insert(tk.END, existing_description)

        
        apply_button = tk.Button(dialog, text="Appliquer les changements", command=apply_changes)
        apply_button.pack()
                
    def get_existing_description(self, book_code):
        
        query = "SELECT resume FROM Ressource WHERE code = %s"        
        result = self.execute_query(query, (book_code,))

        
        if result:
            return result[0][0]
        else:
            return None

    def update_description(self, book_code, new_description):
        
        query = "UPDATE Ressource SET resume = %s WHERE code = %s"

        try:
            self.execute_no_return_query(query, (new_description, book_code))
            self.connection.commit()
            return True
        except Exception as e:
            print(f"Erreur lors de la mise à jour de la description : {e}")
            self.connection.rollback()
            return False
        
    def delete_book(self, book_code):
        
        delete_relation_query = "DELETE FROM est_auteur WHERE livre = %s;"
        self.execute_no_return_query(delete_relation_query, (book_code,))
        
        delete_exemplaires_query = "DELETE FROM Exemplaire WHERE code_ressource = %s;"
        self.execute_no_return_query(delete_exemplaires_query, (book_code,))
        
        
        delete_query = "DELETE FROM Ressource WHERE code = %s;"
        self.execute_no_return_query(delete_query, (book_code,))

        self.connection.commit()
        messagebox.showinfo("Succès", "Livre supprimé avec succès.")
        self.show_books()

    def add_book(self):
        
        book_title = simpledialog.askstring("Ajouter un livre", "Titre du livre:")
        if not book_title:
            return  

        
        date_apparition = simpledialog.askstring("Ajouter un livre", "Date d'apparition (AAAA-MM-JJ):")
        editeur = simpledialog.askstring("Ajouter un livre", "Éditeur:")
        genre = simpledialog.askstring("Ajouter un livre", "Genre:")
        code_classification = simpledialog.askstring("Ajouter un livre", "Code de classification:")
        isbn = simpledialog.askstring("Ajouter un livre", "ISBN:")
        resume = simpledialog.askstring("Ajouter un livre", "Résumé:")
        langue = simpledialog.askstring("Ajouter un livre", "Langue:")

        
        if None in (date_apparition, editeur, genre, code_classification, isbn, resume, langue):
            return  

        
        existing_authors = self.get_existing_authors()

        
        selected_author = self.choose_or_create_author(existing_authors)

        
        if selected_author is None:
            return  

        
        insert_query = """
        INSERT INTO Ressource (titre, date_apparition, editeur, genre, code_classification, type, ISBN, resume, langue)
        VALUES (%s, %s, %s, %s, %s, 'livre', %s, %s, %s)
        RETURNING code
        """
        values = (book_title, date_apparition, editeur, genre, code_classification, isbn, resume, langue)

        try:
            
            self.execute_no_return_query(insert_query, values)

            
            select_query = "SELECT code FROM Ressource WHERE titre = %s"
            code_result = self.execute_query(select_query, (book_title,), fetchall=False)

            if code_result:
                book_code = code_result[0]
                
                relation_query = "INSERT INTO est_auteur (livre, auteur) VALUES (%s, %s)"
                relation_values = (book_code, selected_author)
                self.execute_no_return_query(relation_query, relation_values)

                self.connection.commit()
                messagebox.showinfo("Succès", "Livre ajouté avec succès à la base de données avec la relation d'auteur.")
            else:
                messagebox.showerror("Erreur", "Erreur lors de la récupération du code du livre.")
        except Exception as e:
            messagebox.showerror("Erreur", f"Erreur lors de l'ajout du livre : {e}")

    def get_existing_authors(self):
        
        query = "SELECT id, nom, prenom FROM Contributeur WHERE type = 'auteur'"
        authors = self.execute_query(query)
        return authors

    def choose_or_create_author(self, existing_authors):
        
        author_dict = {author[0]: f"{author[1]} {author[2]}" for author in existing_authors}

        
        author_dict[None] = "Créer un nouvel auteur"

        
        dialog = tk.Toplevel()
        dialog.title("Choisir un auteur")

        
        author_var = tk.StringVar()

        
        author_combobox = Combobox(dialog, textvariable=author_var, values=list(author_dict.values()))
        author_combobox.grid(row=0, column=0, padx=10, pady=10)

        
        author_var.set(list(author_dict.values())[0])

        def confirm_selection():
            dialog.destroy()

        
        confirm_button = tk.Button(dialog, text="Valider", command=confirm_selection)
        confirm_button.grid(row=1, column=0, padx=10, pady=10)

        
        dialog.wait_window(dialog)

        
        selected_author_name = author_var.get()

        
        selected_author_id = [k for k, v in author_dict.items() if v == selected_author_name][0]

        
        if selected_author_id is None:
            return self.create_new_author()

        return selected_author_id

    def create_new_author(self):
        
        new_author_name = simpledialog.askstring("Nouvel auteur", "Nom de l'auteur:")
        new_author_firstname = simpledialog.askstring("Nouvel auteur", "Prénom de l'auteur:")
        new_birth_date = simpledialog.askstring("Nouvel auteur", "Date de naissance de l'auteur (AAAA-MM-JJ):")
        new_nationality = simpledialog.askstring("Nouvel auteur", "Nationalité de l'auteur:")

        
        if None in (new_author_name, new_author_firstname, new_birth_date, new_nationality):
            return None

        
        query = """
        INSERT INTO Contributeur (nom, prenom, date_naissance, nationalite, type)
        VALUES (%s, %s, %s, %s, 'auteur')
        RETURNING id
        """
        values = (new_author_name, new_author_firstname, new_birth_date, new_nationality)

        try:
            new_author_id = self.execute_no_return_query(query, values)
            self.connection.commit()
            return new_author_id
        except Exception as e:
            messagebox.showerror("Erreur", f"Erreur lors de la création du nouvel auteur : {e}")
            return None

    def show_films(self):
        
        result = self.execute_query("SELECT * FROM Ressource WHERE type='film';")
        self.display_films(result, "Liste des films")

    def display_films(self, result, title):
        
        popup = tk.Toplevel(self.root)
        popup.title(title)

        
        canvas = tk.Canvas(popup)
        canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        
        scrollbar_y = tk.Scrollbar(popup, orient=tk.VERTICAL, command=canvas.yview)
        scrollbar_y.pack(side=tk.RIGHT, fill=tk.Y)
        canvas.configure(yscrollcommand=scrollbar_y.set)
        
        frame = tk.Frame(canvas)
        canvas.create_window((0, 0), window=frame, anchor=tk.NW)
        
        if not self.is_adherent():
            add_film_button = tk.Button(frame, text="Ajouter un film", command=self.add_film)
            add_film_button.grid(row=0, column=0, padx=10, pady=5, sticky="w")

        if result:
            for row in result:
                title_label = tk.Label(frame, text=row[1], font=("Helvetica", 12, "bold"), wraplength=300)  
                title_label.grid(padx=10, pady=5, sticky="w")

                details_label_text = f"Date de sortie : {row[2]}, Production : {row[3]}, Genre : {row[4]}"
                details_label = tk.Label(frame, text=details_label_text, font=("Helvetica", 10, "italic"), wraplength=300)  
                details_label.grid(padx=10, pady=2, sticky="w")

                description_label = tk.Label(frame, text=row[10], wraplength=300)  
                description_label.grid(padx=10, pady=2, sticky="w")
                
                if not self.is_adherent():
                    edit_button = tk.Button(frame, text="Modifier le synopsis", command=lambda code=row[0]: self.edit_film(code))
                    edit_button.grid(pady=5, padx=10, sticky="w")

                duration_label = tk.Label(frame, text=f"Durée : {row[11]}", font=("Helvetica", 10, "italic"), wraplength=300)  
                duration_label.grid(padx=10, pady=2, sticky="w")
                
                exemplar_count_label = tk.Label(frame, text=f"Nombre d'exemplaires : {self.get_exemplar_count(row[0])} dont {self.get_good_condition_exemplar_count(row[0])} en bon état ou neuf")
                exemplar_count_label.grid(padx=10, pady=2, sticky="w")

                if not self.is_adherent():
                    add_exemplar_button = tk.Button(frame, text="Ajouter un exemplaire", command=lambda code=row[0]: self.add_exemplar(code))
                    add_exemplar_button.grid(padx=10, pady=5, sticky="w")
                
                if self.is_adherent():
                    disponibilite = self.verifier_disponibilite_emprunt(row[0])
                    demande_en_attente = self.verifier_demande_en_attente(row[0])
                    
                    if not self.emprunts_possibles():
                        impossible_label = tk.Label(frame, text="Emprunt impossible : 5 emprunts déjà en cours", fg="red", wraplength=300)
                        impossible_label.grid(padx=10, pady=2, sticky="w")
                    elif self.endette():
                        endette_label = tk.Label(frame, text="Emprunt impossible : vous devez encore de l'argent à la bibliothèque", fg="red", wraplength=300)
                        endette_label.grid(padx=10, pady=2, sticky="w")
                    elif self.est_retard():
                        retard_label = tk.Label(frame, text="Emprunt impossible : vous avez un prêt non rendu en retard", fg="red", wraplength=300)
                        retard_label.grid(padx=10, pady=2, sticky="w")
                    elif demande_en_attente:
                        attente_label = tk.Label(frame, text="Demande d'emprunt envoyée", fg="green", wraplength=300)
                        attente_label.grid(padx=10, pady=2, sticky="w")
                    elif disponibilite:
                        emprunt_button = tk.Button(frame, text="Emprunter", command=lambda: self.effectuer_demande_pret(row[0]))
                        emprunt_button.grid(padx=10, pady=5, sticky="w")
                    else:
                        indisponible_label = tk.Label(frame, text="Emprunt indisponible", fg="orange", wraplength=300)
                        indisponible_label.grid(padx=10, pady=2, sticky="w")
                
                if not self.is_adherent():
                    delete_button = tk.Button(frame, text="Supprimer", command=lambda code=row[0]: self.delete_film(code))
                    delete_button.grid(pady=5, padx=10, sticky="w")

                separator = tk.Label(frame, text="-" * 50)
                separator.grid(padx=10, pady=5, sticky="w")
        else:
            tk.Label(frame, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)

        
        canvas.bind_all("<MouseWheel>", lambda event: canvas.yview_scroll(int(-1 * (event.delta / 120)), "units"))

        
        frame.update_idletasks()
        canvas.config(scrollregion=canvas.bbox("all"))
        
    def edit_film(self, film_code):
        existing_synopsis = self.get_existing_synopsis(film_code)

        
        def apply_changes():
            new_synopsis = text_widget.get("1.0", tk.END).strip()

            if new_synopsis is not None:
                
                success = self.update_synopsis(film_code, new_synopsis)

                if success:
                    messagebox.showinfo("Succès", "Synopsis mis à jour avec succès.")
                    self.show_films()
                else:
                    messagebox.showerror("Erreur", "Erreur lors de la mise à jour de la synopsys.")
            
            dialog.destroy()

        
        dialog = tk.Toplevel(self.root)
        dialog.title("Modifier le synopsis")

        
        text_widget = tk.Text(dialog, height=10, width=40)
        text_widget.pack()

        
        text_widget.insert(tk.END, existing_synopsis)

        
        apply_button = tk.Button(dialog, text="Appliquer les changements", command=apply_changes)
        apply_button.pack()
                
    def get_existing_synopsis(self, film_code):
        
        query = "SELECT synopsis FROM Ressource WHERE code = %s"        
        result = self.execute_query(query, (film_code,))

        
        if result:
            return result[0][0]
        else:
            return None

    def update_synopsis(self, film_code, new_description):
        
        query = "UPDATE Ressource SET synopsis = %s WHERE code = %s"

        try:
            self.execute_no_return_query(query, (new_description, film_code))
            self.connection.commit()
            return True
        except Exception as e:
            print(f"Erreur lors de la mise à jour du synopsis : {e}")
            self.connection.rollback()
            return False
        
    def delete_film(self, film_code):
        
        delete_realisateur_query = "DELETE FROM est_realisateur WHERE film = %s;"
        self.execute_no_return_query(delete_realisateur_query, (film_code,))

        delete_acteur_query = "DELETE FROM est_acteur WHERE film = %s;"
        self.execute_no_return_query(delete_acteur_query, (film_code,))
        
        delete_exemplaires_query = "DELETE FROM Exemplaire WHERE code_ressource = %s;"
        self.execute_no_return_query(delete_exemplaires_query, (film_code,))

        
        delete_query = "DELETE FROM Ressource WHERE code = %s;"
        self.execute_no_return_query(delete_query, (film_code,))

        self.connection.commit()
        messagebox.showinfo("Succès", "Film supprimé avec succès.")
        self.show_films()
        
    def add_film(self):
        
        film_title = simpledialog.askstring("Ajouter un film", "Titre du film:")
        if not film_title:
            return  

        
        date_apparition = simpledialog.askstring("Ajouter un film", "Date d'apparition (AAAA-MM-JJ):")
        editeur = simpledialog.askstring("Ajouter un film", "Éditeur:")
        genre = simpledialog.askstring("Ajouter un film", "Genre:")
        code_classification = simpledialog.askstring("Ajouter un film", "Code de classification:")
        synopsis = simpledialog.askstring("Ajouter un film", "Synopsis:")
        language = simpledialog.askstring("Ajouter un film", "Langue:")
        duration = simpledialog.askstring("Ajouter un film", "Durée (HH:MM:SS):")

        
        if None in (date_apparition, editeur, genre, code_classification, synopsis, duration):
            return  

        
        existing_directors = self.get_existing_directors()

        
        selected_directors = self.choose_or_create_director(existing_directors)

        
        if selected_directors is None:
            return  

        
        existing_actors = self.get_existing_actors()

        
        selected_actors = self.choose_or_create_actors(existing_actors)

        
        if selected_actors is None:
            return  

        
        insert_query = """
        INSERT INTO Ressource (titre, date_apparition, editeur, genre, code_classification, type, synopsis, langue, longueur)
        VALUES (%s, %s, %s, %s, %s, 'film', %s, %s, %s)
        RETURNING code
        """
        values = (film_title, date_apparition, editeur, genre, code_classification, synopsis, language, duration)

        try:
            
            
            code_result = self.execute_query(insert_query, values, fetchall=False)

            if code_result:
                film_code = code_result[0]

                
                for selected_director in selected_directors:
                    relation_query_director = "INSERT INTO est_realisateur (film, realisateur) VALUES (%s, %s)"
                    relation_values_director = (film_code, selected_director)
                    self.execute_no_return_query(relation_query_director, relation_values_director)
                self.connection.commit()

                
                for selected_actor in selected_actors:
                    relation_query_actor = "INSERT INTO est_acteur (film, acteur) VALUES (%s, %s)"
                    relation_values_actor = (film_code, selected_actor)
                    self.execute_no_return_query(relation_query_actor, relation_values_actor)

                self.connection.commit()
                self.display_actors()
                messagebox.showinfo("Succès", "Film ajouté avec succès à la base de données avec les relations.")
            else:
                messagebox.showerror("Erreur", "Erreur lors de la récupération du code du film.")
        except Exception as e:
            messagebox.showerror("Erreur", f"Erreur lors de l'ajout du film : {e}")

    def get_existing_directors(self):
        
        query = "SELECT id, nom, prenom FROM Contributeur WHERE type = 'realisateur'"
        directors = self.execute_query(query)
        return directors

    def choose_or_create_director(self, existing_directors):
        
        director_dict = {director[0]: f"{director[1]} {director[2]}" for director in existing_directors}

        
        director_dict[None] = "Ajouter"

        
        director_list_dialog = tk.Toplevel(self.root)
        director_list_dialog.title("Liste des réalisateurs existants")

        
        director_list_text = tk.Text(director_list_dialog, height=10, width=40)
        director_list_text.pack(padx=10, pady=10)

        
        for director_name in director_dict.values():
            director_list_text.insert(tk.END, f"{director_name}\n")

        
        selected_directors = simpledialog.askstring("Choisir des réalisateurs", "Choisissez des réalisateurs (séparés par des virgules):", 
                                                parent=self.root)

        
        if selected_directors is None:
            director_list_dialog.destroy()
            return None

        
        director_list_dialog.destroy()
        
        selected_directors_ids = [k for k, v in director_dict.items() if v in selected_directors]
        
        
        if None in director_dict and director_dict[None] in selected_directors:
            selected_directors_ids.pop()
            new_directors = self.create_new_directors()
            for director in new_directors:
                selected_directors_ids.append(director)

        return selected_directors_ids


    def create_new_directors(self):
        
        new_directors_names = simpledialog.askstring("Nouveaux réalisateurs", "Noms des nouveaux réalisateurs (séparés par des virgules):")
        
        new_directors_names_list = [name.strip() for name in new_directors_names.split(',')]
        new_directors_ids = []

        
        for director_name in new_directors_names_list:
            director_info = director_name.split(' ')
            if len(director_info) == 2:
                
                new_director_name, new_director_firstname = director_info
                new_birth_date = simpledialog.askstring("Nouveaux réalisateurs", f"Date de naissance de {new_director_name} {new_director_firstname} (AAAA-MM-JJ):")
                new_nationality = simpledialog.askstring("Nouveaux réalisateurs", f"Nationalité de {new_director_name} {new_director_firstname}:")

                
                if None in (new_birth_date, new_nationality):
                    continue

                
                query = """
                INSERT INTO Contributeur (nom, prenom, date_naissance, nationalite, type)
                VALUES (%s, %s, %s, %s, 'realisateur')
                RETURNING id
                """
                values = (new_director_name, new_director_firstname, new_birth_date, new_nationality)

                try:
                    new_director_id = self.execute_query(query, values)
                    new_directors_ids.append(new_director_id[0][0])
                except Exception as e:
                    messagebox.showerror("Erreur", f"Erreur lors de la création du nouvel réalisateur {director_name}: {e}")

        self.connection.commit()
        return new_directors_ids

    def get_existing_actors(self):
        
        query = "SELECT id, nom, prenom FROM Contributeur WHERE type = 'acteur'"
        actors = self.execute_query(query)
        return actors
    
    def display_actors(self):
        query = "SELECT * FROM est_realisateur;"
        actors = self.execute_query(query)
        self.display_result(actors, "Liste acteur-film")

    def choose_or_create_actors(self, existing_actors):
        
        actor_dict = {actor[0]: f"{actor[1]} {actor[2]}" for actor in existing_actors}

        
        actor_dict[None] = "Ajouter"

        
        actor_list_dialog = tk.Toplevel(self.root)
        actor_list_dialog.title("Liste des acteurs existants")

        
        actor_list_text = tk.Text(actor_list_dialog, height=10, width=40)
        actor_list_text.pack(padx=10, pady=10)

        
        for actor_name in actor_dict.values():
            actor_list_text.insert(tk.END, f"{actor_name}\n")

        
        selected_actors = simpledialog.askstring("Choisir des acteurs", "Choisissez des acteurs (séparés par des virgules):", 
                                                parent=self.root)

        
        if selected_actors is None:
            actor_list_dialog.destroy()
            return None

        
        actor_list_dialog.destroy()
        
        selected_actors_ids = [k for k, v in actor_dict.items() if v in selected_actors]
        
        
        if None in actor_dict and actor_dict[None] in selected_actors:
            selected_actors_ids.pop()
            new_actors = self.create_new_actors()
            for actor in new_actors:
                selected_actors_ids.append(actor)

        return selected_actors_ids


    def create_new_actors(self):
        
        new_actors_names = simpledialog.askstring("Nouveaux acteurs", "Noms des nouveaux acteurs (séparés par des virgules):")
        
        new_actors_names_list = [name.strip() for name in new_actors_names.split(',')]
        new_actors_ids = []

        
        for actor_name in new_actors_names_list:
            actor_info = actor_name.split(' ')
            if len(actor_info) == 2:
                
                new_actor_name, new_actor_firstname = actor_info
                new_birth_date = simpledialog.askstring("Nouveaux acteurs", f"Date de naissance de {new_actor_name} {new_actor_firstname} (AAAA-MM-JJ):")
                new_nationality = simpledialog.askstring("Nouveaux acteurs", f"Nationalité de {new_actor_name} {new_actor_firstname}:")

                
                if None in (new_birth_date, new_nationality):
                    continue

                
                query = """
                INSERT INTO Contributeur (nom, prenom, date_naissance, nationalite, type)
                VALUES (%s, %s, %s, %s, 'acteur')
                RETURNING id
                """
                values = (new_actor_name, new_actor_firstname, new_birth_date, new_nationality)

                try:
                    new_actor_id = self.execute_query(query, values)
                    new_actors_ids.append(new_actor_id[0][0])
                except Exception as e:
                    messagebox.showerror("Erreur", f"Erreur lors de la création du nouvel acteur {actor_name}: {e}")

        self.connection.commit()
        return new_actors_ids

    def show_music(self):
        
        result = self.execute_query("SELECT * FROM Ressource WHERE type='enregistrement_musical';")
        self.display_music(result, "Liste des enregistrements musicaux")

    def display_music(self, result, title):
        
        popup = tk.Toplevel(self.root)
        popup.title(title)

        
        canvas = tk.Canvas(popup)
        canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        
        scrollbar_y = tk.Scrollbar(popup, orient=tk.VERTICAL, command=canvas.yview)
        scrollbar_y.pack(side=tk.RIGHT, fill=tk.Y)
        canvas.configure(yscrollcommand=scrollbar_y.set)

        
        frame = tk.Frame(canvas)
        canvas.create_window((0, 0), window=frame, anchor=tk.NW)
        
        if not self.is_adherent():
            add_music_button = tk.Button(frame, text="Ajouter une musique", command=self.add_music)
            add_music_button.grid(row=0, column=0, padx=10, pady=5, sticky="w")

        if result:
            for row in result:
                title_label = tk.Label(frame, text=row[1], font=("Helvetica", 12, "bold"), wraplength=300)  
                title_label.grid(padx=10, pady=5, sticky="w")

                details_label_text = f"Date de sortie : {row[2]}, Studio : {row[3]}, Genre : {row[4]}"
                details_label = tk.Label(frame, text=details_label_text, font=("Helvetica", 10, "italic"), wraplength=300)  
                details_label.grid(padx=10, pady=2, sticky="w")

                duration_label = tk.Label(frame, text=f"Durée : {row[11]}", font=("Helvetica", 10, "italic"), wraplength=300)  
                duration_label.grid(padx=10, pady=2, sticky="w")
                
                exemplar_count_label = tk.Label(frame, text=f"Nombre d'exemplaires : {self.get_exemplar_count(row[0])} dont {self.get_good_condition_exemplar_count(row[0])} en bon état ou neuf")
                exemplar_count_label.grid(padx=10, pady=2, sticky="w")

                if not self.is_adherent():
                    add_exemplar_button = tk.Button(frame, text="Ajouter un exemplaire", command=lambda code=row[0]: self.add_exemplar(code))
                    add_exemplar_button.grid(padx=10, pady=5, sticky="w")
                
                if self.is_adherent():
                    disponibilite = self.verifier_disponibilite_emprunt(row[0])
                    demande_en_attente = self.verifier_demande_en_attente(row[0])
                    
                    if not self.emprunts_possibles():
                        impossible_label = tk.Label(frame, text="Emprunt impossible : 5 emprunts déjà en cours", fg="red", wraplength=300)
                        impossible_label.grid(padx=10, pady=2, sticky="w")
                    elif self.endette():
                        endette_label = tk.Label(frame, text="Emprunt impossible : vous devez encore de l'argent à la bibliothèque", fg="red", wraplength=300)
                        endette_label.grid(padx=10, pady=2, sticky="w")
                    elif self.est_retard():
                        retard_label = tk.Label(frame, text="Emprunt impossible : vous avez un prêt non rendu en retard", fg="red", wraplength=300)
                        retard_label.grid(padx=10, pady=2, sticky="w")
                    elif demande_en_attente:
                        attente_label = tk.Label(frame, text="Demande d'emprunt envoyée", fg="green", wraplength=300)
                        attente_label.grid(padx=10, pady=2, sticky="w")
                    elif disponibilite:
                        emprunt_button = tk.Button(frame, text="Emprunter", command=lambda: self.effectuer_demande_pret(row[0]))
                        emprunt_button.grid(padx=10, pady=5, sticky="w")
                    else:
                        indisponible_label = tk.Label(frame, text="Emprunt indisponible", fg="orange", wraplength=300)
                        indisponible_label.grid(padx=10, pady=2, sticky="w")
                
                if not self.is_adherent():
                    delete_button = tk.Button(frame, text="Supprimer", command=lambda code=row[0]: self.delete_music(code))
                    delete_button.grid(pady=5, padx=10, sticky="w")

                separator = tk.Label(frame, text="-" * 50)
                separator.grid(padx=10, pady=5, sticky="w")
        else:
            tk.Label(frame, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)

        
        canvas.bind_all("<MouseWheel>", lambda event: canvas.yview_scroll(int(-1 * (event.delta / 120)), "units"))

        
        frame.update_idletasks()
        canvas.config(scrollregion=canvas.bbox("all"))
        
    def delete_music(self, music_code):
        
        delete_compositeur_query = "DELETE FROM est_compositeur WHERE enregistrement_musical = %s;"
        self.execute_no_return_query(delete_compositeur_query, (music_code,))

        delete_interprete_query = "DELETE FROM est_interprete WHERE enregistrement_musical = %s;"
        self.execute_no_return_query(delete_interprete_query, (music_code,))
        
        delete_exemplaires_query = "DELETE FROM Exemplaire WHERE code_ressource = %s;"
        self.execute_no_return_query(delete_exemplaires_query, (music_code,))

        
        delete_query = "DELETE FROM Ressource WHERE code = %s;"
        self.execute_no_return_query(delete_query, (music_code,))

        self.connection.commit()
        messagebox.showinfo("Succès", "Musique supprimée avec succès.")
        self.show_music()
        
    def add_music(self):
        
        music_title = simpledialog.askstring("Ajouter une musique", "Titre de la musique:")
        if not music_title:
            return  

        
        date_apparition = simpledialog.askstring("Ajouter une musique", "Date d'apparition (AAAA-MM-JJ):")
        editeur = simpledialog.askstring("Ajouter une musique", "Éditeur:")
        genre = simpledialog.askstring("Ajouter une musique", "Genre:")
        code_classification = simpledialog.askstring("Ajouter une musique", "Code de classification:")
        duration = simpledialog.askstring("Ajouter une musique", "Durée (HH:MM:SS):")

        
        if None in (date_apparition, editeur, genre, code_classification, duration):
            return  

        
        existing_compositeurs = self.get_existing_compositeurs()

        
        selected_compositeurs = self.choose_or_create_compositeur(existing_compositeurs)

        
        if selected_compositeurs is None:
            return  

        
        existing_interpretes = self.get_existing_interpretes()

        
        selected_interpretes = self.choose_or_create_interprete(existing_interpretes)

        
        if selected_interpretes is None:
            return  

        
        insert_query = """
        INSERT INTO Ressource (titre, date_apparition, editeur, genre, code_classification, type, longueur)
        VALUES (%s, %s, %s, %s, %s, 'enregistrement_musical', %s)
        RETURNING code
        """
        values = (music_title, date_apparition, editeur, genre, code_classification, duration)

        try:
            
            
            code_result = self.execute_query(insert_query, values, fetchall=False)

            if code_result:
                music_code = code_result[0]

                
                for selected_compositeur in selected_compositeurs:
                    relation_query_compositeur = "INSERT INTO est_compositeur (enregistrement_musical, compositeur) VALUES (%s, %s)"
                    relation_values_compositeur = (music_code, selected_compositeur)
                    self.execute_no_return_query(relation_query_compositeur, relation_values_compositeur)
                self.connection.commit()

                
                for selected_interprete in selected_interpretes:
                    relation_query_interprete = "INSERT INTO est_interprete (enregistrement_musical, interprete) VALUES (%s, %s)"
                    relation_values_interprete = (music_code, selected_interprete)
                    self.execute_no_return_query(relation_query_interprete, relation_values_interprete)

                self.connection.commit()
                messagebox.showinfo("Succès", "Musique ajouté avec succès à la base de données avec les relations.")
            else:
                messagebox.showerror("Erreur", "Erreur lors de la récupération du code de la musique.")
        except Exception as e:
            messagebox.showerror("Erreur", f"Erreur lors de l'ajout de la musique : {e}")

    def get_existing_compositeurs(self):
        
        query = "SELECT id, nom, prenom FROM Contributeur WHERE type = 'compositeur'"
        compositeurs = self.execute_query(query)
        return compositeurs

    def choose_or_create_compositeur(self, existing_compositeurs):
        
        compositeur_dict = {compositeur[0]: f"{compositeur[1]} {compositeur[2]}" for compositeur in existing_compositeurs}

        
        compositeur_dict[None] = "Ajouter"

        
        compositeur_list_dialog = tk.Toplevel(self.root)
        compositeur_list_dialog.title("Liste des compositeurs existants")

        
        compositeur_list_text = tk.Text(compositeur_list_dialog, height=10, width=40)
        compositeur_list_text.pack(padx=10, pady=10)

        
        for compositeur_name in compositeur_dict.values():
            compositeur_list_text.insert(tk.END, f"{compositeur_name}\n")

        
        selected_compositeurs = simpledialog.askstring("Choisir des compositeurs", "Choisissez des compositeurs (séparés par des virgules):", 
                                                parent=self.root)

        
        if selected_compositeurs is None:
            compositeur_list_dialog.destroy()
            return None

        
        compositeur_list_dialog.destroy()
        
        selected_compositeurs_ids = [k for k, v in compositeur_dict.items() if v in selected_compositeurs]
        
        
        if None in compositeur_dict and compositeur_dict[None] in selected_compositeurs:
            selected_compositeurs_ids.pop()
            new_compositeurs = self.create_new_compositeurs()
            for compositeur in new_compositeurs:
                selected_compositeurs_ids.append(compositeur)

        return selected_compositeurs_ids


    def create_new_compositeurs(self):
        
        new_compositeurs_names = simpledialog.askstring("Nouveaux compositeurs", "Noms des nouveaux compositeurs (séparés par des virgules):")
        
        new_compositeurs_names_list = [name.strip() for name in new_compositeurs_names.split(',')]
        new_compositeurs_ids = []

        
        for compositeur_name in new_compositeurs_names_list:
            compositeur_info = compositeur_name.split(' ')
            if len(compositeur_info) == 2:
                
                new_compositeur_name, new_compositeur_firstname = compositeur_info
                new_birth_date = simpledialog.askstring("Nouveaux compositeurs", f"Date de naissance de {new_compositeur_name} {new_compositeur_firstname} (AAAA-MM-JJ):")
                new_nationality = simpledialog.askstring("Nouveaux compositeurs", f"Nationalité de {new_compositeur_name} {new_compositeur_firstname}:")

                
                if None in (new_birth_date, new_nationality):
                    continue

                
                query = """
                INSERT INTO Contributeur (nom, prenom, date_naissance, nationalite, type)
                VALUES (%s, %s, %s, %s, 'compositeur')
                RETURNING id
                """
                values = (new_compositeur_name, new_compositeur_firstname, new_birth_date, new_nationality)

                try:
                    new_compositeur_id = self.execute_query(query, values)
                    new_compositeurs_ids.append(new_compositeur_id[0][0])
                except Exception as e:
                    messagebox.showerror("Erreur", f"Erreur lors de la création du nouvel compositeur {compositeur_name}: {e}")

        self.connection.commit()
        return new_compositeurs_ids

    def get_existing_interpretes(self):
        
        query = "SELECT id, nom, prenom FROM Contributeur WHERE type = 'interprete'"
        interpretes = self.execute_query(query)
        return interpretes

    def choose_or_create_interprete(self, existing_interpretes):
        
        interprete_dict = {interprete[0]: f"{interprete[1]} {interprete[2]}" for interprete in existing_interpretes}

        
        interprete_dict[None] = "Ajouter"

        
        interprete_list_dialog = tk.Toplevel(self.root)
        interprete_list_dialog.title("Liste des interpretes existants")

        
        interprete_list_text = tk.Text(interprete_list_dialog, height=10, width=40)
        interprete_list_text.pack(padx=10, pady=10)

        
        for interprete_name in interprete_dict.values():
            interprete_list_text.insert(tk.END, f"{interprete_name}\n")

        
        selected_interpretes = simpledialog.askstring("Choisir des interpretes", "Choisissez des interpretes (séparés par des virgules):", 
                                                parent=self.root)

        
        if selected_interpretes is None:
            interprete_list_dialog.destroy()
            return None

        
        interprete_list_dialog.destroy()
        
        selected_interpretes_ids = [k for k, v in interprete_dict.items() if v in selected_interpretes]
        
        
        if None in interprete_dict and interprete_dict[None] in selected_interpretes:
            selected_interpretes_ids.pop()
            new_interpretes = self.create_new_interpretes()
            for interprete in new_interpretes:
                selected_interpretes_ids.append(interprete)

        return selected_interpretes_ids


    def create_new_interpretes(self):
        
        new_interpretes_names = simpledialog.askstring("Nouveaux interpretes", "Noms des nouveaux interpretes (séparés par des virgules):")
        
        new_interpretes_names_list = [name.strip() for name in new_interpretes_names.split(',')]
        new_interpretes_ids = []

        
        for interprete_name in new_interpretes_names_list:
            interprete_info = interprete_name.split(' ')
            if len(interprete_info) == 2:
                
                new_interprete_name, new_interprete_firstname = interprete_info
                new_birth_date = simpledialog.askstring("Nouveaux interpretes", f"Date de naissance de {new_interprete_name} {new_interprete_firstname} (AAAA-MM-JJ):")
                new_nationality = simpledialog.askstring("Nouveaux interpretes", f"Nationalité de {new_interprete_name} {new_interprete_firstname}:")

                
                if None in (new_birth_date, new_nationality):
                    continue

                
                query = """
                INSERT INTO Contributeur (nom, prenom, date_naissance, nationalite, type)
                VALUES (%s, %s, %s, %s, 'interprete')
                RETURNING id
                """
                values = (new_interprete_name, new_interprete_firstname, new_birth_date, new_nationality)

                try:
                    new_interprete_id = self.execute_query(query, values)
                    new_interpretes_ids.append(new_interprete_id[0][0])
                except Exception as e:
                    messagebox.showerror("Erreur", f"Erreur lors de la création du nouvel interprete {interprete_name}: {e}")

        self.connection.commit()
        return new_interpretes_ids
    
    def show_top_ressources(self):
        result = self.execute_query("SELECT R.titre, R.type, COUNT(Pret.exemplaire) AS fois_empruntee FROM Ressource AS R INNER JOIN Exemplaire ON Exemplaire.code_ressource = R.code INNER JOIN Pret ON Pret.exemplaire = Exemplaire.id GROUP BY R.titre, R.type ORDER BY fois_empruntee DESC LIMIT 10;")
        self.display_top_ressources(result, "Top des ressources les plus empruntées")


    def display_top_ressources(self, result, title):
        popup = tk.Toplevel(self.root)
        popup.title(title)
            
        if result:
            for row in result:
                title_label = tk.Label(popup, text=row[0], font=("Helvetica", 12, "bold"))
                title_label.grid(padx=10, pady=5, sticky="w")
                type_label = tk.Label(popup, text=f"Type de ressource : {row[1]}")
                type_label.grid(padx=10, pady=5, sticky="w")
                    
        else:
            tk.Label(popup, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)

    def show_meilleurs_contributeurs(self):
        result = self.execute_query("SELECT C.nom, C.prenom, C.type, COUNT(Exemplaire.id) AS nombre_de_ressources FROM Contributeur AS C LEFT JOIN Ressource ON C.id = Ressource.code LEFT JOIN Exemplaire ON Ressource.code = Exemplaire.code_ressource GROUP BY C.nom, C.prenom, C.type ORDER BY nombre_de_ressources DESC LIMIT 10;")
        self.display_meilleurs_contributeur(result, "Liste des meilleurs contributeurs")
        
    def display_meilleurs_contributeur(self, result, title):
        popup = tk.Toplevel(self.root)
        popup.title(title)
        
        if result:
            for row in result:
                title_label = tk.Label(popup, text=f"{row[0]} {row[1]}", font=("Helvetica", 12, "bold"))
                title_label.grid(padx=10, pady=5, sticky="w")
                details_label = tk.Label(popup, text=f"{row[2]} a participé à {row[3]} ressource(s)")
                details_label.grid(padx=10, pady=2, sticky="w")
        else:
            tk.Label(popup, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)
 
 
    def show_historique_emprunt(self):
        result = self.execute_query(f"SELECT P.date_pret, P.date_retour, R.titre, E.etat FROM Pret AS P JOIN Exemplaire AS E ON P.exemplaire = E.id JOIN RESSOURCE AS R ON E.code_ressource = R.code WHERE P.adherent = '{self.global_username.get()}';") 
        self.display_historique_emprunt(result, "Historique emprunt")
        
    def display_historique_emprunt(self, result, title):
        popup = tk.Toplevel(self.root)
        popup.title(title)
        if result:
            for row in result:
                title_label = tk.Label(popup, text=row[2], font=("Helvetica", 12, "bold"))
                title_label.grid(padx=10, pady=5, sticky="w")
                details_label = tk.Label(popup, text=f"Date du prêt {row[0]}, date retour du prêt {row[1]}  état de la ressource : {row[3]}")
                details_label.grid(padx=10, pady=2, sticky="w")
        else:
            tk.Label(popup, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)
            
    def show_historique(self):
        result = self.execute_query(f"SELECT P.date_pret, P.date_retour, R.titre, E.etat, A.login FROM Pret AS P JOIN Exemplaire AS E ON P.exemplaire = E.id JOIN RESSOURCE AS R ON E.code_ressource = R.code JOIN Adherent AS A ON P.adherent = A.login;") 
        self.display_historique(result, "Historique des prêts")
        
    def display_historique(self, result, title):
        popup = tk.Toplevel(self.root)
        popup.title(title)
        if result:
            for row in result:
                title_label = tk.Label(popup, text=row[2], font=("Helvetica", 12, "bold"))
                title_label.grid(padx=10, pady=5, sticky="w")
                details_label = tk.Label(popup, text=f"Date du prêt {row[0]}, Date de retour du prêt {row[1]}, Adhérent (login) : {row[4]}, [État de la ressource : {row[3]}")
                details_label.grid(padx=10, pady=2, sticky="w")
        else:
            tk.Label(popup, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)
            
            
    def show_emprunt_cours(self):
        result = self.execute_query(f"SELECT P.date_pret, P.date_maximale, R.titre, E.etat FROM Pret AS P JOIN Exemplaire AS E ON P.exemplaire = E.id JOIN RESSOURCE AS R ON E.code_ressource = R.code WHERE P.adherent = '{self.global_username.get()}' AND P.date_retour IS NULL;")
        self.display_emprunt_cours(result, "Emprunt en cours")
        
    def display_emprunt_cours(self, result, title):
        popup = tk.Toplevel(self.root)
        popup.title(title)
        if result:
            for row in result:
                title_label = tk.Label(popup, text=row[2], font=("Helvetica", 12, "bold"))
                title_label.grid(padx=10, pady=5, sticky="w")
                details_label = tk.Label(popup, text=f"Date du prêt {row[0]}, Date maximale de retour du prêt {row[1]}, État initial de la ressource : {row[3]}")
                details_label.grid(padx=10, pady=2, sticky="w")
        else:
            tk.Label(popup, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)
            
    
    def show_sanction_adherent(self):
        result = self.execute_query(f"SELECT * FROM Sanction WHERE pret_adherent = '{self.global_username.get()}';")
        self.display_emprunt_cours(result, "Liste de mes sanctions")
        
    def show_sanctions(self):
        result = self.execute_query(f"SELECT s.pret_date, s.type_sanction, a.login, r.titre, a.nom, a.prenom FROM Sanction AS s JOIN Adherent AS a ON s.pret_adherent = a.login JOIN Exemplaire AS e ON s.pret_exemplaire = e.id JOIN Ressource AS r ON e.code_ressource = r.code;")
        self.display_sanction(result, "Liste des sanctions")
        
    def display_sanction(self, result, title):
        popup = tk.Toplevel(self.root)
        popup.title(title)
        if result:
            for row in result:
                title_label = tk.Label(popup, text=f"{row[4]} {row[5]}", font=("Helvetica", 12, "bold"))
                title_label.grid(padx=10, pady=5, sticky="w")
                details_label = tk.Label(popup, text=f"Date du prêt : {row[0]}, Sanction : {row[1]}, Adhérent (login) : {row[2]}, Ressource : {row[3]}")
                details_label.grid(padx=10, pady=2, sticky="w")
        else:
            tk.Label(popup, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)
    
    def somme_argent(self):
        result= self.execute_query(" SELECT SUM(S.prix_a_payer), P.adherent, A.nom, A.prenom FROM Sanction AS S INNER JOIN Pret AS P ON S.pret_date = P.date_pret AND S.pret_exemplaire = P.exemplaire AND S.pret_adherent = P.adherent INNER JOIN Adherent AS A ON P.adherent = A.login GROUP BY P.adherent, A.nom, A.prenom;")
        self.display_somme(result, "Somme dûe par adherent")

    def retard(self):
        result= self.execute_query(" SELECT A.nom, A.prenom, P.date_pret, S.nb_jours_retard, A.login FROM Pret AS P INNER JOIN Sanction AS S ON P.date_pret = S.pret_date AND P.exemplaire = S.pret_exemplaire AND P.adherent = S.pret_adherent INNER JOIN Adherent AS A ON P.adherent = A.login WHERE S.type_sanction = 'retard';")
        self.display_retard(result, "Liste des retards")
        
    def degradation(self):
        result= self.execute_query("SELECT A.nom, A.prenom, P.date_pret, S.etat_apres_rendu, A.login, S.prix_a_payer, S.id FROM Pret AS P INNER JOIN Sanction AS S ON P.date_pret = S.pret_date AND P.exemplaire = S.pret_exemplaire AND P.adherent = S.pret_adherent INNER JOIN Adherent AS A ON P.adherent = A.login WHERE S.type_sanction = 'dégradation';" )
        self.display_degradation(result, "Liste de toutes les dégradations")
        
    def display_retard(self, result, title):
        popup = tk.Toplevel(self.root)
        popup.title(title)
        if result:
            for row in result:
                title_label = tk.Label(popup, text=f"{row[0]} {row[1]}", font=("Helvetica", 12, "bold"))
                title_label.grid(padx=10, pady=5, sticky="w")
                details_label = tk.Label(popup, text=f"Date du prêt : {row[2]}, Adhérent (login) : {row[4]}, Jours de retard : {row[3]}")
                details_label.grid(padx=10, pady=2, sticky="w")
        else:
            tk.Label(popup, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)

    def display_degradation(self, result, title):
        popup = tk.Toplevel(self.root)
        popup.title(title)
        if result:
            for row in result:
                title_label = tk.Label(popup, text=f"{row[0]} {row[1]}", font=("Helvetica", 12, "bold"))
                title_label.grid(padx=10, pady=5, sticky="w")
                details_label = tk.Label(popup, text=f"Date du prêt : {row[2]}, Adhérent (login) : {row[4]}, État après rendu : {row[3]}, Reste à payer : {row[5]}")
                details_label.grid(padx=10, pady=2, sticky="w")
                if row[5] != 0:
                    paiement_button = tk.Button(popup, text="Payé", command=lambda code=row[0]: self.payer(row[6]))
                    paiement_button.grid(padx=10, pady=5, sticky="w")
        else:
            tk.Label(popup, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)
    
    def payer(self, code):
        try:
            self.execute_no_return_query(f"UPDATE Sanction SET prix_a_payer = 0 WHERE id = {code}")
            self.degradation()
            tk.messagebox.showinfo("Succès", "Le paiement a bien été enregistré")

        except Exception as e:
            tk.messagebox.showerror("Erreur", f"Erreur lors du paiement : {e}")

    def display_somme(self, result, title):
        popup = tk.Toplevel(self.root)
        popup.title(title)
        if result:
            for row in result:
                title_label = tk.Label(popup, text=f"{row[2]} {row[3]}", font=("Helvetica", 12, "bold"))
                title_label.grid(padx=10, pady=5, sticky="w")
                details_label = tk.Label(popup, text=f"Adhérent (login) : {row[1]}, Somme dûe : {row[0]}")
                details_label.grid(padx=10, pady=2, sticky="w")
        else:
            tk.Label(popup, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)
    
    def fin_programe(self):
        for widget in self.root.winfo_children():
            widget.destroy()
        self.create_login_screen()


    def show_adherents(self):
        result = self.execute_query("SELECT * FROM Adherent;")
        self.display_adherents(result, "Liste des adhérents")
        
    def display_adherents(self, result, title):
        self.popup = tk.Toplevel(self.root)
        self.popup.title(title)

        
        filter_frame = tk.Frame(self.popup)
        filter_frame.pack(side=tk.TOP, fill=tk.X)

        all_button = tk.Button(filter_frame, text="Tous", command=lambda: self.display_filtered_adherents(result, "Adhérents"))
        all_button.pack(side=tk.LEFT)

        active_button = tk.Button(filter_frame, text="Actifs", command=lambda: self.display_filtered_adherents(self.filter_adherents(result, 1), "Adhérents actifs"))
        active_button.pack(side=tk.LEFT)

        inactive_button = tk.Button(filter_frame, text="Inactifs", command=lambda: self.display_filtered_adherents(self.filter_adherents(result, 0), "Adhérents inactifs"))
        inactive_button.pack(side=tk.LEFT)

        blacklisted_button = tk.Button(filter_frame, text="Blacklistés", command=lambda: self.display_filtered_adherents(self.filter_adherents(result, -1), "Adhérents blacklistés"))
        blacklisted_button.pack(side=tk.LEFT)
        
        add_button = tk.Button(filter_frame, text="Rechercher", command=lambda: self.open_search_window())
        add_button.pack(side=tk.LEFT)
        
        add_button = tk.Button(filter_frame, text="Ajouter", command=lambda: self.open_add_adherent_window())
        add_button.pack(side=tk.LEFT)

        
        self.canvas = tk.Canvas(self.popup)
        self.canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        
        scrollbar_y = tk.Scrollbar(self.popup, orient=tk.VERTICAL, command=self.canvas.yview)
        scrollbar_y.pack(side=tk.RIGHT, fill=tk.Y)
        self.canvas.configure(yscrollcommand=scrollbar_y.set)

        
        self.frame = tk.Frame(self.canvas)
        self.canvas.create_window((0, 0), window=self.frame, anchor=tk.NW)

        self.display_filtered_adherents(result, "Adhérents")  

        
        self.canvas.bind_all("<MouseWheel>", lambda event: self.canvas.yview_scroll(int(-1 * (event.delta / 120)), "units"))

        
        self.frame.update_idletasks()
        self.canvas.config(scrollregion=self.canvas.bbox("all"))

    def filter_adherents(self, result, status):
        if status == -1:
            return [row for row in result if row[10] == 1]
        else:            
            return [row for row in result if row[8] == status]

    def display_filtered_adherents(self, filtered_result, title):
        
        for widget in self.frame.winfo_children():
            widget.destroy()

        if filtered_result:
            for row in filtered_result:
                title_label = tk.Label(self.frame, text=f"{row[2]} {row[3]}", font=("Helvetica", 12, "bold"), justify=tk.LEFT)
                title_label.pack(padx=10, pady=5, anchor=tk.W)

                login_label = tk.Label(self.frame, text=row[0], font=("Helvetica", 10, "italic"), justify=tk.LEFT)
                login_label.pack(padx=10, pady=2, anchor=tk.W)

                if row[8] == 1:
                    actuel_label = tk.Label(self.frame, text="Adhérent actif", fg="green", font=("Helvetica", 10, "bold"), justify=tk.LEFT)
                    actuel_label.pack(padx=10, pady=2, anchor=tk.W)
                else:
                    actuel_label = tk.Label(self.frame, text="Adhérent inactif", fg="red", font=("Helvetica", 10, "bold"), justify=tk.LEFT)
                    actuel_label.pack(padx=10, pady=2, anchor=tk.W)

                if row[10] == 1:
                    blacklist_label = tk.Label(self.frame, text="BLACKLISTÉ", fg="red", font=("Helvetica", 12, "bold"), justify=tk.LEFT)
                    blacklist_label.pack(padx=10, pady=2, anchor=tk.W)

                ncarte_label = tk.Label(self.frame, text=f"N° carte : {row[5]}", font=("Helvetica", 10, "bold"), justify=tk.LEFT)
                ncarte_label.pack(padx=10, pady=2, anchor=tk.W)

                details_label_text = f"Mail : {row[4]}\nDate de naissance : {row[6]}\nTéléphone : {row[7]}\nAdresse : {row[9]}\n"
                details_label = tk.Label(self.frame, text=details_label_text, font=("Helvetica", 10), justify=tk.LEFT)
                details_label.pack(padx=10, pady=2, anchor=tk.W)
                
                modify_button = tk.Button(self.frame, text="Modifier les informations", command=lambda r=row: self.open_modify_adherent_window(r))
                modify_button.pack(padx=10, pady=5, anchor=tk.W)
                
                self.display_adherent_info(row[0])

                separator = tk.Label(self.frame, text="-" * 50, justify=tk.LEFT)
                separator.pack(padx=10, pady=5, anchor=tk.W)
        else:
            tk.Label(self.frame, text="Aucun résultat", font=("Helvetica", 10)).pack(padx=10, pady=5, anchor=tk.W)

        
        self.frame.update_idletasks()
        self.canvas.config(scrollregion=self.canvas.bbox("all"))
        
    def open_modify_adherent_window(self, adherent_data):
        
        modify_window = tk.Toplevel(self.root)
        modify_window.title("Modifier les informations")

        
        nom_var = tk.StringVar(value=adherent_data[2])
        prenom_var = tk.StringVar(value=adherent_data[3])
        mail_var = tk.StringVar(value=adherent_data[4])
        telephone_var = tk.StringVar(value=adherent_data[7])
        adresse_var = tk.StringVar(value=adherent_data[9])

        
        tk.Label(modify_window, text="Nom:").grid(row=0, column=0, padx=10, pady=5, sticky=tk.W)
        tk.Entry(modify_window, textvariable=nom_var).grid(row=0, column=1, padx=10, pady=5, sticky=tk.W)

        tk.Label(modify_window, text="Prénom:").grid(row=1, column=0, padx=10, pady=5, sticky=tk.W)
        tk.Entry(modify_window, textvariable=prenom_var).grid(row=1, column=1, padx=10, pady=5, sticky=tk.W)

        tk.Label(modify_window, text="Mail:").grid(row=2, column=0, padx=10, pady=5, sticky=tk.W)
        tk.Entry(modify_window, textvariable=mail_var).grid(row=2, column=1, padx=10, pady=5, sticky=tk.W)

        tk.Label(modify_window, text="Téléphone:").grid(row=3, column=0, padx=10, pady=5, sticky=tk.W)
        tk.Entry(modify_window, textvariable=telephone_var).grid(row=3, column=1, padx=10, pady=5, sticky=tk.W)

        tk.Label(modify_window, text="Adresse:").grid(row=4, column=0, padx=10, pady=5, sticky=tk.W)
        tk.Entry(modify_window, textvariable=adresse_var).grid(row=4, column=1, padx=10, pady=5, sticky=tk.W)

        
        validate_button = tk.Button(modify_window, text="Valider les changements", command=lambda: self.update_adherent(adherent_data[0], nom_var.get(), prenom_var.get(), mail_var.get(), telephone_var.get(), adresse_var.get(), modify_window))
        validate_button.grid(row=5, column=0, columnspan=2, pady=10)

    def update_adherent(self, login, nom, prenom, mail, telephone, adresse, modify_window):
        
        query = """
        UPDATE Adherent
        SET nom = %s, prenom = %s, mail = %s, telephone = %s, adresse = %s
        WHERE login = %s
        """
        values = (nom, prenom, mail, telephone, adresse, login)

        try:
            self.execute_no_return_query(query, values)
            self.connection.commit()
            modify_window.destroy()
            self.show_adherents()
            messagebox.showinfo("Succès", "Les informations de l'adhérent ont été mises à jour.")
        except Exception as e:
            messagebox.showerror("Erreur", f"Erreur lors de la mise à jour des informations de l'adhérent : {e}")
        
    def open_search_window(self):
        search_window = tk.Toplevel(self.root)
        search_window.title("Recherche Adhérent")

        
        login_label = tk.Label(search_window, text="Login:")
        login_label.grid(row=0, column=0, padx=10, pady=5, sticky=tk.W)

        login_entry = tk.Entry(search_window)
        login_entry.grid(row=0, column=1, padx=10, pady=5, sticky=tk.W)
        
        name_label = tk.Label(search_window, text="Nom:")
        name_label.grid(row=1, column=0, padx=10, pady=5, sticky=tk.W)

        name_entry = tk.Entry(search_window)
        name_entry.grid(row=1, column=1, padx=10, pady=5, sticky=tk.W)
        
        firstname_label = tk.Label(search_window, text="Prénom:")
        firstname_label.grid(row=2, column=0, padx=10, pady=5, sticky=tk.W)

        firstname_entry = tk.Entry(search_window)
        firstname_entry.grid(row=2, column=1, padx=10, pady=5, sticky=tk.W)

        
        search_button = tk.Button(search_window, text="Rechercher", command=lambda: self.search_adherents(login_entry.get(), name_entry.get(), firstname_entry.get()))
        search_button.grid(row=3, column=0, columnspan=2, pady=10)

    def search_adherents(self, login, nom, prenom):
        try:
            search_query = f"SELECT * FROM Adherent WHERE login = '{login}' OR nom = '{nom}' OR prenom = '{prenom}';"
            result = self.execute_query(search_query)
            self.display_filtered_adherents(result, "Résultats de la recherche")

        except Exception as e:
            tk.messagebox.showerror("Erreur", f"Erreur lors de la recherche des adhérents : {e}")
        
    def open_add_adherent_window(self):
        add_adherent_window = tk.Toplevel(self.root)
        add_adherent_window.title("Ajouter un adhérent")

        
        login_label = tk.Label(add_adherent_window, text="Login:")
        login_label.grid(row=0, column=0, padx=10, pady=5, sticky=tk.W)

        login_entry = tk.Entry(add_adherent_window)
        login_entry.grid(row=0, column=1, padx=10, pady=5, sticky=tk.W)
        
        password_label = tk.Label(add_adherent_window, text="Mot de passe:")
        password_label.grid(row=1, column=0, padx=10, pady=5, sticky=tk.W)

        password_entry = tk.Entry(add_adherent_window)
        password_entry.grid(row=1, column=1, padx=10, pady=5, sticky=tk.W)
        
        name_label = tk.Label(add_adherent_window, text="Nom:")
        name_label.grid(row=2, column=0, padx=10, pady=5, sticky=tk.W)

        name_entry = tk.Entry(add_adherent_window)
        name_entry.grid(row=2, column=1, padx=10, pady=5, sticky=tk.W)
        
        firstname_label = tk.Label(add_adherent_window, text="Prénom:")
        firstname_label.grid(row=3, column=0, padx=10, pady=5, sticky=tk.W)

        firstname_entry = tk.Entry(add_adherent_window)
        firstname_entry.grid(row=3, column=1, padx=10, pady=5, sticky=tk.W)
        
        mail_label = tk.Label(add_adherent_window, text="Mail:")
        mail_label.grid(row=4, column=0, padx=10, pady=5, sticky=tk.W)

        mail_entry = tk.Entry(add_adherent_window)
        mail_entry.grid(row=4, column=1, padx=10, pady=5, sticky=tk.W)
        
        card_label = tk.Label(add_adherent_window, text="Numéro de carte:")
        card_label.grid(row=5, column=0, padx=10, pady=5, sticky=tk.W)

        card_entry = tk.Entry(add_adherent_window)
        card_entry.grid(row=5, column=1, padx=10, pady=5, sticky=tk.W)

        date_label = tk.Label(add_adherent_window, text="Date de naissance (AAAA-MM-JJ):")
        date_label.grid(row=6, column=0, padx=10, pady=5, sticky=tk.W)

        date_entry = tk.Entry(add_adherent_window)
        date_entry.grid(row=6, column=1, padx=10, pady=5, sticky=tk.W)
        
        phone_label = tk.Label(add_adherent_window, text="Téléphone:")
        phone_label.grid(row=7, column=0, padx=10, pady=5, sticky=tk.W)

        phone_entry = tk.Entry(add_adherent_window)
        phone_entry.grid(row=7, column=1, padx=10, pady=5, sticky=tk.W)
        
        adress_label = tk.Label(add_adherent_window, text="Adresse:")
        adress_label.grid(row=8, column=0, padx=10, pady=5, sticky=tk.W)

        adress_entry = tk.Entry(add_adherent_window)
        adress_entry.grid(row=8, column=1, padx=10, pady=5, sticky=tk.W)

        
        add_button = tk.Button(add_adherent_window, text="Ajouter", command=lambda: self.add_adherent(add_adherent_window, login_entry.get(), password_entry.get(), name_entry.get(), firstname_entry.get(), mail_entry.get(), card_entry.get(), date_entry.get(), phone_entry.get(), adress_entry.get()))
        add_button.grid(row=10, column=0, columnspan=2, pady=10)
        
    def add_adherent(self, add_adherent_window, login, password, nom, prenom, mail, ncarte, date_naissance, telephone, adresse):
        try:
            
            insert_query = f"INSERT INTO Adherent (login, mot_de_passe, nom, prenom, mail, ncarte, date_naissance, telephone, actuel, adresse, est_blacklist) VALUES ('{login}', '{password}', '{nom}', '{prenom}', '{mail}', {ncarte}, '{date_naissance}', '{telephone}', TRUE, '{adresse}', FALSE);"
            self.execute_no_return_query(insert_query)

            
            add_adherent_window.destroy()
            tk.messagebox.showinfo("Succès", "Adhérent ajouté avec succès.")
        except Exception as e:
            tk.messagebox.showerror("Erreur", f"Erreur lors de l'ajout de l'adhérent : {e}")
        
    def display_adherent_info(self, login):
        
        is_blacklisted = self.execute_query(f"SELECT est_blacklist FROM Adherent WHERE login = '{login}'")[0][0]

        
        result_retard = self.execute_query(f"SELECT COUNT(*) FROM Sanction WHERE pret_adherent = '{login}' AND type_sanction = 'retard'")
        result_degradation = self.execute_query(f"SELECT COUNT(*) FROM Sanction WHERE pret_adherent = '{login}' AND type_sanction = 'dégradation'")
        result_somme = self.execute_query(f"SELECT SUM(prix_a_payer) FROM Sanction WHERE pret_adherent = '{login}'")

        
        info_label_text = f"Retards: {result_retard[0][0]}, Dégradations: {result_degradation[0][0]}, Somme à payer: {result_somme[0][0]}"
        info_label = tk.Label(self.frame, text=info_label_text, font=("Helvetica", 10), justify=tk.LEFT)
        info_label.pack(padx=10, pady=2, anchor=tk.W)

        
        if not is_blacklisted:
            blacklist_button = tk.Button(self.frame, text="Blacklister", command=lambda: self.toggle_blacklist(login, True))
            blacklist_button.pack(padx=10, pady=2, anchor=tk.W)

        
        if is_blacklisted:
            unblacklist_button = tk.Button(self.frame, text="Déblacklister", command=lambda: self.toggle_blacklist(login, False))
            unblacklist_button.pack(padx=10, pady=2, anchor=tk.W)  
            
        
        is_active = self.execute_query(f"SELECT actuel FROM Adherent WHERE login = '{login}'")[0][0]

        
        if is_active:
            resign_button = tk.Button(self.frame, text="Résilier adhésion", command=lambda: self.toggle_membership(login, False))
            resign_button.pack(padx=10, pady=2, anchor=tk.W)
        
        else:
            reactivate_button = tk.Button(self.frame, text="Réactiver adhésion", command=lambda: self.toggle_membership(login, True))
            reactivate_button.pack(padx=10, pady=2, anchor=tk.W)
            
    def toggle_membership(self, login, is_active):
        
        self.execute_no_return_query(f"UPDATE Adherent SET actuel = {is_active} WHERE login = '{login}'")
        action = "résilié" if not is_active else "réactivé"
        messagebox.showinfo("Résiliation adhésion", f"L'adhérent {login} a été {action} avec succès.")
        
        self.show_adherents()

    def toggle_blacklist(self, login, is_blacklisted):
        
        self.execute_no_return_query(f"UPDATE Adherent SET est_blacklist = {is_blacklisted} WHERE login = '{login}'")
        action = "blacklisté" if is_blacklisted else "déblacklisté"
        messagebox.showinfo("Blacklist", f"L'adhérent {login} a été {action} avec succès.")
        
        self.show_adherents()

    def create_table(self):
        
        popup = tk.Toplevel(self.root)
        popup.title("Créer une table")

        tk.Label(popup, text="Nom de la table:").grid(row=0, column=0, padx=10, pady=10)
        table_name_entry = tk.Entry(popup)
        table_name_entry.grid(row=0, column=1, padx=10, pady=10)

        tk.Label(popup, text="Colonnes (séparées par des virgules):").grid(row=1, column=0, padx=10, pady=10)
        columns_entry = tk.Entry(popup)
        columns_entry.grid(row=1, column=1, padx=10, pady=10)

        create_button = tk.Button(popup, text="Créer la table", command=lambda: self.execute_create_table(table_name_entry.get(), columns_entry.get(), popup))
        create_button.grid(row=2, column=0, columnspan=2, pady=10)

    def delete_table(self):
        
        popup = tk.Toplevel(self.root)
        popup.title("Supprimer une table")

        tk.Label(popup, text="Nom de la table à supprimer:").grid(row=0, column=0, padx=10, pady=10)
        table_name_entry = tk.Entry(popup)
        table_name_entry.grid(row=0, column=1, padx=10, pady=10)

        delete_button = tk.Button(popup, text="Supprimer la table", command=lambda: self.execute_delete_table(table_name_entry.get(), popup))
        delete_button.grid(row=1, column=0, columnspan=2, pady=10)

    def execute_create_table(self, table_name, columns, popup):
        try:
            create_table_query = f"CREATE TABLE IF NOT EXISTS {table_name} ({columns});"
            self.execute_no_return_query(create_table_query)
            self.connection.commit()
            popup.destroy()
            self.show_message(f"Table '{table_name}' créée avec succès.")
        except (Exception, psycopg2.Error) as error:
            self.show_message(f"Erreur lors de la création de la table '{table_name}': {error}")

    def execute_delete_table(self, table_name, popup):
        try:
            drop_table_query = f"DROP TABLE IF EXISTS {table_name};"
            self.execute_no_return_query(drop_table_query)
            self.connection.commit()
            popup.destroy()
            self.show_message(f"Table '{table_name}' supprimée avec succès.")
        except (Exception, psycopg2.Error) as error:
            self.show_message(f"Erreur lors de la suppression de la table '{table_name}': {error}")

    def display_result(self, result, title):
        
        popup = tk.Toplevel(self.root)
        popup.title(title)

        if result:
            for i, row in enumerate(result):
                tk.Label(popup, text=row).grid(row=i, column=0, padx=10, pady=5)
        else:
            tk.Label(popup, text="Aucun résultat").grid(row=0, column=0, padx=10, pady=5)

    def show_message(self, message):
        
        popup = tk.Toplevel(self.root)
        popup.title("Message")
        tk.Label(popup, text=message).pack(padx=10, pady=10)
        close_button = tk.Button(popup, text="Fermer", command=popup.destroy)
        close_button.pack(pady=10)


if __name__ == "__main__":
    root = tk.Tk()
    app = LibraryApp(root)
    root.mainloop()
